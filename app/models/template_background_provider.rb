class TemplateBackgroundProvider < ActiveRecord::Base
  has_many :template_background_prices, dependent: :restrict_with_error

  has_many :orders_print_background,
           class_name: 'Order',
           foreign_key: 'background_for_print_provider_id',
           dependent: :restrict_with_error

  validates :name, presence: true
end
