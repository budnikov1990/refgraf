class LaminationRotoGlueTypesController < ApplicationController
  before_action :set_and_authorize_lamination_roto_glue_type,
                only: [:edit, :update, :destroy]

  # GET /lamination_roto_glue_types
  def index
    authorize LaminationRotoGlueType
    @lamination_roto_glue_types = policy_scope(LaminationRotoGlueType)
    @lamination_roto_glue_types = @lamination_roto_glue_types.order(id: :asc)
    @lamination_roto_glue_types = @lamination_roto_glue_types.page(params[:page]).per(25)
  end

  # GET /lamination_roto_glue_types/new
  def new
    authorize LaminationRotoGlueType
    @lamination_roto_glue_type = LaminationRotoGlueType.new
  end

  # POST /lamination_roto_glue_types
  def create
    authorize LaminationRotoGlueType
    @lamination_roto_glue_type =
      LaminationRotoGlueType.new(lamination_roto_glue_type_params)
    if @lamination_roto_glue_type.save
      redirect_to lamination_roto_glue_types_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lamination_roto_glue_types/:id/edit
  def edit
  end

  # PATCH/PUT /lamination_roto_glue_types/:id
  def update
    if @lamination_roto_glue_type.update(lamination_roto_glue_type_params)
      redirect_to edit_lamination_roto_glue_type_url(@lamination_roto_glue_type),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lamination_roto_glue_types/:id
  def destroy
    if @lamination_roto_glue_type.destroy
      redirect_to lamination_roto_glue_types_url, notice: t('shared.destroyed')
    else
      redirect_to lamination_roto_glue_types_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lamination_roto_glue_type_params
    params.require(:lamination_roto_glue_type).permit(:name)
  end

  def set_and_authorize_lamination_roto_glue_type
    @lamination_roto_glue_type = LaminationRotoGlueType.find(params[:id])
    authorize @lamination_roto_glue_type
  end
end
