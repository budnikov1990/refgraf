class Cylinder < ActiveRecord::Base
  belongs_to :machine, required: true, inverse_of: :cylinders
  has_many :orders, dependent: :restrict_with_error
  has_many :offset_plates, dependent: :restrict_with_error

  validates :name, presence: true
  validates :perimeter, presence: true, numericality: { greater_than: 0 }
end
