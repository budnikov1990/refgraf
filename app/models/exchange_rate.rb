class ExchangeRate < ActiveRecord::Base
  validates :from, presence: true
  validates :to, presence: true
  validates :rate, presence: true, numericality: { greater_than: 0 }

  def self.get_rate(from_iso_code, to_iso_code, _date = nil)
    rate = find_by(from: from_iso_code, to: to_iso_code)
    rate.present? ? rate.rate : nil
  end

  def self.add_rate(from_iso_code, to_iso_code, rate, _date = nil)
    exrate = find_or_initialize_by(from: from_iso_code, to: to_iso_code)
    exrate.rate = rate
    exrate.save!
  end
end
