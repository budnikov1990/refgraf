require 'features_helper'

RSpec.feature 'Create new roto_preser' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_roto_preser_button) { t('roto_presers.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit roto_presers_path(locale: I18n.locale)
      click_on add_roto_preser_button
    end

    context 'With valid data' do
      scenario 'Admin creates new roto_preser' do
        fill_in 'roto_preser_value', with: 999
        click_on create_button

        expect(current_path).to eq(roto_presers_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#roto_preser_#{RotoPreser.last.id}" do
          expect(page).to have_content '999'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new roto_preser' do
        click_on create_button
        expect(current_path).to eq(roto_presers_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
