FactoryGirl.define do
  factory :lacquering_index do
    machine
    lacquering_type
    lacquering_anilox
    coverage_ratio 1.5
    dry_weight 2.0
    wet_weight 5.0
  end
end
