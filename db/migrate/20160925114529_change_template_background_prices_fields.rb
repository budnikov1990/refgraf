class ChangeTemplateBackgroundPricesFields < ActiveRecord::Migration
  def change
    remove_column :template_background_prices, :background_print_price_cents, :integer
    remove_column :template_background_prices, :background_print_price_currency, :string
    remove_column :template_background_prices, :background_print_pack_amount, :integer
    remove_column :template_background_prices, :background_print_pack_unit, :string
    remove_column :template_background_prices, :background_lamination1_price_cents, :integer
    remove_column :template_background_prices, :background_lamination1_price_currency, :string
    remove_column :template_background_prices, :background_lamination1_pack_amount, :integer
    remove_column :template_background_prices, :background_lamination1_pack_unit, :string
    remove_column :template_background_prices, :background_lamination2_price_cents, :integer
    remove_column :template_background_prices, :background_lamination2_price_currency, :string
    remove_column :template_background_prices, :background_lamination2_pack_amount, :integer
    remove_column :template_background_prices, :background_lamination2_pack_unit, :string

    add_column :template_background_prices, :price_cents, :integer
    add_column :template_background_prices, :price_currency, :string
    add_column :template_background_prices, :pack_amount, :integer
    add_column :template_background_prices, :pack_unit, :string
    add_column :template_background_prices, :price_type, :string
  end
end
