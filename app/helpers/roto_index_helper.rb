module RotoIndexHelper
  def roto_linearities_data_for_select
    RotoLinearity.all.map do |c|
      ri = c.roto_indices.first
      name = c.name

      if ri.present?
        name += " - masa na mokro #{ri.value_wet} [g] na 1 m^2, masa na sucho #{ri.value_dry} [g] na 1 m^2"
      end

      [name, c.id]
    end
  end

  def roto_presers_data_for_select
    RotoPreser.all.map { |c| [c.value, c.id] }
  end

  def roto_paints_data_for_select
    Paint.where(paint_type: 'roto').map { |c| [c.name, c.id] }
  end

  def roto_acetates_data_for_select
    AcetateName.all.map { |c| [c.acetate_provider.name, c.id] }
  end
end
