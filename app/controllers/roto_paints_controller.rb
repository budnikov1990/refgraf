class RotoPaintsController < ApplicationController
  before_action :set_and_authorize_roto_paint, only: [:edit, :update, :destroy]

  # GET /roto_paints
  def index
    authorize RotoPaint
    @roto_paints = policy_scope(RotoPaint)
    @roto_paints = @roto_paints.order(id: :asc)
    @roto_paints = @roto_paints.page(params[:page]).per(25)
  end

  # GET /roto_paints/new
  def new
    authorize RotoPaint
    @roto_paint = RotoPaint.new
  end

  # POST /roto_paints
  def create
    authorize RotoPaint
    @roto_paint = RotoPaint.new(roto_paint_params)
    if @roto_paint.save
      redirect_to roto_paints_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /roto_paints/:id/edit
  def edit
  end

  # PATCH/PUT /roto_paints/:id
  def update
    if @roto_paint.update(roto_paint_params)
      redirect_to edit_roto_paint_url(@roto_paint), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /roto_paints/:id
  def destroy
    if @roto_paint.destroy
      redirect_to roto_paints_url, notice: t('shared.destroyed')
    else
      redirect_to roto_paints_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def roto_paint_params
    params.require(:roto_paint).permit(:name)
  end

  def set_and_authorize_roto_paint
    @roto_paint = RotoPaint.find(params[:id])
    authorize @roto_paint
  end
end
