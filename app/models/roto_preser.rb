class RotoPreser < ActiveRecord::Base
  has_many :orders, dependent: :restrict_with_error

  validates :value, presence: true
end
