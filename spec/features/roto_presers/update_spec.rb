require 'features_helper'

RSpec.feature 'Update roto_preser' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:roto_preser) { create(:roto_preser) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_roto_preser_path(roto_preser, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'roto_preser_value', with: 888
        click_on save_button

        expect(current_path).to eq(edit_roto_preser_path(roto_preser, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        fill_in 'roto_preser_value', with: ''
        click_on save_button
        expect(current_path).to eq(roto_preser_path(roto_preser, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
