require 'features_helper'

RSpec.feature 'View list of color_presets' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:color_preset) { create(:color_preset) }

    background do
      login admin
      visit color_presets_path
    end

    scenario 'User sees list of color_presets' do
      within "#color_preset_#{color_preset.id}" do
        expect(page).to have_content color_preset.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
