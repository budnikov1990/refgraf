require 'rails_helper'

RSpec.describe OffsetPlatePrice, type: :model do
  it { is_expected.to validate_presence_of(:offset_plate_id) }
  it { is_expected.to monetize(:price) }
  it { is_expected.to validate_presence_of(:price_currency) }
end
