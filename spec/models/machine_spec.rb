require 'rails_helper'

RSpec.describe Machine, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:calculation_coefficient) }
  it { is_expected.to validate_numericality_of(:calculation_coefficient).is_greater_than_or_equal_to(0) }
end
