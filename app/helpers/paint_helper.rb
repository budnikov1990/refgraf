module PaintHelper
  def paint_providers_data_for_select
    PaintProvider.all.map { |c| [c.name, c.id] }
  end
end
