class CreateRotoLinearities < ActiveRecord::Migration
  def change
    create_table :roto_linearities do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
