module MenuHelper
  def lamination_top_menu_active(controller_name)
    controller_name == 'lamination_glues' ||
      controller_name == 'lamination_machines' ||
      controller_name == 'lamination_types' ||
      controller_name == 'lamination_glue_types' ||
      controller_name == 'lamination_glue_systems' ||
      controller_name == 'lamination_indices'
  end

  def lacquering_top_menu_active(controller_name)
    controller_name == 'lacquering_aniloxes' ||
      controller_name == 'lacquering_names' ||
      controller_name == 'lacquering_types' ||
      controller_name == 'lacquering_polymers' ||
      controller_name == 'lacquering_indices'
  end

  def machine_top_menu_active(controller_name)
    controller_name == 'machines' ||
      controller_name == 'machine_parameters' ||
      controller_name == 'offset_plates'
  end

  def lamination_roto_top_menu_active(controller_name)
    controller_name == 'lamination_roto_glue_names' ||
      controller_name == 'lamination_roto_glue_types' ||
      controller_name == 'lamination_roto_indices' ||
      controller_name == 'lamination_roto_linearities' ||
      controller_name == 'lamination_roto_presers'
  end

  def roto_top_menu_active(controller_name)
    controller_name == 'roto_linearities' ||
      controller_name == 'roto_presers' ||
      controller_name == 'roto_paints' ||
      controller_name == 'roto_indices'
  end

  def cutting_top_menu_active(controller_name)
    controller_name == 'cutting_machines' ||
      controller_name == 'cutting_indices'
  end

  def providers_top_menu_active(controller_name)
    controller_name == 'paint_providers' ||
      controller_name == 'acetate_providers' ||
      controller_name == 'template_background_providers' ||
      controller_name == 'template_background_prices' ||
      controller_name == 'offset_plate_providers' ||
      controller_name == 'azote_providers' ||
      controller_name == 'pack_providers'
  end
end
