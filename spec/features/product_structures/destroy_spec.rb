require 'features_helper'

RSpec.feature 'Destroy structure' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:product_structure) { create(:product_structure) }

    background do
      login admin
      visit product_structures_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the structure', js: true do
      within "#product_structure_#{product_structure.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(product_structures_path(locale: I18n.locale))
      expect(page).to have_no_selector("#product_structure_#{product_structure.id}")
    end
  end
end
