require 'features_helper'

RSpec.feature 'Update structure' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:product_structure) { create(:product_structure) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_product_structure_path(product_structure, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'product_structure_name', with: 'test123456'
        click_on save_button

        expect(current_path).to eq(edit_product_structure_path(product_structure, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        fill_in 'product_structure_name', with: ''
        click_on save_button
        expect(current_path).to eq(product_structure_path(product_structure, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
