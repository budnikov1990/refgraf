class OrderFinishLaminationFirstUpdate
  include Interactor
  include TransactionalInteractor

  def call
    context.order.finish_lamination_first!
    log
  end

  private

  delegate :params, :user, :order, to: :context

  def log
    Activity.create!(
      key: 'order.finish_lamination_first',
      trackable: order,
      owner: user,
      data: log_params
    )
  end

  def log_params
    {
      id: context.order.id,
      title: context.order.name,
      number: context.order.number,
      new_state: 'laminated_first'
    }
  end
end
