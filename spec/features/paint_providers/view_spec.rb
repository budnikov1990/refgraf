require 'features_helper'

RSpec.feature 'View list of paint_providers' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:paint_provider) { create(:paint_provider) }

    background do
      login admin
      visit paint_providers_path
    end

    scenario 'User sees list of paint_providers' do
      within "#paint_provider_#{paint_provider.id}" do
        expect(page).to have_content paint_provider.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
