require 'features_helper'

RSpec.feature 'Create new template_background_price' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given(:add_template_background_price_button) { t('template_background_prices.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }
    given!(:template) { create(:template) }
    given!(:template_background_provider) { create(:template_background_provider) }

    background do
      login admin
      visit template_background_prices_path(locale: I18n.locale)
      click_on add_template_background_price_button
    end

    context 'With valid data' do
      scenario 'Admin creates new template_background_price' do
        select template.name, from: 'template_background_price_template_id'
        select template_background_provider.name, from: 'template_background_price_template_background_provider_id'
        click_on create_button

        expect(current_path).to eq(template_background_prices_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#template_background_price_#{TemplateBackgroundPrice.last.id}" do
          expect(page).to have_content template_background_provider.name
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new template_background_price' do
        click_on create_button
        expect(current_path).to eq(template_background_prices_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
