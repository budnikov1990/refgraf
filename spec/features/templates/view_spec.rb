require 'features_helper'

RSpec.feature 'View list of templates' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:product_structure) { create(:product_structure) }
    given!(:template) { create(:template, product_structure: product_structure) }

    background do
      login admin
      visit templates_path
    end

    scenario 'User sees list of templates' do
      within "#template_#{template.id}" do
        expect(page).to have_content template.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
