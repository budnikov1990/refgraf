class LaminationGlueTypesController < ApplicationController
  before_action :set_and_authorize_lamination_glue_type, only: [:edit, :update, :destroy]

  # GET /lamination_glue_types
  def index
    authorize LaminationGlueType
    @lamination_glue_types = policy_scope(LaminationGlueType)
    @lamination_glue_types = @lamination_glue_types.order(id: :asc)
    @lamination_glue_types = @lamination_glue_types.page(params[:page]).per(25)
  end

  # GET /lamination_glue_types/new
  def new
    authorize LaminationGlueType
    @lamination_glue_type = LaminationGlueType.new
  end

  # POST /lamination_glue_types
  def create
    authorize LaminationGlueType
    @lamination_glue_type = LaminationGlueType.new(lamination_glue_type_params)
    if @lamination_glue_type.save
      redirect_to lamination_glue_types_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lamination_glue_types/:id/edit
  def edit
  end

  # PATCH/PUT /lamination_glue_types/:id
  def update
    if @lamination_glue_type.update(lamination_glue_type_params)
      redirect_to edit_lamination_glue_type_url(@lamination_glue_type),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lamination_glue_types/:id
  def destroy
    if @lamination_glue_type.destroy
      redirect_to lamination_glue_types_url, notice: t('shared.destroyed')
    else
      redirect_to lamination_glue_types_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lamination_glue_type_params
    params.require(:lamination_glue_type).permit(:name, :lamination_type_id)
  end

  def set_and_authorize_lamination_glue_type
    @lamination_glue_type = LaminationGlueType.find(params[:id])
    authorize @lamination_glue_type
  end
end
