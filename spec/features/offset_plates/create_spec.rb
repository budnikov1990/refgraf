require 'features_helper'

RSpec.feature 'Create new offset_plate' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_offset_plate_button) { t('offset_plates.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }
    given!(:machine) { create(:machine) }

    background do
      login admin
      visit offset_plates_path(locale: I18n.locale)
      click_on add_offset_plate_button
    end

    context 'With valid data' do
      scenario 'Admin creates new offset_plate' do
        fill_in 'offset_plate_name', with: 'test1234'
        select machine.name, from: 'offset_plate_machine_id'
        select machine.cylinders.first.name, from: 'offset_plate_cylinder_id'
        click_on create_button

        expect(current_path).to eq(offset_plates_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#offset_plate_#{OffsetPlate.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new offset_plate' do
        click_on create_button
        expect(current_path).to eq(offset_plates_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
