FactoryGirl.define do
  factory :paint do
    name "MyString"
    price_cents 1
    price_currency "MyString"
    pack_amount 1
    pack_unit "MyString"
    paint_provider nil
  end
end
