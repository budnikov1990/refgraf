require 'features_helper'

RSpec.feature 'View list of roto_indices' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:roto_index) { create(:roto_index) }

    background do
      login admin
      visit roto_indices_path
    end

    scenario 'User sees list of roto_indices' do
      within "#roto_index_#{roto_index.id}" do
        expect(page).to have_content roto_index.value_wet
        expect(page).to have_content roto_index.value_dry
        expect(page).to have_content roto_index.roto_linearity.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
