require 'features_helper'

RSpec.feature 'Destroy user' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:user) { create(:user) }

    background do
      login admin
      visit users_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the user', js: true do
      within "#user_#{user.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(users_path(locale: I18n.locale))
      expect(page).to have_no_selector("#user_#{user.id}")
    end
  end
end
