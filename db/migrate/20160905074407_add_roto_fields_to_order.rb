class AddRotoFieldsToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :roto_paint_provider_id, :integer
    add_column :orders, :roto_acetate_name_id, :integer
    add_column :orders, :roto_paint_percentage, :integer
    add_column :orders, :roto_acetate_percentage, :integer

    add_index :orders, :roto_paint_provider_id
    add_index :orders, :roto_acetate_name_id
  end
end
