require 'features_helper'

RSpec.feature 'Create new lamination_index' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_lamination_index_button) { t('lamination_indices.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }
    given!(:lamination_machine) { create(:lamination_machine) }
    given!(:lamination_glue_system) { create(:lamination_glue_system) }

    background do
      login admin
      visit lamination_indices_path(locale: I18n.locale)
      click_on add_lamination_index_button
    end

    context 'With valid data' do
      scenario 'Admin creates new lamination_index' do
        fill_in 'lamination_index_value', with: 5
        select lamination_glue_system.name, from: 'lamination_index_lamination_glue_system_1_id'
        select lamination_machine.name, from: 'lamination_index_lamination_machine_id'
        click_on create_button

        expect(current_path).to eq(lamination_indices_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#lamination_index_#{LaminationIndex.last.id}" do
          expect(page).to have_content 5
          expect(page).to have_content lamination_glue_system.name
          expect(page).to have_content lamination_machine.name
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new lamination_index' do
        click_on create_button
        expect(current_path).to eq(lamination_indices_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
