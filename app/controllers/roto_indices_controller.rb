class RotoIndicesController < ApplicationController
  before_action :set_and_authorize_roto_index, only: [:edit, :update, :destroy]

  # GET /roto_indices
  def index
    authorize RotoIndex
    @roto_indices = policy_scope(RotoIndex)
    @roto_indices = @roto_indices.order(id: :asc)
    @roto_indices = @roto_indices.page(params[:page]).per(25)
  end

  # GET /roto_indices/new
  def new
    authorize RotoIndex
    @roto_index = RotoIndex.new
  end

  # POST /roto_indices
  def create
    authorize RotoIndex
    @roto_index = RotoIndex.new(roto_index_params)
    if @roto_index.save
      redirect_to roto_indices_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /roto_indices/:id/edit
  def edit
  end

  # PATCH/PUT /roto_indices/:id
  def update
    if @roto_index.update(roto_index_params)
      redirect_to edit_roto_index_url(@roto_index), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /roto_indices/:id
  def destroy
    if @roto_index.destroy
      redirect_to roto_indices_url, notice: t('shared.destroyed')
    else
      redirect_to roto_indices_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def roto_index_params
    params.require(:roto_index).permit(:roto_linearity_id, :value_wet, :value_dry)
  end

  def set_and_authorize_roto_index
    @roto_index = RotoIndex.find(params[:id])
    authorize @roto_index
  end
end
