class CreateCuttingIndices < ActiveRecord::Migration
  def change
    create_table :cutting_indices do |t|
      t.float :value, default: 0.0
      t.references :cutting_machine, index: true, foreign_key: true
      t.references :product_structure, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
