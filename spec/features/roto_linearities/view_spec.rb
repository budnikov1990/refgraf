require 'features_helper'

RSpec.feature 'View list of roto_linearities' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:roto_linearity) { create(:roto_linearity) }

    background do
      login admin
      visit roto_linearities_path
    end

    scenario 'User sees list of roto_linearities' do
      within "#roto_linearity_#{roto_linearity.id}" do
        expect(page).to have_content roto_linearity.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
