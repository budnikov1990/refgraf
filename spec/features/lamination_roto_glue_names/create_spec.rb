require 'features_helper'

RSpec.feature 'Create new lamination_roto_glue_name' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_lamination_roto_glue_name_button) { t('lamination_roto_glue_names.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit lamination_roto_glue_names_path(locale: I18n.locale)
      click_on add_lamination_roto_glue_name_button
    end

    context 'With valid data' do
      scenario 'Admin creates new lamination_roto_glue_name' do
        fill_in 'lamination_roto_glue_name_name', with: 'test test'
        fill_in 'lamination_roto_glue_name_resin_name', with: 'test test'
        fill_in 'lamination_roto_glue_name_hardener_name', with: 'test test'
        click_on create_button

        expect(current_path).to eq(lamination_roto_glue_names_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#lamination_roto_glue_name_#{LaminationRotoGlueName.last.id}" do
          expect(page).to have_content 'test test'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new lamination_roto_glue_name' do
        click_on create_button
        expect(current_path).to eq(lamination_roto_glue_names_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
