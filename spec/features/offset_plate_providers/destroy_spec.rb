require 'features_helper'

RSpec.feature 'Destroy offset_plate_provider' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:offset_plate_provider) { create(:offset_plate_provider) }

    background do
      login admin
      visit offset_plate_providers_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the offset_plate_provider', js: true do
      within "#offset_plate_provider_#{offset_plate_provider.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(offset_plate_providers_path(locale: I18n.locale))
      expect(page).to have_no_selector("#offset_plate_provider_#{offset_plate_provider.id}")
    end
  end
end
