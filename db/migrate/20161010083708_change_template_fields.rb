class ChangeTemplateFields < ActiveRecord::Migration
  def change
    remove_column :templates, :name, :string
    remove_column :templates, :raw_material_weight, :float
    remove_column :templates, :raw_material_weight_for_lamination_1, :float
    remove_column :templates, :raw_material_weight_for_lamination_2, :float
    remove_column :templates, :background_for_print, :string
    remove_column :templates, :background_for_lamination_1, :string
    remove_column :templates, :background_for_lamination_2, :string

    add_reference :templates, :background, index: true
    add_reference :templates, :background_for_lamination_1, index: true
    add_reference :templates, :background_for_lamination_2, index: true
  end
end
