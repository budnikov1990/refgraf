require 'features_helper'

RSpec.feature 'Update roto_index' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:roto_index) { create(:roto_index) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_roto_index_path(roto_index, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'roto_index_value_wet', with: 123
        fill_in 'roto_index_value_dry', with: 456
        click_on save_button

        expect(current_path).to eq(edit_roto_index_path(roto_index, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        fill_in 'roto_index_value_wet', with: ''
        click_on save_button
        expect(current_path).to eq(roto_index_path(roto_index, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
