class LaminationRotoGlueNamesController < ApplicationController
  before_action :set_and_authorize_lamination_roto_glue_name,
                only: [:edit, :update, :destroy]

  # GET /lamination_roto_glue_names
  def index
    authorize LaminationRotoGlueName
    @lamination_roto_glue_names = policy_scope(LaminationRotoGlueName)
    @lamination_roto_glue_names = @lamination_roto_glue_names.order(id: :asc)
    @lamination_roto_glue_names = @lamination_roto_glue_names.page(params[:page]).per(25)
  end

  # GET /lamination_roto_glue_names/new
  def new
    authorize LaminationRotoGlueName
    @lamination_roto_glue_name = LaminationRotoGlueName.new
  end

  # POST /lamination_roto_glue_names
  def create
    authorize LaminationRotoGlueName
    @lamination_roto_glue_name =
      LaminationRotoGlueName.new(lamination_roto_glue_name_params)
    if @lamination_roto_glue_name.save
      redirect_to lamination_roto_glue_names_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lamination_roto_glue_names/:id/edit
  def edit
  end

  # PATCH/PUT /lamination_roto_glue_names/:id
  def update
    if @lamination_roto_glue_name.update(lamination_roto_glue_name_params)
      redirect_to edit_lamination_roto_glue_name_url(@lamination_roto_glue_name),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lamination_roto_glue_names/:id
  def destroy
    if @lamination_roto_glue_name.destroy
      redirect_to lamination_roto_glue_names_url, notice: t('shared.destroyed')
    else
      redirect_to lamination_roto_glue_names_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lamination_roto_glue_name_params
    params.require(:lamination_roto_glue_name).permit(:name, :resin_name, :hardener_name)
  end

  def set_and_authorize_lamination_roto_glue_name
    @lamination_roto_glue_name = LaminationRotoGlueName.find(params[:id])
    authorize @lamination_roto_glue_name
  end
end
