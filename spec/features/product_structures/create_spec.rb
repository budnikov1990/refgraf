require 'features_helper'

RSpec.feature 'Create new structure' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_structure_button) { t('product_structures.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit product_structures_path(locale: I18n.locale)
      click_on add_structure_button
    end

    context 'With valid data' do
      scenario 'Admin creates new structure' do
        fill_in 'product_structure_name', with: 'test1234'
        fill_in 'product_structure_uid', with: 'test1234'
        click_on create_button

        expect(current_path).to eq(product_structures_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#product_structure_#{ProductStructure.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new structure' do
        click_on create_button
        expect(current_path).to eq(product_structures_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
