require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to validate_presence_of(:first_name) }
  it { is_expected.to validate_presence_of(:last_name) }
  it { is_expected.to validate_presence_of(:role) }
  it { is_expected.to allow_values('admin', 'technologist', 'operator', 'storeman').for(:role) }
  it { is_expected.not_to allow_values('user', 'random').for(:role) }
end
