class Machine < ActiveRecord::Base
  has_many :orders, dependent: :restrict_with_error
  has_many :machine_parameters, dependent: :restrict_with_error
  has_many :cylinders, dependent: :destroy, inverse_of: :machine
  has_many :lacquering_aniloxes, dependent: :restrict_with_error
  has_many :lacquering_names, dependent: :restrict_with_error
  has_many :lacquering_polymers, dependent: :restrict_with_error
  has_many :lacquering_types, dependent: :restrict_with_error
  has_many :lacquering_indices, dependent: :restrict_with_error
  has_many :offset_plates, dependent: :restrict_with_error
  has_many :machine_template_speeds

  accepts_nested_attributes_for :cylinders, reject_if: :all_blank, allow_destroy: true

  monetize :price_clear_cents
  monetize :hour_cost_cents

  validates :name, :price_clear, :price_clear_currency,
            :hour_cost, :hour_cost_currency, presence: true
  validates :calculation_coefficient,
            presence: true,
            numericality: { greater_than_or_equal_to: 0 }
end
