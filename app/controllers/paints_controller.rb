class PaintsController < ApplicationController
  before_action :set_and_authorize_paint, only: [:edit, :update, :destroy]

  # GET /paints
  def index
    authorize Paint
    @paints = policy_scope(Paint)
    @paints = @paints.order(id: :asc)
    @paints = @paints.page(params[:page]).per(25)
  end

  # GET /paints/new
  def new
    authorize Paint
    @paint = Paint.new
  end

  # POST /paints
  def create
    authorize Paint
    @paint = Paint.new(paint_params)
    if @paint.save
      redirect_to paints_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /paints/:id/edit
  def edit
  end

  # PATCH/PUT /paints/:id
  def update
    if @paint.update(paint_params)
      redirect_to edit_paint_url(@paint), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /paints/:id
  def destroy
    if @paint.destroy
      redirect_to paints_url, notice: t('shared.destroyed')
    else
      redirect_to paints_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def paint_params
    params.require(:paint).permit(
      :name, :paint_type, :machine_id, paint_prices_attributes: [
        :id, :price_cents, :price, :paint_provider_id,
        :price_currency, :pack_amount, :pack_unit,
        :_destroy
      ]
    )
  end

  def set_and_authorize_paint
    @paint = Paint.find(params[:id])
    authorize @paint
  end
end
