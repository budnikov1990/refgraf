class UsersController < ApplicationController
  before_action :set_and_authorize_user, only: [:edit, :update, :destroy]

  # GET /users
  def index
    authorize User
    @users = policy_scope(User)
    @users = @users.order(id: :asc)
    @users = @users.page(params[:page]).per(25)
  end

  # GET /users/new
  def new
    authorize User
    @user = User.new
  end

  # POST /users
  def create
    authorize User
    @user = User.new(user_params)
    if @user.save
      redirect_to users_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /users/:id/edit
  def edit
  end

  # PATCH/PUT /users/:id
  def update
    if @user.update(update_user_params)
      redirect_to edit_user_url(@user), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /users/:id
  def destroy
    if @user.destroy
      redirect_to users_url, notice: t('shared.destroyed')
    else
      redirect_to users_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def user_params
    params.require(:user).permit(
      :email, :role, :first_name, :last_name,
      :password, :password_confirmation
    )
  end

  def update_user_params
    result = user_params
    if result[:password].present?
      result
    else
      result.except(:password, :password_confirmation)
    end
  end

  def set_and_authorize_user
    @user = User.find(params[:id])
    authorize @user
  end
end
