class AddOrderOffsetPlateProvider < ActiveRecord::Migration
  def change
    add_column :orders, :offset_plate_provider_id, :integer
  end
end
