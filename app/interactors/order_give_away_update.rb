class OrderGiveAwayUpdate
  include Interactor
  include TransactionalInteractor

  def call
    context.order.give_away!
    log
  end

  private

  delegate :params, :user, :order, to: :context

  def log
    Activity.create!(
      key: 'order.give_away',
      trackable: order,
      owner: user,
      data: log_params
    )
  end

  def log_params
    {
      id: context.order.id,
      title: context.order.name,
      number: context.order.number,
      previous_state: 'cut',
      new_state: 'complete'
    }
  end
end
