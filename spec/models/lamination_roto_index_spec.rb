require 'rails_helper'

RSpec.describe LaminationRotoIndex, type: :model do
  it { is_expected.to validate_presence_of(:value_dry) }
  it { is_expected.to validate_presence_of(:value_wet) }
  it { is_expected.to validate_presence_of(:lamination_roto_linearity_id) }
  it { is_expected.to validate_presence_of(:lamination_roto_glue_type_id) }
end
