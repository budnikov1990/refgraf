require 'features_helper'

RSpec.feature 'Update template_background_provider' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:template_background_provider) { create(:template_background_provider) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_template_background_provider_path(template_background_provider, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'template_background_provider_name', with: 'test1234'
        click_on save_button

        expect(current_path).to eq(edit_template_background_provider_path(template_background_provider, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        fill_in 'template_background_provider_name', with: ''
        click_on save_button
        expect(current_path).to eq(template_background_provider_path(template_background_provider, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
