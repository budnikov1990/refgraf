require 'features_helper'

RSpec.feature 'Destroy lacquering_index' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lacquering_index) { create(:lacquering_index) }

    background do
      login admin
      visit lacquering_indices_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lacquering_index', js: true do
      within "#lacquering_index_#{lacquering_index.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lacquering_indices_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lacquering_index_#{lacquering_index.id}")
    end
  end
end
