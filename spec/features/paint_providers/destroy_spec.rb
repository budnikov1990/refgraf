require 'features_helper'

RSpec.feature 'Destroy paint_provider' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:paint_provider) { create(:paint_provider) }

    background do
      login admin
      visit paint_providers_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the paint_provider', js: true do
      within "#paint_provider_#{paint_provider.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(paint_providers_path(locale: I18n.locale))
      expect(page).to have_no_selector("#paint_provider_#{paint_provider.id}")
    end
  end
end
