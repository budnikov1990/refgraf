require 'features_helper'

RSpec.feature 'View list of users' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:users) { create_list(:user, 2) }

    background do
      login admin
      visit users_path
    end

    scenario 'User sees list of users' do
      within "#user_#{admin.id}" do
        expect(page).to have_content t("user.role.#{admin.role}")
        expect(page).to have_content admin.email
        expect(page).to have_no_selector '.fa-trash'
      end
      users.each do |user|
        within "#user_#{user.id}" do
          expect(page).to have_content t("user.role.#{user.role}")
          expect(page).to have_content user.email
          expect(page).to have_selector '.fa-trash'
        end
      end
    end
  end
end
