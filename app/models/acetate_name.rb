class AcetateName < ActiveRecord::Base
  belongs_to :acetate_provider, required: true, inverse_of: :acetate_names

  monetize :price_cents

  validates :name, presence: true
  validates :price, presence: true
  validates :price_currency, presence: true
end
