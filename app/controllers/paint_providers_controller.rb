class PaintProvidersController < ApplicationController
  before_action :set_and_authorize_paint_provider, only: [:edit, :update, :destroy]

  # GET /paint_providers
  def index
    authorize PaintProvider
    @paint_providers = policy_scope(PaintProvider)
    @paint_providers = @paint_providers.order(id: :asc)
    @paint_providers = @paint_providers.page(params[:page]).per(25)
  end

  # GET /paint_providers/new
  def new
    authorize PaintProvider
    @paint_provider = PaintProvider.new
  end

  # POST /paint_providers
  def create
    authorize PaintProvider
    @paint_provider = PaintProvider.new(paint_provider_params)
    if @paint_provider.save
      redirect_to paint_providers_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /paint_providers/:id/edit
  def edit
  end

  # PATCH/PUT /paint_providers/:id
  def update
    if @paint_provider.update(paint_provider_params)
      redirect_to edit_paint_provider_url(@paint_provider), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /paint_providers/:id
  def destroy
    if @paint_provider.destroy
      redirect_to paint_providers_url, notice: t('shared.destroyed')
    else
      redirect_to paint_providers_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def paint_provider_params
    params.require(:paint_provider).permit(
      :name
    )
  end

  def set_and_authorize_paint_provider
    @paint_provider = PaintProvider.find(params[:id])
    authorize @paint_provider
  end
end
