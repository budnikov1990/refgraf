require 'rails_helper'

RSpec.describe LaminationRotoGlueName, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:resin_name) }
  it { is_expected.to validate_presence_of(:hardener_name) }
end
