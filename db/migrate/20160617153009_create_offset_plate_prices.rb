class CreateOffsetPlatePrices < ActiveRecord::Migration
  def change
    create_table :offset_plate_prices do |t|
      t.monetize :price
      t.references :offset_plate, index: true, foreign_key: true
      t.references :offset_plate_provider, index: true, foreign_key: true
      t.integer :pack_amount
      t.string :pack_unit

      t.timestamps null: false
    end
  end
end
