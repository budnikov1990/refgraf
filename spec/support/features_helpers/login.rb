module FeaturesHelpers
  module Login
    # @example Usage:
    #   login # Creates new user
    #   login User.last # Logs in last user
    #   login :admin # Creates admin
    #   login username: 'tester' # Creates user called 'tester'
    #   login factory: :admin, username: 'tester' # Creates admin called 'tester'
    def login(args = nil)
      user = setup_user(args)
      password = setup_password(args)
      perform_login(user, password)
      expect(current_path).to eq(root_path(locale: I18n.locale))
      user
    end

    private

    def setup_user(args)
      if args.is_a?(User)
        args
      elsif args.is_a?(Hash)
        factory = args.delete(:factory) || :user
        create(factory, args)
      elsif args.is_a?(Symbol)
        create(args)
      else
        create(:user)
      end
    end

    def setup_password(args)
      if args.is_a?(Hash) && args[:password].present?
        args[:password]
      else
        attributes_for(:user)[:password]
      end
    end

    def perform_login(user, password)
      visit new_user_session_path
      within 'form#new_user' do
        fill_in('user_email', with: user.email)
        fill_in('user_password', with: password)
        find('input[type=submit]').click
      end
    end
  end
end
