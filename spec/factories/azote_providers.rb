FactoryGirl.define do
  factory :azote_provider do
    name 'test'
    price 1
    pack_amount 1
    pack_unit 'l'
  end
end
