class CreateAcetateNames < ActiveRecord::Migration
  def change
    create_table :acetate_names do |t|
      t.string :name
      t.monetize :price
      t.references :acetate_provider
      t.integer :pack_amount
      t.string :pack_unit

      t.timestamps null: false
    end
  end
end
