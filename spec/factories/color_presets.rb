FactoryGirl.define do
  factory :color_preset do
    name 'test color'
    colors_number 2
    offset_plates_count 4
  end
end
