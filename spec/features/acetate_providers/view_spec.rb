require 'features_helper'

RSpec.feature 'View list of acetate_providers' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:acetate_provider) { create(:acetate_provider) }

    background do
      login admin
      visit acetate_providers_path
    end

    scenario 'User sees list of acetate_providers' do
      within "#acetate_provider_#{acetate_provider.id}" do
        expect(page).to have_content acetate_provider.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
