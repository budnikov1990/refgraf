class LacqueringTypesController < ApplicationController
  before_action :set_and_authorize_lacquering_type, only: [:edit, :update, :destroy]

  # GET /lacquering_types
  def index
    authorize LacqueringType
    @lacquering_types = policy_scope(LacqueringType)
    @lacquering_types = @lacquering_types.order(id: :asc)
    @lacquering_types = @lacquering_types.page(params[:page]).per(25)
  end

  # GET /lacquering_types/new
  def new
    authorize LacqueringType
    @lacquering_type = LacqueringType.new
  end

  # POST /lacquering_types
  def create
    authorize LacqueringType
    @lacquering_type = LacqueringType.new(lacquering_type_params)
    if @lacquering_type.save
      redirect_to lacquering_types_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lacquering_types/:id/edit
  def edit
  end

  # PATCH/PUT /lacquering_types/:id
  def update
    if @lacquering_type.update(lacquering_type_params)
      redirect_to edit_lacquering_type_url(@lacquering_type),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lacquering_types/:id
  def destroy
    if @lacquering_type.destroy
      redirect_to lacquering_types_url, notice: t('shared.destroyed')
    else
      redirect_to lacquering_types_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lacquering_type_params
    params.require(:lacquering_type).permit(:value, :machine_id)
  end

  def set_and_authorize_lacquering_type
    @lacquering_type = LacqueringType.find(params[:id])
    authorize @lacquering_type
  end
end
