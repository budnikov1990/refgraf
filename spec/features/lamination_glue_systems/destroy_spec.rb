require 'features_helper'

RSpec.feature 'Destroy lamination_glue_system' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_glue_system) { create(:lamination_glue_system) }

    background do
      login admin
      visit lamination_glue_systems_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lamination_glue_system', js: true do
      within "#lamination_glue_system_#{lamination_glue_system.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lamination_glue_systems_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lamination_glue_system_#{lamination_glue_system.id}")
    end
  end
end
