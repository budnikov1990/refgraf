class CreateTemplateBackgroundProviders < ActiveRecord::Migration
  def change
    create_table :template_background_providers do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
