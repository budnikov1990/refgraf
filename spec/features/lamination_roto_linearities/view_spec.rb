require 'features_helper'

RSpec.feature 'View list of lamination_roto_linearities' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_roto_linearity) { create(:lamination_roto_linearity) }

    background do
      login admin
      visit lamination_roto_linearities_path
    end

    scenario 'User sees list of lamination_roto_linearities' do
      within "#lamination_roto_linearity_#{lamination_roto_linearity.id}" do
        expect(page).to have_content lamination_roto_linearity.value
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
