require 'features_helper'

RSpec.feature 'View list of lacquering_names' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lacquering_name) { create(:lacquering_name) }

    background do
      login admin
      visit lacquering_names_path
    end

    scenario 'User sees list of lacquering_names' do
      within "#lacquering_name_#{lacquering_name.id}" do
        expect(page).to have_content lacquering_name.value
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
