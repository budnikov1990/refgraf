class CuttingMachinesController < ApplicationController
  before_action :set_and_authorize_cutting_machine, only: [:edit, :update, :destroy]

  # GET /cutting_machines
  def index
    authorize CuttingMachine
    @cutting_machines = policy_scope(CuttingMachine)
    @cutting_machines = @cutting_machines.order(id: :asc)
    @cutting_machines = @cutting_machines.page(params[:page]).per(25)
  end

  # GET /cutting_machines/new
  def new
    authorize CuttingMachine
    @cutting_machine = CuttingMachine.new
  end

  # POST /cutting_machines
  def create
    authorize CuttingMachine
    @cutting_machine = CuttingMachine.new(cutting_machine_params)
    if @cutting_machine.save
      redirect_to cutting_machines_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /cutting_machines/:id/edit
  def edit
  end

  # PATCH/PUT /cutting_machines/:id
  def update
    if @cutting_machine.update(cutting_machine_params)
      redirect_to edit_cutting_machine_url(@cutting_machine), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /cutting_machines/:id
  def destroy
    if @cutting_machine.destroy
      redirect_to cutting_machines_url, notice: t('shared.destroyed')
    else
      redirect_to cutting_machines_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def cutting_machine_params
    params.require(:cutting_machine).permit(
      :name, :vpzp, :hour_cost, :hour_cost_currency
    )
  end

  def set_and_authorize_cutting_machine
    @cutting_machine = CuttingMachine.find(params[:id])
    authorize @cutting_machine
  end
end
