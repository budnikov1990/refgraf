require 'features_helper'

RSpec.feature 'Destroy roto_index' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:roto_index) { create(:roto_index) }

    background do
      login admin
      visit roto_indices_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the roto_index', js: true do
      within "#roto_index_#{roto_index.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(roto_indices_path(locale: I18n.locale))
      expect(page).to have_no_selector("#roto_index_#{roto_index.id}")
    end
  end
end
