class AddColorPresetsFields < ActiveRecord::Migration
  def change
    add_column :color_presets, :offset_plates_count, :integer
  end
end
