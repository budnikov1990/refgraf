require 'features_helper'

RSpec.feature 'Update template' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:product_structure) { create(:product_structure) }
    given!(:product_structure_new) { create(:product_structure, name: 'new structure', uid: 'new_uid') }
    given!(:template) { create(:template, product_structure: product_structure) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_template_path(template, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'template_name', with: 'test123456'
        select product_structure_new.name, from: 'template_product_structure_id'
        click_on save_button

        expect(current_path).to eq(edit_template_path(template, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        fill_in 'template_name', with: ''
        click_on save_button
        expect(current_path).to eq(template_path(template, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
