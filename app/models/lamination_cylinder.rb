class LaminationCylinder < ActiveRecord::Base
  belongs_to :lamination_machine, required: true, inverse_of: :lamination_cylinders
  has_many :orders_duplex,
           class_name: 'Order',
           foreign_key: 'lamination_cylinder_1_id',
           dependent: :restrict_with_error

  has_many :orders_triplex,
           class_name: 'Order',
           foreign_key: 'lamination_cylinder_2_id',
           dependent: :restrict_with_error

  validates :value, presence: true, numericality: { greater_than: 0 }
end
