class AddVpzpToLaminationIndices < ActiveRecord::Migration
  def change
    add_column :lamination_indices, :vpzp, :integer, default: 0
  end
end
