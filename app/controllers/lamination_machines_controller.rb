class LaminationMachinesController < ApplicationController
  before_action :set_and_authorize_lamination_machine, only: [:edit, :update, :destroy]

  # GET /lamination_machines
  def index
    authorize LaminationMachine
    @lamination_machines = policy_scope(LaminationMachine)
    @lamination_machines = @lamination_machines.order(id: :asc)
    @lamination_machines = @lamination_machines.page(params[:page]).per(25)
  end

  # GET /lamination_machines/new
  def new
    authorize LaminationMachine
    @lamination_machine = LaminationMachine.new
  end

  # POST /lamination_machines
  def create
    authorize LaminationMachine
    @lamination_machine = LaminationMachine.new(lamination_machine_params)
    if @lamination_machine.save
      redirect_to lamination_machines_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lamination_machines/:id/edit
  def edit
  end

  # PATCH/PUT /lamination_machines/:id
  def update
    if @lamination_machine.update(lamination_machine_params)
      redirect_to edit_lamination_machine_url(@lamination_machine),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lamination_machines/:id
  def destroy
    if @lamination_machine.destroy
      redirect_to lamination_machines_url, notice: t('shared.destroyed')
    else
      redirect_to lamination_machines_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lamination_machine_params
    params.require(:lamination_machine).permit(
      :name, :use_lamination_roto,
      :hour_cost, :hour_cost_currency,
      lamination_cylinders_attributes: [:id, :value, :_destroy]
    )
  end

  def set_and_authorize_lamination_machine
    @lamination_machine = LaminationMachine.find(params[:id])
    authorize @lamination_machine
  end
end
