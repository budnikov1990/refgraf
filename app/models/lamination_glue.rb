class LaminationGlue < ActiveRecord::Base
  has_many :orders_duplex,
           class_name: 'Order',
           foreign_key: 'lamination_glue_1_id',
           dependent: :restrict_with_error

  has_many :orders_triplex,
           class_name: 'Order',
           foreign_key: 'lamination_glue_2_id',
           dependent: :restrict_with_error

  validates :name, presence: true, uniqueness: true
end
