module PricesHelper
  def currency_for_select
    %w(EUR PLN).map { |c| [c, c] }
  end

  def paint_types_for_select
    %w(yellow magenta cyan black additional roto lacquer).map do |color|
      [I18n.t("color_presets.colors.#{color}"), color]
    end
  end

  def paint_providers_for_select
    PaintProvider.all.map do |provider|
      [provider.name, provider.id]
    end
  end

  def units_for_select
    %w(l kg).map { |c| [c, c] }
  end

  def background_for_print_provider_select(background_id)
    TemplateBackgroundProvider.includes(:template_background_prices).where(template_background_prices: { background_id: background_id }).all.map do |c|
      [c.name, c.id]
    end
  end

  def cyan_paint_providers_select
    providers = PaintPrice.joins(:paint).where(paints: { paint_type: 'cyan' })
    provider_ids = providers.map{|price| price.paint_provider_id }.uniq

    PaintProvider.where(id: provider_ids).map do |provider|
      [provider.name, provider.id]
    end
  end

  def black_paint_providers_select
    providers = PaintPrice.joins(:paint).where(paints: { paint_type: 'black' })
    provider_ids = providers.map{|price| price.paint_provider_id }.uniq

    PaintProvider.where(id: provider_ids).map do |provider|
      [provider.name, provider.id]
    end
  end

  def magenta_paint_providers_select
    providers = PaintPrice.joins(:paint).where(paints: { paint_type: 'magenta' })
    provider_ids = providers.map{|price| price.paint_provider_id }.uniq

    PaintProvider.where(id: provider_ids).map do |provider|
      [provider.name, provider.id]
    end
  end

  def yellow_paint_providers_select
    providers = PaintPrice.joins(:paint).where(paints: { paint_type: 'yellow' })
    provider_ids = providers.map{|price| price.paint_provider_id }.uniq

    PaintProvider.where(id: provider_ids).map do |provider|
      [provider.name, provider.id]
    end
  end

  def additional_paint_providers_select(paint_id)
    providers = PaintPrice.joins(:paint).where(paints: { id: paint_id })
    provider_ids = providers.map{|price| price.paint_provider_id }.uniq

    PaintProvider.where(id: provider_ids).map do |provider|
      [provider.name, provider.id]
    end
  end

  def exchange(value, to)
    value.as_us_dollar.exchange_to(to)
  end
end
