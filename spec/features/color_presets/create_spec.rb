require 'features_helper'

RSpec.feature 'Create new color_preset' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_color_preset_button) { t('color_presets.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit color_presets_path(locale: I18n.locale)
      click_on add_color_preset_button
    end

    context 'With valid data' do
      scenario 'Admin creates new color_preset' do
        fill_in 'color_preset_name', with: 'test1234'
        fill_in 'color_preset_colors_number', with: 5
        fill_in 'color_preset_offset_plates_count', with: 4
        click_on create_button

        expect(current_path).to eq(color_presets_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#color_preset_#{ColorPreset.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new color_preset' do
        click_on create_button
        expect(current_path).to eq(color_presets_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
