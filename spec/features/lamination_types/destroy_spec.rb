require 'features_helper'

RSpec.feature 'Destroy lamination_type' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_type) { create(:lamination_type) }

    background do
      login admin
      visit lamination_types_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lamination_type', js: true do
      within "#lamination_type_#{lamination_type.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lamination_types_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lamination_type_#{lamination_type.id}")
    end
  end
end
