FactoryGirl.define do
  factory :lamination_machine do
    name 'test lamination machine'
    use_lamination_roto false

    transient do
      lamination_cylinders_count 1
    end

    after :build do |machine, evaluator|
      evaluator.lamination_cylinders_count.times do
        machine.lamination_cylinders.build(
          attributes_for(:lamination_cylinder, lamination_machine: machine)
        )
      end
    end
  end
end
