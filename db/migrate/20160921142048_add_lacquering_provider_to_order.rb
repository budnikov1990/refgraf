class AddLacqueringProviderToOrder < ActiveRecord::Migration
  def change
    add_reference :orders, :lacquering_provider, index: true
  end
end
