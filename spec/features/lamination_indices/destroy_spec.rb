require 'features_helper'

RSpec.feature 'Destroy lamination_index' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_glue_system_1) { create(:lamination_glue_system) }
    given!(:lamination_index) { create(:lamination_index, lamination_glue_system_1_id: lamination_glue_system_1.id) }

    background do
      login admin
      visit lamination_indices_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lamination_index', js: true do
      within "#lamination_index_#{lamination_index.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lamination_indices_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lamination_index_#{lamination_index.id}")
    end
  end
end
