require 'features_helper'

RSpec.feature 'Create new lamination_roto_index' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_lamination_roto_index_button) { t('lamination_roto_indices.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }
    given!(:lamination_roto_linearity) { create(:lamination_roto_linearity) }
    given!(:lamination_roto_glue_type) { create(:lamination_roto_glue_type) }

    background do
      login admin
      visit lamination_roto_indices_path(locale: I18n.locale)
      click_on add_lamination_roto_index_button
    end

    context 'With valid data' do
      scenario 'Admin creates new lamination_roto_index' do
        fill_in 'lamination_roto_index_value_wet', with: 5
        fill_in 'lamination_roto_index_value_dry', with: 8
        select lamination_roto_glue_type.name, from: 'lamination_roto_index_lamination_roto_glue_type_id'
        select lamination_roto_linearity.value, from: 'lamination_roto_index_lamination_roto_linearity_id'
        click_on create_button

        expect(current_path).to eq(lamination_roto_indices_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#lamination_roto_index_#{LaminationRotoIndex.last.id}" do
          expect(page).to have_content '5'
          expect(page).to have_content '8'
          expect(page).to have_content lamination_roto_glue_type.name
          expect(page).to have_content lamination_roto_linearity.value
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new lamination_roto_index' do
        click_on create_button
        expect(current_path).to eq(lamination_roto_indices_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
