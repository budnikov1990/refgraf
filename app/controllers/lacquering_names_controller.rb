class LacqueringNamesController < ApplicationController
  before_action :set_and_authorize_lacquering_name, only: [:edit, :update, :destroy]

  # GET /lacquering_names
  def index
    authorize LacqueringName
    @lacquering_names = policy_scope(LacqueringName)
    @lacquering_names = @lacquering_names.order(id: :asc)
    @lacquering_names = @lacquering_names.page(params[:page]).per(25)
  end

  # GET /lacquering_names/new
  def new
    authorize LacqueringName
    @lacquering_name = LacqueringName.new
  end

  # POST /lacquering_names
  def create
    authorize LacqueringName
    @lacquering_name = LacqueringName.new(lacquering_name_params)
    if @lacquering_name.save
      redirect_to lacquering_names_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lacquering_names/:id/edit
  def edit
  end

  # PATCH/PUT /lacquering_names/:id
  def update
    if @lacquering_name.update(lacquering_name_params)
      redirect_to edit_lacquering_name_url(@lacquering_name),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lacquering_names/:id
  def destroy
    if @lacquering_name.destroy
      redirect_to lacquering_names_url, notice: t('shared.destroyed')
    else
      redirect_to lacquering_names_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lacquering_name_params
    params.require(:lacquering_name).permit(:value, :machine_id)
  end

  def set_and_authorize_lacquering_name
    @lacquering_name = LacqueringName.find(params[:id])
    authorize @lacquering_name
  end
end
