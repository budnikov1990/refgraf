class Order < ActiveRecord::Base
  include Workflow

  monetize :price_additional_cents
  monetize :price_pack_cents

  belongs_to :template
  belongs_to :color_preset
  belongs_to :machine
  belongs_to :cylinder
  belongs_to :client
  belongs_to :cutting_machine
  belongs_to :product_structure

  belongs_to :lacquering_anilox
  belongs_to :lacquering_polymer
  belongs_to :lacquering_type
  belongs_to :lacquering_provider, class_name: 'PaintProvider'
  belongs_to :lacquering_name, class_name: 'Paint'

  belongs_to :roto_linearity
  belongs_to :roto_preser
  belongs_to :roto_paint, class_name: 'Paint'
  belongs_to :roto_paint_provider, class_name: 'PaintProvider'
  belongs_to :roto_acetate_provider, class_name: 'AcetateProvider'
  belongs_to :roto_acetate_name, class_name: 'AcetateName'

  belongs_to :lamination_machine_1, class_name: 'LaminationMachine'
  belongs_to :lamination_cylinder_1, class_name: 'LaminationCylinder'
  belongs_to :lamination_type_1, class_name: 'LaminationType'
  belongs_to :lamination_glue_1, class_name: 'LaminationGlue'
  belongs_to :lamination_glue_system_1, class_name: 'LaminationGlueSystem'
  belongs_to :lamination_glue_type_1, class_name: 'LaminationGlueType'

  belongs_to :lamination_machine_2, class_name: 'LaminationMachine'
  belongs_to :lamination_cylinder_2, class_name: 'LaminationCylinder'
  belongs_to :lamination_type_2, class_name: 'LaminationType'
  belongs_to :lamination_glue_2, class_name: 'LaminationGlue'
  belongs_to :lamination_glue_system_2, class_name: 'LaminationGlueSystem'
  belongs_to :lamination_glue_type_2, class_name: 'LaminationGlueType'

  belongs_to :lamination_roto_glue_name_1, class_name: 'LaminationRotoGlueName'
  belongs_to :lamination_roto_glue_type_1, class_name: 'LaminationRotoGlueType'
  belongs_to :lamination_roto_linearity_1, class_name: 'LaminationRotoLinearity'
  belongs_to :lamination_roto_preser_1, class_name: 'LaminationRotoPreser'

  belongs_to :lamination_roto_glue_name_2, class_name: 'LaminationRotoGlueName'
  belongs_to :lamination_roto_glue_type_2, class_name: 'LaminationRotoGlueType'
  belongs_to :lamination_roto_linearity_2, class_name: 'LaminationRotoLinearity'
  belongs_to :lamination_roto_preser_2, class_name: 'LaminationRotoPreser'

  belongs_to :yellow_provider, class_name: 'PaintProvider'
  belongs_to :magenta_provider, class_name: 'PaintProvider'
  belongs_to :cyan_provider, class_name: 'PaintProvider'
  belongs_to :black_provider, class_name: 'PaintProvider'

  belongs_to :panton_1_provider, class_name: 'PaintProvider'
  belongs_to :panton_2_provider, class_name: 'PaintProvider'

  belongs_to :panton_1_paint, class_name: 'Paint'
  belongs_to :panton_2_paint, class_name: 'Paint'

  belongs_to :background_for_print_provider, class_name: 'TemplateBackgroundProvider'
  belongs_to :background_for_lamination1_provider, class_name: 'TemplateBackgroundProvider'
  belongs_to :background_for_lamination2_provider, class_name: 'TemplateBackgroundProvider'
  belongs_to :offset_plate_provider
  belongs_to :azote_provider
  belongs_to :pack_provider

  has_one :additional_parameter, dependent: :destroy

  workflow do
    state :new do
      event :actualize, transitions_to: :in_preparation
    end
    state :in_preparation do
      event :prepare_materials, transitions_to: :prepared
    end
    state :prepared do
      event :rollback, transitions_to: :in_preparation
      event :start_print, transitions_to: :printing
    end
    state :printing do
      event :finish_print, transitions_to: :printed
    end
    state :printed do
      event :start_cut, transitions_to: :cutting
      event :start_lamination_first, transitions_to: :laminating_first
    end
    state :laminating_first do
      event :finish_lamination_first, transitions_to: :laminated_first
    end
    state :laminated_first do
      event :start_cut, transitions_to: :cutting
      event :start_lamination_second, transitions_to: :laminating_second
    end
    state :laminating_second do
      event :finish_lamination_second, transitions_to: :laminated_second
    end
    state :laminated_second do
      event :start_cut, transitions_to: :cutting
    end
    state :cutting do
      event :finish_cut, transitions_to: :cut
    end
    state :cut do
      event :give_away, transitions_to: :complete
    end
    state :complete
  end

  delegate :simplex, to: :product_structure
  delegate :duplex, to: :product_structure
  delegate :triplex, to: :product_structure

  def machine_params
    @mp ||= MachineParameter.params(machine, product_structure).first
  end

  def lacquering
    @lacquering ||= LacqueringIndex.find_by(find_lacquering_params)
  end

  def roto
    @roto ||= RotoIndex.find_by(roto_linearity: roto_linearity)
  end

  def lamination_index
    @lamination_index ||= LaminationIndex.find_by(
      lamination_glue_system_1: lamination_glue_system_1,
      lamination_machine: lamination_machine_1
    )
  end

  def find_lacquering_params
    {
      lacquering_type: lacquering_type,
      lacquering_anilox: lacquering_anilox,
      lacquering_polymer: lacquering_polymer,
      machine: machine
    }
  end

  def yellow_price
    yellow_provider.paint_prices.joins(:paint).where(paints: { paint_type: 'yellow' }).first
  end

  def magenta_price
    magenta_provider.paint_prices.joins(:paint).where(paints: { paint_type: 'magenta' }).first
  end

  def cyan_price
    cyan_provider.paint_prices.joins(:paint).where(paints: { paint_type: 'cyan' }).first
  end

  def black_price
    black_provider.paint_prices.joins(:paint).where(paints: { paint_type: 'black' }).first
  end

  def panton_1_price
    PaintPrice.where(paint_provider_id: panton_1_provider_id, paint_id: panton_1_paint_id).first
  end

  def panton_2_price
    PaintPrice.where(paint_provider_id: panton_1_provider_id, paint_id: panton_1_paint_id).first
  end

  def roto_paint_price
    PaintPrice.where(paint_provider_id: roto_paint_provider_id, paint_id: roto_paint_id).first
  end

  def lacquer_price
    PaintPrice.where(paint_provider_id: lacquering_provider_id, paint_id: lacquering_name_id).first
  end

  def background_print_price
    TemplateBackgroundPrice.where(background_id: template.background_id, template_background_provider_id: background_for_print_provider_id,).first
  end

  def background_lamination1_price
    TemplateBackgroundPrice.where(background_id: template.background_for_lamination_1_id, template_background_provider_id: background_for_lamination1_provider_id).first
  end

  def background_lamination2_price
    TemplateBackgroundPrice.where(background_id: template.background_for_lamination_2_id, template_background_provider_id: background_for_lamination2_provider_id).first
  end

  def machine_template_speed
    template.machine_template_speeds.find_by(machine_id: machine_id)
  end

  def machine_template_spending
    template.machine_template_spendings.find_by(machine_id: machine_id)
  end
end
