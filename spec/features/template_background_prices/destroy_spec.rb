require 'features_helper'

RSpec.feature 'Destroy template_background_price' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:template_background_price) { create(:template_background_price) }

    background do
      login admin
      visit template_background_prices_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the template_background_price', js: true do
      within "#template_background_price_#{template_background_price.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(template_background_prices_path(locale: I18n.locale))
      expect(page).to have_no_selector("#template_background_price_#{template_background_price.id}")
    end
  end
end
