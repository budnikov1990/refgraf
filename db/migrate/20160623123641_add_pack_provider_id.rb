class AddPackProviderId < ActiveRecord::Migration
  def change
    change_table :orders do |t|
      t.monetize :price_pack
      t.integer :pack_provider_id
      t.string :pack_name
      t.integer :pack_amount
    end
  end
end
