class CalculateOrderPrice
  attr_reader :order, :params

  def initialize(params)
    @order = params[:order]
    @params = params[:params]
  end

  def background_for_print_price
    @background_for_print_price ||=
      (
        @order.background_print_price.price * @params.weight
      ).exchange_to('PLN')
  end

  def background_for_lamination1_price
    if order.duplex || order.triplex
      @background_for_lamination1_price ||=
        (@order.background_lamination1_price.price * @params.duplex_weight).exchange_to('PLN')
    else
      0
    end
  end

  def background_for_lamination2_price
    if order.triplex
      @background_for_lamination2_price ||=
        (@order.background_lamination2_price.price * @params.triplex_weight).exchange_to('PLN')
    else
      0
    end
  end

  def cutting_price
    @cutting_price ||= exchange(((@params.before_cutting_mb / @order.cutting_machine.vpzp) / 60) * @order.cutting_machine.hour_cost, 'PLN')
  end

  def machine_work_costs
    @machine_work_costs ||= exchange(((@params.order_estimate_time / 60) * @order.machine.hour_cost), 'PLN')
  end

  def lamination_work_costs
    if order.duplex || order.triplex
      @lamination_work_costs ||= exchange(((@params.lamination_time / 60) * @order.lamination_machine_1.hour_cost), 'PLN')
    else
      0
    end
  end

  def yellow_price
    @yellow_price ||=
      exchange((yellow_weight * @order.yellow_price.price), 'PLN')
  end

  def magenta_price
    @magenta_price ||=
      exchange((magenta_weight * @order.magenta_price.price), 'PLN')
  end

  def cyan_price
    @cyan_price ||=
      exchange((cyan_weight * @order.cyan_price.price), 'PLN')
  end

  def black_price
    @black_price ||=
      exchange((black_weight * @order.black_price.price), 'PLN')
  end

  def offset_plate_price
    @offset_plate_price ||=
      exchange((
        @order.color_preset.offset_plates_count * offset_plate_price_obj.price
      ), 'PLN')
  end

  def panton_1_price
    @panton_1_price ||=
      if @order.panton_1_paint_id.present?
        exchange((params.panton_1_weight * @order.panton_1_price.price), 'PLN')
      else
        0
      end
  end

  def panton_2_price
    @panton_2_price ||=
      if @order.panton_2_paint_id.present?
        exchange((params.panton_2_weight * @order.panton_2_price.price), 'PLN')
      else
        0
      end
  end

  def lacquer_price
    @lacquer_price ||=
      if @order.lacquering
        exchange((
          order.lacquer_price.price * params.lacquer_wet_weight
        ), 'PLN')
      else
        0
      end
  end

  def roto_paint_price
    @roto_paint_price ||=
      if @order.roto
        exchange((
          order.roto_paint_price.price * params.roto_paint_wet_percentage
        ), 'PLN')
      else
        0
      end
  end

  def roto_acetate_price
    @roto_acetate_price ||=
      if @order.roto
        exchange((
          order.roto_acetate_name.price * params.roto_acetate_wet_percentage
        ), 'PLN')
      else
        0
      end
  end

  def roto_acetate_cleaning
    roto_acetate_cleaning ||=
      if @order.roto
        exchange((
          order.roto_acetate_name.price * 5.5
        ), 'PLN')
      else
        0
      end
  end

  def azote_price
    @azote_price ||=
      if @order.machine.use_azote
        exchange((
          @order.azote_provider.price * azote_weight
        ), 'PLN')
      else
        0
      end
  end

  def machine_clear_price
    @machine_clear_price ||=
      exchange((@params.amount_mb * @order.machine.price_clear), 'PLN')
  end

  def pack_price
    @pack_price ||=
      exchange((pack_amount * @order.price_pack), 'PLN')
  end

  def yellow_weight
    @yellow_weight ||=
      (@params.base_ink_weight * (@order.yellow_percent.to_d / 100)).round(2)
  end

  def magenta_weight
    @magenta_weight ||=
      (@params.base_ink_weight * (@order.magenta_percent.to_d / 100)).round(2)
  end

  def cyan_weight
    @cyan_weight ||=
      (@params.base_ink_weight * (@order.cyan_percent.to_d / 100)).round(2)
  end

  def black_weight
    @black_weight ||=
      (@params.base_ink_weight * (@order.black_percent.to_d / 100)).round(2)
  end

  def azote_weight
    @azote_weight ||=
      (@params.amount_mb / @order.machine.azote_count).round(2)
  end

  def pack_amount
    @pack_amount ||=
      (@order.amount.to_f / @order.pack_amount.to_f).ceil
  end

  def background_provider_price
    @background_provider_price ||= TemplateBackgroundPrice.find_by(
      template: @order.template,
      template_background_provider: @order.background_for_print_provider
    )
  end

  def offset_plate_price_obj
    @offset_plate_price_obj ||=
      OffsetPlatePrice.find_by(
        offset_plate: @order.cylinder.offset_plates.first,
        offset_plate_provider: @order.offset_plate_provider
      )
  end

  def constant_costs
    @constant_costs ||= Money.new(27040, 'PLN') * 4
  end

  def sum_materials_price
    @sum_price ||=
      background_for_print_price + background_for_lamination1_price +
      background_for_lamination2_price + yellow_price + magenta_price + cyan_price +
      black_price + panton_1_price + panton_2_price +
      offset_plate_price + azote_price + machine_clear_price +
      @order.price_additional + pack_price + roto_paint_price +
      roto_acetate_price + roto_acetate_cleaning + lacquer_price
  end

  def sum_working_price
    @sum_working_price ||= machine_work_costs + lamination_work_costs
  end

  def total_sum
    total_sum ||= sum_materials_price + sum_working_price + exchange(order.price_additional, 'PLN') + constant_costs + cutting_price
  end

  private

  def exchange(value, to)
    value.as_us_dollar.exchange_to(to)
  end
end
