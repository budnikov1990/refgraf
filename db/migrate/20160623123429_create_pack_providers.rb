class CreatePackProviders < ActiveRecord::Migration
  def change
    create_table :pack_providers do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
