require 'features_helper'

RSpec.feature 'Create new machine' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_machine_button) { t('machines.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit machines_path(locale: I18n.locale)
      click_on add_machine_button
    end

    context 'With valid data' do
      scenario 'Admin creates new machine without cylinders' do
        fill_in 'machine_name', with: 'test1234'
        fill_in 'machine_calculation_coefficient', with: 500
        click_on create_button

        expect(current_path).to eq(machines_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#machine_#{Machine.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new machine' do
        click_on create_button
        expect(current_path).to eq(machines_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
