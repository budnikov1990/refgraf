require 'rails_helper'

RSpec.describe LacqueringIndex, type: :model do
  it { is_expected.to validate_presence_of(:machine_id) }
  it { is_expected.to validate_presence_of(:lacquering_type_id) }
end
