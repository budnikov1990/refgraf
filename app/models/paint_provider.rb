class PaintProvider < ActiveRecord::Base
  has_many :orders_yellow_paint,
           class_name: 'Order',
           foreign_key: 'yellow_provider_id',
           dependent: :restrict_with_error
  has_many :orders_magenta_paint,
           class_name: 'Order',
           foreign_key: 'magenta_provider_id',
           dependent: :restrict_with_error
  has_many :orders_cyan_paint,
           class_name: 'Order',
           foreign_key: 'cyan_provider_id',
           dependent: :restrict_with_error
  has_many :orders_black_paint,
           class_name: 'Order',
           foreign_key: 'black_provider_id',
           dependent: :restrict_with_error

  has_many :paint_prices
  has_many :paints, through: :paint_prices

  validates :name, presence: true

  # validates :yellow, presence: true
  # validates :magenta, presence: true
  # validates :cyan, presence: true
  # validates :black, presence: true

  # validates :price_yellow, presence: true
  # validates :price_magenta, presence: true
  # validates :price_cyan, presence: true
  # validates :price_black, presence: true

  # validates :price_yellow_currency, presence: true
  # validates :price_magenta_currency, presence: true
  # validates :price_cyan_currency, presence: true
  # validates :price_black_currency, presence: true

  def black
    paints.where(paint_type: 'black').first
  end

  def cyan
    paints.where(paint_type: 'cyan').first
  end

  def yellow
    paints.where(paint_type: 'yellow').first
  end

  def magenta
    paints.where(paint_type: 'magenta').first
  end
end
