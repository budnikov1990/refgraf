class OrdersController < ApplicationController
  before_action :set_and_authorize_order,
                only: [
                  :show, :edit, :update, :destroy,
                  :actualize_edit, :actualize_update,
                  :prepared_update, :rollback_update, :start_print_update,
                  :finish_print_update, :give_away_update,
                  :pdf_main, :pdf_for_print, :pdf_after_print,
                  :pdf_lamination_1, :pdf_lamination_2,
                  :update_order_additional_info,
                  :start_cut_update, :finish_cut_update,
                  :cant_prepare_materials_update,
                  :start_lamination_first_update, :finish_lamination_first_update,
                  :start_lamination_second_update, :finish_lamination_second_update,
                  :price_edit, :price_update
                ]

  # GET /orders
  def index
    authorize Order
    @orders = policy_scope(Order)
    @orders = @orders.order(id: :asc)
    @orders = @orders.page(params[:page]).per(25)
  end

  # GET /orders/:id
  def show
    calculate_params
    calculate_price
    order_log
  end

  # GET /orders/new
  def new
    authorize Order
    @form = OrderForm.new
  end

  # POST /orders
  def create
    authorize Order
    @form = OrderForm.new(user: current_user, params: order_form_params)
    if @form.save
      redirect_to orders_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /orders/:id/edit
  def edit
    @form = OrderForm.new(user: current_user, order: @order)
  end

  # PATCH/PUT /orders/:id
  def update
    @form = OrderForm.new(user: current_user, order: @order, params: order_form_params)
    if @form.save
      redirect_to order_url(@order), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /orders/:id
  def destroy
    if @order.destroy
      redirect_to orders_url, notice: t('shared.destroyed')
    else
      redirect_to orders_url, alert: t('shared.not_destroyed')
    end
  end

  # GET /orders/:id/actualize_edit
  def actualize_edit
    @form = "ActualizeOrder#{@order.product_structure.uid.capitalize}Form"
            .constantize.new(user: current_user, order: @order)
  end

  # PATCH/PUT /orders/:id/actualize_update
  def actualize_update
    @form = "ActualizeOrder#{@order.product_structure.uid.capitalize}Form"
            .constantize
            .new(
              user: current_user,
              order: @order,
              params: send("actualize_order_#{@order.product_structure.uid}_form")
            )
    if @form.save
      redirect_to order_url(@order), notice: t('shared.updated')
    else
      render :actualize_edit
    end
  end

  # GET /orders/:id/price_edit
  def price_edit
    @form = PriceCalculationOrderForm.new(user: current_user, order: @order)
  end

  # PATCH/PUT /orders/:id/price_update
  def price_update
    @form = PriceCalculationOrderForm.new(
      user: current_user,
      order: @order,
      params: price_update_params
    )
    if @form.save
      redirect_to order_url(@order), notice: t('shared.updated')
    else
      render :price_edit
    end
  end

  # PATCH/PUT /orders/:id/prepared_update
  def prepared_update
    OrderPreparedUpdate.call(interactor_params)
    redirect_to order_url(@order), notice: t('shared.updated')
  end

  # PATCH/PUT /orders/:id/rollback_update
  def rollback_update
    OrderRollbackUpdate.call(interactor_params)
    redirect_to order_url(@order), notice: t('shared.updated')
  end

  # PATCH/PUT /orders/:id/start_print_update
  def start_print_update
    OrderStartPrintUpdate.call(interactor_params)
    redirect_to order_url(@order), notice: t('shared.updated')
  end

  # PATCH/PUT /orders/:id/finish_print_update
  def finish_print_update
    OrderFinishPrintUpdate.call(interactor_params)
    redirect_to order_url(@order), notice: t('shared.updated')

    # params = interactor_params.merge(params: update_order_additional_info_params)
    # flash[:notice] = t('shared.updated')
    # flash.keep(:notice)
    # render js: "window.location = '#{order_url(@order)}'"
  end

  # PATCH/PUT /orders/:id/start_lamination_first_update
  def start_lamination_first_update
    OrderStartLaminationFirstUpdate.call(interactor_params)
    redirect_to order_url(@order), notice: t('shared.updated')
  end

  # PATCH/PUT /orders/:id/finish_lamination_first_update
  def finish_lamination_first_update
    OrderFinishLaminationFirstUpdate.call(interactor_params)
    redirect_to order_url(@order), notice: t('shared.updated')
  end

  # PATCH/PUT /orders/:id/start_lamination_second_update
  def start_lamination_second_update
    OrderStartLaminationSecondUpdate.call(interactor_params)
    redirect_to order_url(@order), notice: t('shared.updated')
  end

  # PATCH/PUT /orders/:id/finish_lamination_second_update
  def finish_lamination_second_update
    OrderFinishLaminationSecondUpdate.call(interactor_params)
    redirect_to order_url(@order), notice: t('shared.updated')
  end

  # PATCH/PUT /orders/:id/start_cut_update
  def start_cut_update
    OrderStartCutUpdate.call(interactor_params)
    redirect_to order_url(@order), notice: t('shared.updated')
  end

  # PATCH/PUT /orders/:id/finish_cut_update
  def finish_cut_update
    OrderFinishCutUpdate.call(interactor_params)
    redirect_to order_url(@order), notice: t('shared.updated')
  end

  # PATCH/PUT /orders/:id/give_away_update
  def give_away_update
    OrderGiveAwayUpdate.call(interactor_params)
    redirect_to order_url(@order), notice: t('shared.updated')
  end

  # GET /orders/:id/pdf_main.pdf
  def pdf_main
    show_in_pdf(t('orders.pdf_file_names.main'))
  end

  # GET /orders/:id/pdf_for_operator.pdf
  def pdf_for_print
    show_in_pdf(t('orders.pdf_file_names.for_print'))
  end

  # GET /orders/:id/pdf_after_print.pdf
  def pdf_after_print
    show_in_pdf(t('orders.pdf_file_names.after_print'))
  end

  # GET /orders/:id/pdf_lamination_1.pdf
  def pdf_lamination_1
    show_in_pdf(t('orders.pdf_file_names.lamination_1'))
  end

  # GET /orders/:id/pdf_lamination_2.pdf
  def pdf_lamination_2
    show_in_pdf(t('orders.pdf_file_names.lamination_2'))
  end

  # PATCH /orders/:id/update_order_additional_info
  def update_order_additional_info
    if @order.additional_parameter.update_attributes(update_order_additional_info_params)
      head :no_content
    else
      format.json { render json: @order.errors, status: :unprocessable_entity }
    end
  end

  # PATCH/PUT /orders/:id/can_not_prepare_materials_update
  def cant_prepare_materials_update
    params = interactor_params.merge(params: update_order_additional_info_params)
    CantPrepareMaterialsUpdate.call(params)
    flash[:notice] = t('shared.updated')
    flash.keep(:notice)
    render js: "window.location = '#{order_url(@order)}'"
  end

  private

  def order_form_params
    params.require(:order_form).permit(
      :template_id, :color_preset_id,
      :name, :amount, :width, :height, :product_name, :client_id, :number,
      :product_structure_id
    )
  end

  def actualize_order_simplex_form
    params.require(:actualize_order_simplex_form).permit(
      :machine_id, :cylinder_id,
      :item_count_on_width, :canvas_width, :ink_amount,
      :panton_1_amount, :panton_2_amount,
      :panton_1_paint_id, :panton_2_paint_id,
      :lacquering, :lacquering_anilox_id, :lacquering_polymer_id,
      :lacquering_type_id, :lacquering_name_id,
      :roto, :roto_preser_id, :roto_linearity_id, :cutting_machine_id,
      :roto_paint_id, :roto_acetate_name_id, :roto_paint_percentage, :roto_acetate_percentage
    )
  end

  # TODO: delete repetition
  def actualize_order_duplex_form
    params.require(:actualize_order_duplex_form).permit(
      :machine_id, :cylinder_id,
      :item_count_on_width, :canvas_width, :ink_amount,
      :panton_1_amount, :panton_2_amount,
      :panton_1_paint_id, :panton_2_paint_id,
      :lacquering, :lacquering_anilox_id, :lacquering_polymer_id,
      :lacquering_type_id, :lacquering_name_id,
      :lamination_machine_1_id, :lamination_cylinder_1_id, :lamination_type_1_id,
      :lamination_glue_1_id, :lamination_glue_system_1_id, :lamination_glue_type_1_id,
      :canvas_width_lamination_1,
      :lamination_roto_glue_name_1_id, :lamination_roto_glue_type_1_id,
      :lamination_roto_linearity_1_id, :lamination_roto_preser_1_id,
      :lamination_roto_glue_viscosity_1, :lamination_roto_acetate_1,
      :lamination_roto_resin_1, :lamination_roto_hardener_1,
      :roto, :roto_preser_id, :roto_linearity_id, :cutting_machine_id,
      :roto_paint_id, :roto_acetate_name_id, :roto_paint_percentage, :roto_acetate_percentage
    )
  end

  # TODO: delete repetition
  def actualize_order_triplex_form
    params.require(:actualize_order_triplex_form).permit(
      :machine_id, :cylinder_id,
      :item_count_on_width, :canvas_width, :ink_amount,
      :panton_1_amount, :panton_2_amount,
      :panton_1_paint_id, :panton_2_paint_id,
      :lacquering, :lacquering_anilox_id, :lacquering_polymer_id,
      :lacquering_type_id, :lacquering_name_id,
      :lamination_machine_1_id, :lamination_cylinder_1_id, :lamination_type_1_id,
      :lamination_glue_1_id, :lamination_glue_system_1_id, :lamination_glue_type_1_id,
      :lamination_machine_2_id, :lamination_cylinder_2_id, :lamination_type_2_id,
      :lamination_glue_2_id, :lamination_glue_system_2_id, :lamination_glue_type_2_id,
      :canvas_width_lamination_1, :canvas_width_lamination_2,
      :lamination_roto_glue_name_1_id, :lamination_roto_glue_type_1_id,
      :lamination_roto_linearity_1_id, :lamination_roto_preser_1_id,
      :lamination_roto_glue_name_2_id, :lamination_roto_glue_type_2_id,
      :lamination_roto_linearity_2_id, :lamination_roto_preser_2_id,
      :lamination_roto_glue_viscosity_1, :lamination_roto_acetate_1,
      :lamination_roto_resin_1, :lamination_roto_hardener_1,
      :lamination_roto_glue_viscosity_2, :lamination_roto_acetate_2,
      :lamination_roto_resin_2, :lamination_roto_hardener_2,
      :roto, :roto_preser_id, :roto_linearity_id, :cutting_machine_id,
      :roto_paint_id, :roto_acetate_name_id, :roto_paint_percentage, :roto_acetate_percentage
    )
  end

  def update_order_additional_info_params
    params.require(:order).permit(
      :comments_after_print, :wastepaper_incoming, :wastepaper_remaining,
      :material_for_store_got, :cant_prepare_materials_comment,
      :cant_prepare_materials
    )
  end

  def interactor_params
    {
      order: @order,
      user: current_user
    }
  end

  def price_update_params
    params.require(:price_calculation_order_form).permit(
      :yellow_percent, :magenta_percent, :cyan_percent, :black_percent,
      :yellow_provider_id, :magenta_provider_id, :cyan_provider_id,
      :black_provider_id, :background_for_print_provider_id,
      :background_for_lamination1_provider_id, :background_for_lamination2_provider_id,
      :panton_1_provider_id, :panton_2_provider_id,
      :need_price_calculation, :offset_plate_provider_id,
      :azote_provider_id, :price_additional, :price_additional_currency,
      :pack_provider_id, :price_pack, :price_pack_currency,
      :pack_name, :pack_amount, :roto_paint_provider_id,
      :lacquering_provider_id,
      :roto_paint_id, :roto_acetate_name_id, :roto_paint_percentage, :roto_acetate_percentage
    )
  end

  def set_and_authorize_order
    @order = Order.find(params[:id])
    authorize @order
  end

  def calculate_params
    unless @order.new?
      @params = "#{@order.product_structure.uid.capitalize}Calculator"
                .constantize.new(order: @order)
    end
  end

  def calculate_price
    if @order.current_state >= :in_preparation && @order.need_price_calculation
      @price = CalculateOrderPrice.new(order: @order, params: @params)
    end
  end

  def show_in_pdf(type)
    calculate_params
    respond_to do |format|
      format.pdf { render pdf: "zlecenie_#{type}_#{@order.name}" }
    end
  end

  def order_log
    @logs = Activity.where(trackable: @order).order(created_at: :asc).all
  end
end
