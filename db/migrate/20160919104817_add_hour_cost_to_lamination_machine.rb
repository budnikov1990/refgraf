class AddHourCostToLaminationMachine < ActiveRecord::Migration
  def change
    add_column :lamination_machines, :hour_cost_cents, :integer
    add_column :lamination_machines, :hour_cost_currency, :string
  end
end
