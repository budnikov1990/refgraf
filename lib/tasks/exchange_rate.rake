namespace :exchange_rate do
  desc 'Download the latest exchange rate from currencylayer.com'
  task update: :environment do
    ExchangeRatesUpdater.new.update
  end
end
