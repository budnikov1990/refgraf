class CantPrepareMaterialsUpdate
  include Interactor
  include TransactionalInteractor

  def call
    context.order.additional_parameter.update_attributes(context.params)
    log
  end

  private

  delegate :params, :user, :order, to: :context

  def log
    Activity.create!(
      key: 'order.cant_prepare_materials',
      trackable: order,
      owner: user,
      data: context.params
    )
  end

  def log_params
    {
      id: context.order.id,
      title: context.order.name,
      number: context.order.number,
      previous_state: 'cutting',
      new_state: 'cut'
    }
  end
end
