class AddOrderAdditionalPrice < ActiveRecord::Migration
  def change
    change_table :orders do |t|
      t.monetize :price_additional
    end
  end
end
