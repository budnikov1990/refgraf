class CuttingMachine < ActiveRecord::Base
  has_many :cutting_indices

  monetize :hour_cost_cents

  validates :name, presence: true, uniqueness: true
end
