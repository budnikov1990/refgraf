class LaminationRotoLinearitiesController < ApplicationController
  before_action :set_and_authorize_lamination_roto_linearity,
                only: [:edit, :update, :destroy]

  # GET /lamination_roto_linearities
  def index
    authorize LaminationRotoLinearity
    @lamination_roto_linearities = policy_scope(LaminationRotoLinearity)
    @lamination_roto_linearities = @lamination_roto_linearities.order(id: :asc)
    @lamination_roto_linearities = @lamination_roto_linearities.page(params[:page])
    @lamination_roto_linearities = @lamination_roto_linearities.per(25)
  end

  # GET /lamination_roto_linearities/new
  def new
    authorize LaminationRotoLinearity
    @lamination_roto_linearity = LaminationRotoLinearity.new
  end

  # POST /lamination_roto_linearities
  def create
    authorize LaminationRotoLinearity
    @lamination_roto_linearity =
      LaminationRotoLinearity.new(lamination_roto_linearity_params)
    if @lamination_roto_linearity.save
      redirect_to lamination_roto_linearities_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lamination_roto_linearities/:id/edit
  def edit
  end

  # PATCH/PUT /lamination_roto_linearities/:id
  def update
    if @lamination_roto_linearity.update(lamination_roto_linearity_params)
      redirect_to edit_lamination_roto_linearity_url(@lamination_roto_linearity),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lamination_roto_linearities/:id
  def destroy
    if @lamination_roto_linearity.destroy
      redirect_to lamination_roto_linearities_url, notice: t('shared.destroyed')
    else
      redirect_to lamination_roto_linearities_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lamination_roto_linearity_params
    params.require(:lamination_roto_linearity).permit(:value)
  end

  def set_and_authorize_lamination_roto_linearity
    @lamination_roto_linearity = LaminationRotoLinearity.find(params[:id])
    authorize @lamination_roto_linearity
  end
end
