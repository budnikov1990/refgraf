require 'features_helper'

RSpec.feature 'Destroy lamination_roto_preser' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_roto_preser) { create(:lamination_roto_preser) }

    background do
      login admin
      visit lamination_roto_presers_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lamination_roto_preser', js: true do
      within "#lamination_roto_preser_#{lamination_roto_preser.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lamination_roto_presers_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lamination_roto_preser_#{lamination_roto_preser.id}")
    end
  end
end
