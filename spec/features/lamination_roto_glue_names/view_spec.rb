require 'features_helper'

RSpec.feature 'View list of lamination_roto_glue_names' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_roto_glue_name) { create(:lamination_roto_glue_name) }

    background do
      login admin
      visit lamination_roto_glue_names_path
    end

    scenario 'User sees list of lamination_roto_glue_names' do
      within "#lamination_roto_glue_name_#{lamination_roto_glue_name.id}" do
        expect(page).to have_content lamination_roto_glue_name.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
