class CreateCuttingMachines < ActiveRecord::Migration
  def change
    create_table :cutting_machines do |t|
      t.string :name
      t.monetize :hour_cost
      t.integer :vpzp, default: 0

      t.timestamps null: false
    end
  end
end
