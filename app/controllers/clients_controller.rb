class ClientsController < ApplicationController
  before_action :set_and_authorize_client, only: [:edit, :update, :destroy]

  # GET /clients
  def index
    authorize Client
    @clients = policy_scope(Client)
    @clients = @clients.order(id: :asc)
    @clients = @clients.page(params[:page]).per(25)
  end

  # GET /clients/new
  def new
    authorize Client
    @client = Client.new
  end

  # POST /clients
  def create
    authorize Client
    @client = Client.new(client_params)
    if @client.save
      redirect_to clients_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /clients/:id/edit
  def edit
  end

  # PATCH/PUT /clients/:id
  def update
    if @client.update(client_params)
      redirect_to edit_client_url(@client), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /clients/:id
  def destroy
    if @client.destroy
      redirect_to clients_url, notice: t('shared.destroyed')
    else
      redirect_to clients_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def client_params
    params.require(:client).permit(:name, :packing_method, :outer_diameter)
  end

  def set_and_authorize_client
    @client = Client.find(params[:id])
    authorize @client
  end
end
