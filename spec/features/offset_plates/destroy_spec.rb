require 'features_helper'

RSpec.feature 'Destroy offset_plate' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:offset_plate) { create(:offset_plate) }

    background do
      login admin
      visit offset_plates_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the offset_plate', js: true do
      within "#offset_plate_#{offset_plate.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(offset_plates_path(locale: I18n.locale))
      expect(page).to have_no_selector("#offset_plate_#{offset_plate.id}")
    end
  end
end
