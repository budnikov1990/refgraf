require 'features_helper'

RSpec.feature 'Actualize order' do
  context 'with authenticated user' do
    given(:technologist) { create(:user, role: 'technologist') }
    given!(:product_structure) { create(:product_structure) }
    given!(:color_preset) { create(:color_preset, colors_number: 0) }
    given!(:machine) { create(:machine, name: 'Super machine') }
    given!(:cylinder) { create(:cylinder, name: 'Super cylinder') }
    given!(:template) { create(:template, product_structure: product_structure) }
    given!(:order) { create(:order, product_structure: product_structure, template: template, color_preset: color_preset) }
    given!(:machine_parameter) { create(:machine_parameter, machine: machine, product_structure: order.product_structure) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      machine
      cylinder
      login technologist
      visit actualize_edit_order_path(order, locale: I18n.locale)
    end

    context 'With valid data in simplex' do
      scenario 'Admin updates structure info' do
        fill_in 'actualize_order_simplex_form_item_count_on_width', with: 5
        fill_in 'actualize_order_simplex_form_canvas_width', with: 600
        fill_in 'actualize_order_simplex_form_ink_amount', with: 1.4
        select machine.name, from: "actualize_order_simplex_form_machine_id"
        select cylinder.name, from: "actualize_order_simplex_form_cylinder_id"
        click_on save_button

        expect(current_path).to eq(order_path(order, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data in simplex' do
      scenario 'Admin fails to update structure info' do
        fill_in 'actualize_order_simplex_form_item_count_on_width', with: 0
        click_on save_button
        expect(current_path).to eq(actualize_update_order_path(order, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
