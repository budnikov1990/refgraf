class CreatePaintPrices < ActiveRecord::Migration
  def change
    create_table :paint_prices do |t|
      t.monetize :price
      t.integer :pack_amount
      t.string :pack_unit
      t.references :paint_provider, index: true, foreign_key: true
      t.references :paint, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
