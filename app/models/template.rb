class Template < ActiveRecord::Base
  belongs_to :product_structure

  belongs_to :background
  belongs_to :background_for_lamination_1, class_name: 'Background'
  belongs_to :background_for_lamination_2, class_name: 'Background'

  has_many :orders, dependent: :restrict_with_error
  has_many :machine_template_speeds
  has_many :machine_template_spendings

  validates :product_structure_id, presence: true

  accepts_nested_attributes_for :machine_template_speeds,
                                reject_if: :all_blank,
                                allow_destroy: true

  accepts_nested_attributes_for :machine_template_spendings,
                                reject_if: :all_blank,
                                allow_destroy: true

  def name
    value = ''

    [background, background_for_lamination_1,
     background_for_lamination_2].compact.each_with_index do |bg, index|
       value += "\n" unless index.eql?(0)

       value += "#{index + 1}. #{bg.name}"
    end

    value
  end
end
