class AddGumCleaningToMachine < ActiveRecord::Migration
  def change
    add_column :machines, :gum_cleaning_202, :integer, default: 0
    add_column :machines, :gum_cleaning_204, :integer, default: 0
    add_column :machines, :gum_cleaning_101_203, :integer, default: 0
  end
end
