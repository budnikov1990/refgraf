class AddFieldsToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :panton_1_paint_id, :integer
    add_column :orders, :panton_1_amount, :float, default: 0.0
    add_column :orders, :panton_1_provider_id, :integer
    add_column :orders, :panton_2_paint_id, :integer
    add_column :orders, :panton_2_amount, :float, default: 0.0
    add_column :orders, :panton_2_provider_id, :integer

    remove_column :orders, :panton_1, :string
    remove_column :orders, :panton_2, :string

    add_index :orders, :panton_1_paint_id
    add_index :orders, :panton_1_provider_id
    add_index :orders, :panton_2_paint_id
    add_index :orders, :panton_2_provider_id
  end
end
