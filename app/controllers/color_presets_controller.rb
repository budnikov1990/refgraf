class ColorPresetsController < ApplicationController
  before_action :set_and_authorize_color_preset, only: [:edit, :update, :destroy]

  # GET /color_presets
  def index
    authorize ColorPreset
    @color_presets = policy_scope(ColorPreset)
    @color_presets = @color_presets.order(id: :asc)
    @color_presets = @color_presets.page(params[:page]).per(25)
  end

  # GET /color_presets/new
  def new
    authorize ColorPreset
    @color_preset = ColorPreset.new
  end

  # POST /color_presets
  def create
    authorize ColorPreset
    @color_preset = ColorPreset.new(color_preset_params)
    if @color_preset.save
      redirect_to color_presets_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /color_presets/:id/edit
  def edit
  end

  # PATCH/PUT /color_presets/:id
  def update
    if @color_preset.update(color_preset_params)
      redirect_to edit_color_preset_url(@color_preset), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /color_presets/:id
  def destroy
    if @color_preset.destroy
      redirect_to color_presets_url, notice: t('shared.destroyed')
    else
      redirect_to color_presets_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def color_preset_params
    params.require(:color_preset).permit(
      :name, :colors_number, :offset_plates_count
    )
  end

  def set_and_authorize_color_preset
    @color_preset = ColorPreset.find(params[:id])
    authorize @color_preset
  end
end
