class LaminationRotoLinearity < ActiveRecord::Base
  has_many :lamination_roto_indices, dependent: :restrict_with_error
  has_many :orders_duplex,
           class_name: 'Order',
           foreign_key: 'lamination_roto_linearity_1_id',
           dependent: :restrict_with_error

  has_many :orders_triplex,
           class_name: 'Order',
           foreign_key: 'lamination_roto_linearity_2_id',
           dependent: :restrict_with_error

  validates :value, presence: true
end
