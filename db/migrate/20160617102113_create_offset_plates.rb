class CreateOffsetPlates < ActiveRecord::Migration
  def change
    create_table :offset_plates do |t|
      t.string :name
      t.references :machine, index: true, foreign_key: true
      t.references :cylinder, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
