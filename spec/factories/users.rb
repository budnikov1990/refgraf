FactoryGirl.define do
  factory :user do
    sequence(:email) { |i| "user-#{i}@example.com" }
    password 'qwerty123'
    first_name 'first name'
    last_name 'last name'
    role 'admin'
  end
end
