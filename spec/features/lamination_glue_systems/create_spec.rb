require 'features_helper'

RSpec.feature 'Create new lamination_glue_system' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_lamination_glue_system_button) { t('lamination_glue_systems.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit lamination_glue_systems_path(locale: I18n.locale)
      click_on add_lamination_glue_system_button
    end

    context 'With valid data' do
      scenario 'Admin creates new lamination_glue_system' do
        fill_in 'lamination_glue_system_name', with: 'test1234'
        click_on create_button

        expect(current_path).to eq(lamination_glue_systems_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#lamination_glue_system_#{LaminationGlueSystem.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new lamination_glue_system' do
        click_on create_button
        expect(current_path).to eq(lamination_glue_systems_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
