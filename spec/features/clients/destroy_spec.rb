require 'features_helper'

RSpec.feature 'Destroy client' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:client) { create(:client) }

    background do
      login admin
      visit clients_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the client', js: true do
      within "#client_#{client.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(clients_path(locale: I18n.locale))
      expect(page).to have_no_selector("#client_#{client.id}")
    end
  end
end
