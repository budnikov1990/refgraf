# Wraps the interactor into an ActiveRecord transaction.
#
# If it is included into the Organizer, then all the steps
# will be performed in a single transaction.
module TransactionalInteractor
  extend ActiveSupport::Concern

  included do
    around do |interactor|
      ActiveRecord::Base.transaction do
        begin
          interactor.call
        rescue ActiveRecord::RecordInvalid
          context.fail!
        end
      end
    end
  end
end
