class TemplatesController < ApplicationController
  before_action :set_and_authorize_template, only: [:edit, :update, :destroy]

  # GET /templates
  def index
    authorize Template
    @templates = policy_scope(Template)
    @templates = @templates.order(id: :asc)
    @templates = @templates.page(params[:page]).per(10)
  end

  # GET /templates/new
  def new
    authorize Template
    @form = TemplateForm.new
  end

  # POST /templates
  def create
    authorize Template
    @form = TemplateForm.new(attrs: template_params)
    if @form.save
      redirect_to templates_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /templates/:id/edit
  def edit
    @form = TemplateForm.new(template: @template)
  end

  # PATCH/PUT /templates/:id
  def update
    @form = TemplateForm.new(template: @template, attrs: template_params)
    if @form.save
      redirect_to edit_template_url(@form.template), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /templates/:id
  def destroy
    if @template.destroy
      redirect_to templates_url, notice: t('shared.destroyed')
    else
      redirect_to templates_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def template_params
    params.require(:template).permit(
      :product_structure_id, :background_id,
      :background_for_lamination_1_id, :background_for_lamination_2_id,
      machine_template_speeds_attributes: [:id, :machine_id, :vpbp],
      machine_template_spendings_attributes: [:id, :machine_id, :thousand_more, :thousand_less]
    )
  end

  def set_and_authorize_template
    @template = Template.find(params[:id])
    authorize @template
  end
end
