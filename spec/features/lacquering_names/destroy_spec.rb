require 'features_helper'

RSpec.feature 'Destroy lacquering_name' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lacquering_name) { create(:lacquering_name) }

    background do
      login admin
      visit lacquering_names_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lacquering_name', js: true do
      within "#lacquering_name_#{lacquering_name.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lacquering_names_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lacquering_name_#{lacquering_name.id}")
    end
  end
end
