class RotoIndex < ActiveRecord::Base
  belongs_to :roto_linearity

  validates :value_dry, presence: true
  validates :value_wet, presence: true
  validates :roto_linearity_id, presence: true
end
