require 'rails_helper'

RSpec.describe PaintProviderPolicy do
  subject { described_class }

  context 'By administrator' do
    let(:user) { build(:user, role: 'manager') }

    permissions :index?, :new?, :create? do
      it 'grants access' do
        expect(subject).to permit(user, PaintProvider)
      end
    end

    permissions :edit?, :update? do
      let(:record) { build(:paint_provider) }
      let(:user) { build(:user, role: 'manager') }

      it 'grants access' do
        expect(subject).to permit(user, record)
      end
    end

    permissions :destroy? do
      let(:record) { build(:paint_provider) }
      let(:user) { build(:user, role: 'manager') }

      it 'grants access if other user is destroyed' do
        expect(subject).to permit(user, record)
      end
    end
  end

  %w(technologist operator storeman).each do |role|
    context "By #{role}" do
      let(:user) { build(:user, role: role) }
      let(:record) { build(:paint_provider) }

      permissions :index?, :new?, :create? do
        it 'denies access' do
          expect(subject).not_to permit(user, PaintProvider)
        end
      end

      permissions :edit?, :update?, :destroy? do
        let(:record) { build(:paint_provider) }

        it 'denies access' do
          expect(subject).not_to permit(user, record)
        end
      end
    end
  end
end
