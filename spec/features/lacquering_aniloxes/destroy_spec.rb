require 'features_helper'

RSpec.feature 'Destroy lacquering_anilox' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lacquering_anilox) { create(:lacquering_anilox) }

    background do
      login admin
      visit lacquering_aniloxes_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lacquering_anilox', js: true do
      within "#lacquering_anilox_#{lacquering_anilox.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lacquering_aniloxes_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lacquering_anilox_#{lacquering_anilox.id}")
    end
  end
end
