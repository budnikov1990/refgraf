module Settings
  class Currencylayer < ::Settingslogic
    source "#{Rails.root}/config/currencylayer.yml"
    namespace Rails.env
  end
end
