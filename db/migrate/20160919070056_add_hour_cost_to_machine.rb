class AddHourCostToMachine < ActiveRecord::Migration
  def change
    add_column :machines, :hour_cost_cents, :integer
    add_column :machines, :hour_cost_currency, :string
  end
end
