class AzoteProvidersController < ApplicationController
  before_action :set_and_authorize_azote_provider, only: [:edit, :update, :destroy]

  # GET /azote_providers
  def index
    authorize AzoteProvider
    @azote_providers = policy_scope(AzoteProvider)
    @azote_providers = @azote_providers.order(id: :asc)
    @azote_providers = @azote_providers.page(params[:page]).per(25)
  end

  # GET /azote_providers/new
  def new
    authorize AzoteProvider
    @azote_provider = AzoteProvider.new
  end

  # POST /azote_providers
  def create
    authorize AzoteProvider
    @azote_provider = AzoteProvider.new(azote_provider_params)
    if @azote_provider.save
      redirect_to azote_providers_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /azote_providers/:id/edit
  def edit
  end

  # PATCH/PUT /azote_providers/:id
  def update
    if @azote_provider.update(azote_provider_params)
      redirect_to edit_azote_provider_url(@azote_provider), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /azote_providers/:id
  def destroy
    if @azote_provider.destroy
      redirect_to azote_providers_url, notice: t('shared.destroyed')
    else
      redirect_to azote_providers_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def azote_provider_params
    params.require(:azote_provider).permit(
      :name, :price, :price_currency,
      :pack_amount, :pack_unit
    )
  end

  def set_and_authorize_azote_provider
    @azote_provider = AzoteProvider.find(params[:id])
    authorize @azote_provider
  end
end
