require 'features_helper'

RSpec.feature 'Create new lacquering_type' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_lacquering_type_button) { t('lacquering_types.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }
    given!(:machine) { create(:machine) }

    background do
      login admin
      visit lacquering_types_path(locale: I18n.locale)
      click_on add_lacquering_type_button
    end

    context 'With valid data' do
      scenario 'Admin creates new lacquering_type' do
        fill_in 'lacquering_type_value', with: 'test1234'
        select machine.name, from: 'lacquering_type_machine_id'
        click_on create_button

        expect(current_path).to eq(lacquering_types_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#lacquering_type_#{LacqueringType.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new lacquering_type' do
        click_on create_button
        expect(current_path).to eq(lacquering_types_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
