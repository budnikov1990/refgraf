class OrderForm
  include ActiveModel::Model
  include Virtus.model

  attr_reader :order

  attribute :name, String
  attribute :client_id, Integer
  attribute :number, String
  attribute :amount, Integer
  attribute :width, Float
  attribute :height, Float
  attribute :template_id, Integer
  attribute :color_preset_id, Integer

  attribute :product_structure_id, Integer

  validates :name, presence: true
  validates :client, :client_id, presence: true
  validates :number, presence: true

  # @todo Try to avoid this duplication
  validates :template, :template_id, presence: true
  validates :color_preset, :color_preset_id, presence: true
  validates :product_structure, :product_structure_id, presence: true

  validates :amount, presence: true, numericality: { greater_than: 0 }
  validates :width, presence: true, numericality: { greater_than: 0 }
  validates :height, presence: true, numericality: { greater_than: 0 }

  delegate :id, :persisted?, to: :order

  def initialize(options = {})
    @order = options[:order] || Order.new
    @user = options[:user]
    apply_order if @order
    @order_action = @order.persisted? ? 'order.update' : 'order.create'
    super(options[:params])
  end

  def save
    if valid?
      persist!
    else
      false
    end
  end

  def color_preset
    @color_preset ||= ColorPreset.find_by(id: color_preset_id)
  end

  def template
    @template ||= Template.find_by(id: template_id)
  end

  def client
    @client ||= Client.find_by(id: client_id)
  end

  def product_structure
    @product_structure ||= ProductStructure.find_by(id: product_structure_id)
  end

  private

  def persist!
    ActiveRecord::Base.transaction { persist_without_transaction }
    true
  rescue
    false
  end

  def persist_without_transaction
    order.update!(insert_params)
    order.save
    AdditionalParameter.where(order: order).first_or_create!
    log
  end

  def insert_params
    @insert_params = {
      name: name, amount: amount, width: width, height: height,
      template: template, color_preset: color_preset, client: client, number: number,
      product_structure: product_structure
    }
  end

  def apply_order
    attributes.each do |attribute, _|
      public_send("#{attribute}=", order.public_send(attribute))
    end
  end

  def log
    Activity.create!(
      key: @order_action,
      trackable: order,
      owner: @user,
      data: log_params
    )
  end

  def log_params
    {
      id: order.id,
      title: order.name,
      number: order.number
    }
  end
end
