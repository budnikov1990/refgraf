module OrderHelper
  def machines_data_for_select
    Machine
      .all
      .map do |m|
        [
          m.name, m.id,
          data: { use_roto: m.use_roto, use_lacquering_polymer: m.use_lacquering_polymer }
        ]
      end
  end

  def cylinders_data_for_select
    Cylinder.all.map { |c| [c.name, c.id, data: { machine_id: c.machine.id }] }
  end

  def cutting_machines_data_for_select
    CuttingMachine.all.map { |c| [c.name, c.id] }
  end

  def additional_paints_for_select
    Paint.where(paint_type: 'additional').map do |paint|
      [paint.name, paint.id]
    end
  end

  def backgrounds_data_for_select
    Background.joins(:template_background_prices)
              .where("template_background_prices.price_cents > 0")
              .map do |bg|
      [bg.name, bg.id]
    end.uniq
  end

  def backgrounds_data_for_select_all
    Background.all.map do |bg|
      [bg.name, bg.id]
    end.uniq
  end

  def templates_data_for_select
    Template.all.map do |t|t
      ids = [t.background_id, t.background_for_lamination_1_id, t.background_for_lamination_2_id].compact
      if Background.where(id: ids).all?{|bg| bg.template_background_prices.any?{|tbp| tbp.price_cents > 0 } }
        [
          t.name, t.id,
          data: {
            product_structure_id: t.product_structure.id,
            product_structure_name: t.product_structure.name,
            print: t.background.name,
            lamination1: t.background_for_lamination_1_id.present? ? t.background_for_lamination_1.name : nil,
            lamination2: t.background_for_lamination_2_id.present? ? t.background_for_lamination_2.name : nil
          }
        ]
      else
        nil
      end
    end.uniq.compact
  end

  def templates_data_for_select_all
    Template.all.map do |t|
      [
        t.name, t.id,
        data: {
          product_structure_id: t.product_structure.id,
          product_structure_name: t.product_structure.name,
          print: t.background.name,
          lamination1: t.background_for_lamination_1.name,
          lamination2: t.background_for_lamination_2.name
        }
      ]
    end
  end

  def lamination_machine_data_for_select
    LaminationMachine
      .all
      .map { |c| [c.name, c.id, data: { use_lamination_roto: c.use_lamination_roto }] }
  end

  def lamination_cylinders_data_for_select
    LaminationCylinder
      .all.map { |c| [c.value, c.id, data: { machine_id: c.lamination_machine.id }] }
  end

  def lamination_glue_data_for_select
    LaminationGlue.all.map { |c| [c.name, c.id] }
  end

  def lamination_type_data_for_select
    LaminationType.all.map { |c| [c.name, c.id] }
  end

  def lamination_glue_type_data_for_select
    LaminationGlueType
      .all.map { |c| [c.name, c.id, data: { lamination_type_id: c.lamination_type.id }] }
  end

  def lamination_roto_glue_name_data_for_select
    LaminationRotoGlueName
      .all
      .map do |c|
        [c.name, c.id, data: { resin: c.resin_name, hardener: c.hardener_name }]
      end
  end

  def lamination_roto_glue_type_data_for_select
    LaminationRotoGlueType.all.map { |c| [c.name, c.id] }
  end

  def lamination_roto_linearity_data_for_select
    LaminationRotoLinearity.all.map do |c|
      ri = c.lamination_roto_indices.first
      name = c.value

      if ri.present?
        name += " - masa na mokro #{ri.value_wet} [g] na 1 m^2, masa na sucho #{ri.value_dry} [g] na 1 m^2"
      end

      [name, c.id]
    end
  end

  def lamination_roto_preser_data_for_select
    LaminationRotoPreser.all.map { |c| [c.value, c.id] }
  end

  def lamination_glue_system_data_for_select
    LaminationGlueSystem.all.map { |c| [c.name, c.id] }
  end

  def workflow_state_color(order, other_state)
    if order.current_state > other_state
      'bg-green'
    elsif order.current_state == other_state
      'bg-blue'
    end
  end

  def lamination_glue_system_show(target)
    if target.lamination_glue_system_2.present?
      "#{target.lamination_glue_system_1.name} // #{target.lamination_glue_system_2.name}"
    else
      target.lamination_glue_system_1.name
    end
  end

  def show_panton_1?(order)
    order.color_preset.name == 'CMYK_PMS1' || show_panton_2?(order)
  end

  def show_panton_2?(order)
    order.color_preset.name == 'CMYK_PMS2'
  end

  def show_lamination1_roto?(order)
    order.lamination_machine_1.present? &&
      order.lamination_machine_1.use_lamination_roto &&
      order.lamination_glue_system_1.name == 'rozpuszczalnikowy'
  end

  def show_lamination2_roto?(order)
    order.lamination_machine_2.present? &&
      order.lamination_machine_2.use_lamination_roto &&
      order.lamination_glue_system_2.name == 'rozpuszczalnikowy'
  end

  def princess_solventless1?(order)
    order.lamination_machine_1.present? &&
      order.lamination_machine_1.name == 'Princess' &&
      order.lamination_type_1.name == 'Bezrozpuszczalnikowa'
  end

  def princess_solventless2?(order)
    order.lamination_machine_2.present? &&
      order.lamination_machine_2.name == 'Princess' &&
      order.lamination_type_2.name == 'Bezrozpuszczalnikowa'
  end
end
