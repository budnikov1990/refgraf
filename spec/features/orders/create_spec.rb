require 'features_helper'

RSpec.feature 'Create new order' do
  context 'with authenticated user' do
    given(:technologist) { create(:user, role: 'technologist') }
    given!(:product_structure) { create(:product_structure) }
    given!(:template) { create(:template, product_structure: product_structure) }
    given!(:color_preset) { create(:color_preset) }
    given!(:client) { create(:client) }
    given(:add_order_button) { t('orders.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login technologist
      visit orders_path(locale: I18n.locale)
      click_on add_order_button
    end

    context 'With valid data' do
      scenario 'Technologist creates new order' do
        fill_in 'order_form_name', with: 'test1234'
        fill_in 'order_form_number', with: 'test1234'
        fill_in 'order_form_amount', with: 500
        fill_in 'order_form_width', with: 100
        fill_in 'order_form_height', with: 20
        select product_structure.name, from: 'order_form_product_structure_id'
        select template.name, from: 'order_form_template_id'
        select color_preset.name, from: 'order_form_color_preset_id'
        select client.name, from: 'order_form_client_id'
        click_on create_button

        expect(current_path).to eq(orders_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#order_#{Order.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Technologist fails to create a new order' do
        click_on create_button
        expect(current_path).to eq(orders_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
