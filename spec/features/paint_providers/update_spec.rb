require 'features_helper'

RSpec.feature 'Update paint_provider' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:paint_provider) { create(:paint_provider) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_paint_provider_path(paint_provider, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'paint_provider_name', with: 'test1234'
        click_on save_button

        expect(current_path).to eq(edit_paint_provider_path(paint_provider, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        fill_in 'paint_provider_name', with: ''
        click_on save_button
        expect(current_path).to eq(paint_provider_path(paint_provider, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
