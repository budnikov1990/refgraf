require 'rails_helper'

RSpec.describe LaminationGlueType, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:lamination_type_id) }
end
