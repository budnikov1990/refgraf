require 'rails_helper'

RSpec.describe LacqueringPolymer, type: :model do
  it { is_expected.to validate_presence_of(:value) }
  it { is_expected.to validate_presence_of(:machine_id) }
end
