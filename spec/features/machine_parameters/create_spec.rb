require 'features_helper'

RSpec.feature 'Create new machine_parameter' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:machine) { create(:machine) }
    given!(:product_structure) { create(:product_structure) }
    given(:add_machine_parameter_button) { t('machine_parameters.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit machine_parameters_path(locale: I18n.locale)
      click_on add_machine_parameter_button
    end

    context 'With valid data' do
      scenario 'Admin creates new machine_parameter' do
        select machine.name, from: 'machine_parameter_machine_id'
        select product_structure.name, from: 'machine_parameter_product_structure_id'
        click_on create_button

        expect(current_path).to eq(machine_parameters_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#machine_parameter_#{MachineParameter.last.id}" do
          expect(page).to have_content machine.name
          expect(page).to have_content product_structure.name
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new machine_parameter' do
        click_on create_button
        expect(current_path).to eq(machine_parameters_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
