FactoryGirl.define do
  factory :cylinder do
    machine
    name 'M500'
    perimeter 500
  end
end
