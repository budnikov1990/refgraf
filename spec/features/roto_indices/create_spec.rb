require 'features_helper'

RSpec.feature 'Create new roto_index' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_roto_index_button) { t('roto_indices.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }
    given!(:roto_linearity) { create(:roto_linearity) }

    background do
      login admin
      visit roto_indices_path(locale: I18n.locale)
      click_on add_roto_index_button
    end

    context 'With valid data' do
      scenario 'Admin creates new roto_index' do
        fill_in 'roto_index_value_wet', with: 123
        fill_in 'roto_index_value_dry', with: 456
        select roto_linearity.name, from: 'roto_index_roto_linearity_id'
        click_on create_button

        expect(current_path).to eq(roto_indices_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#roto_index_#{RotoIndex.last.id}" do
          expect(page).to have_content '123'
          expect(page).to have_content '456'
          expect(page).to have_content roto_linearity.name
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new roto_index' do
        click_on create_button
        expect(current_path).to eq(roto_indices_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
