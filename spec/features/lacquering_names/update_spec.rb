require 'features_helper'

RSpec.feature 'Update lacquering_name' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lacquering_name) { create(:lacquering_name) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_lacquering_name_path(lacquering_name, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'lacquering_name_value', with: 'test123456'
        click_on save_button

        expect(current_path).to eq(edit_lacquering_name_path(lacquering_name, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        fill_in 'lacquering_name_value', with: ''
        click_on save_button
        expect(current_path).to eq(lacquering_name_path(lacquering_name, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
