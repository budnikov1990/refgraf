require 'features_helper'

RSpec.feature 'Create new acetate_provider' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given(:add_acetate_provider_button) { t('acetate_providers.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit acetate_providers_path(locale: I18n.locale)
      click_on add_acetate_provider_button
    end

    context 'With valid data' do
      scenario 'Admin creates new acetate_provider without cylinders' do
        fill_in 'acetate_provider_name', with: 'test1234'
        click_on create_button

        expect(current_path).to eq(acetate_providers_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#acetate_provider_#{AcetateProvider.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new acetate_provider' do
        click_on create_button
        expect(current_path).to eq(acetate_providers_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
