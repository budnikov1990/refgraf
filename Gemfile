source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5.1'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.15'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# JavaScript libs
gem 'jquery-rails'
gem 'bootstrap-sass'
gem 'font-awesome-rails'
gem 'bootstrap-editable-rails'
gem 'select2-rails'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Puma as the app server
gem 'puma'

# Use Capistrano for deployment
group :development do
  gem 'capistrano', '3.4.0'
  gem 'capistrano-rvm'
  gem 'capistrano-rails'
  gem 'capistrano3-puma'
end

# Testing environment
group :development, :test do
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'launchy'

  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
end

group :test do
  gem 'shoulda-matchers'
  gem 'rspec-its'
  gem 'capybara'
  gem 'poltergeist'
  gem 'database_cleaner'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Better error page for Rack apps
  gem 'better_errors'
  gem 'binding_of_caller'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # Static code analyzer
  gem 'rubocop', require: false

  # Git pre-commit hooks
  gem 'overcommit', require: false

  # Skip assets log
  gem 'quiet_assets'
end

# Use slim as a template markup language
gem 'slim-rails'

# Form helpers
gem 'simple_form'
gem 'cocoon'

# Authentication
gem 'devise'

# Authorization
gem 'pundit'

# Internationalization gems
gem 'rails-i18n' # Global localization rules
gem 'devise-i18n' # Devise translations

# Pagination
gem 'kaminari'
gem 'bootstrap-kaminari-views'
gem 'kaminari-i18n'

# workflow
gem 'workflow'

gem 'virtus'

# PDF generation
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'

# table fields comments
gem 'migration_comments'

# Used to encapsulate application's business logic
gem 'interactor'

gem 'money-rails'

# Calculate the exchange rate using published rates from currencylayer.com
gem 'money-currencylayer-bank'

# Store settings in YML files
gem 'settingslogic'

# cron jobs
gem 'whenever', :require => false
