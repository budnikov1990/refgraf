class BackgroundsController < ApplicationController
  before_action :set_and_authorize_background, only: [:edit, :update, :destroy]

  # GET /backgrounds
  def index
    authorize Background
    @backgrounds = policy_scope(Background)
    @backgrounds = @backgrounds.order(id: :asc)
    @backgrounds = @backgrounds.page(params[:page]).per(10)
  end

  # GET /backgrounds/new
  def new
    authorize Background
    @background = Background.new
  end

  # POST /backgrounds
  def create
    authorize Background
    if @background.update(background_params)
      redirect_to backgrounds_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /backgrounds/:id/edit
  def edit
  end

  # PATCH/PUT /backgrounds/:id
  def update
    if @background.update(background_params)
      redirect_to edit_background_url(@background), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /backgrounds/:id
  def destroy
    if @background.destroy
      redirect_to backgrounds_url, notice: t('shared.destroyed')
    else
      redirect_to backgrounds_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def background_params
    params.require(:background).permit(
      :name, :raw_weight
    )
  end

  def set_and_authorize_background
    @background = Background.find(params[:id])
    authorize @background
  end
end
