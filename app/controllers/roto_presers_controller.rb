class RotoPresersController < ApplicationController
  before_action :set_and_authorize_roto_preser, only: [:edit, :update, :destroy]

  # GET /roto_presers
  def index
    authorize RotoPreser
    @roto_presers = policy_scope(RotoPreser)
    @roto_presers = @roto_presers.order(id: :asc)
    @roto_presers = @roto_presers.page(params[:page]).per(25)
  end

  # GET /roto_presers/new
  def new
    authorize RotoPreser
    @roto_preser = RotoPreser.new
  end

  # POST /roto_presers
  def create
    authorize RotoPreser
    @roto_preser = RotoPreser.new(roto_preser_params)
    if @roto_preser.save
      redirect_to roto_presers_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /roto_presers/:id/edit
  def edit
  end

  # PATCH/PUT /roto_presers/:id
  def update
    if @roto_preser.update(roto_preser_params)
      redirect_to edit_roto_preser_url(@roto_preser), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /roto_presers/:id
  def destroy
    if @roto_preser.destroy
      redirect_to roto_presers_url, notice: t('shared.destroyed')
    else
      redirect_to roto_presers_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def roto_preser_params
    params.require(:roto_preser).permit(:value)
  end

  def set_and_authorize_roto_preser
    @roto_preser = RotoPreser.find(params[:id])
    authorize @roto_preser
  end
end
