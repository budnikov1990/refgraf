require 'rails_helper'

RSpec.describe LaminationGlueTypePolicy do
  subject { described_class }

  context 'By administrator' do
    let(:user) { build(:user, role: 'admin') }

    permissions :index?, :new?, :create? do
      it 'grants access' do
        expect(subject).to permit(user, LaminationGlueType)
      end
    end

    permissions :edit?, :update? do
      let(:record) { build(:lamination_glue_type) }
      let(:user) { build(:user, role: 'admin') }

      it 'grants access' do
        expect(subject).to permit(user, record)
      end
    end

    permissions :destroy? do
      let(:record) { build(:lamination_glue_type) }
      let(:user) { build(:user, role: 'admin') }

      it 'grants access if other user is destroyed' do
        expect(subject).to permit(user, record)
      end
    end
  end

  %w(technologist operator storeman).each do |role|
    context "By #{role}" do
      let(:user) { build(:user, role: role) }
      let(:record) { build(:lamination_glue_type) }

      permissions :index?, :new?, :create? do
        it 'denies access' do
          expect(subject).not_to permit(user, LaminationGlueType)
        end
      end

      permissions :edit?, :update?, :destroy? do
        let(:record) { build(:lamination_glue_type) }

        it 'denies access' do
          expect(subject).not_to permit(user, record)
        end
      end
    end
  end
end
