require 'features_helper'

RSpec.feature 'View list of lamination_machines' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_machine) { create(:lamination_machine) }

    background do
      login admin
      visit lamination_machines_path
    end

    scenario 'User sees list of lamination_machines' do
      within "#lamination_machine_#{lamination_machine.id}" do
        expect(page).to have_content lamination_machine.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
