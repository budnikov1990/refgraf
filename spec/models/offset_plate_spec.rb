require 'rails_helper'

RSpec.describe OffsetPlate, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:machine_id) }
  it { is_expected.to validate_presence_of(:cylinder_id) }
end
