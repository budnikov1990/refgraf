class InitialSetup < ActiveRecord::Migration
  def change  
    # These are extensions that must be enabled in order to support this database
    enable_extension "plpgsql"
    enable_extension "hstore"

    create_table "activities" do |t|
      t.string   "key"
      t.integer  "owner_id"
      t.string   "owner_type"
      t.integer  "trackable_id"
      t.string   "trackable_type"
      t.json     "data",           default: {}, null: false
      t.datetime "created_at",                  null: false
      t.datetime "updated_at",                  null: false
    end

    add_index "activities", ["key"], name: "index_activities_on_key", using: :btree
    add_index "activities", ["owner_type", "owner_id"], name: "index_activities_on_owner_type_and_owner_id", using: :btree
    add_index "activities", ["trackable_type", "trackable_id"], name: "index_activities_on_trackable_type_and_trackable_id", using: :btree

    create_table "additional_parameters" do |t|
      t.integer  "order_id"
      t.boolean  "cant_prepare_materials",         default: false, null: false
      t.text     "cant_prepare_materials_comment", default: "",    null: false
      t.text     "comments_after_print",           default: "",    null: false
      t.float    "wastepaper_incoming"
      t.float    "wastepaper_remaining"
      t.float    "material_for_store_got"
      t.datetime "created_at",                                     null: false
      t.datetime "updated_at",                                     null: false
    end

    add_index "additional_parameters", ["order_id"], name: "index_additional_parameters_on_order_id", using: :btree

    create_table "clients" do |t|
      t.string   "name"
      t.string   "packing_method"
      t.datetime "created_at",     null: false
      t.datetime "updated_at",     null: false
      t.string   "outer_diameter"
    end

    create_table "color_presets" do |t|
      t.datetime "created_at",    null: false
      t.datetime "updated_at",    null: false
      t.string   "name"
      t.integer  "colors_number"
    end

    create_table "cylinders" do |t|
      t.string   "name"
      t.integer  "perimeter"
      t.integer  "machine_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "cylinders", ["machine_id"], name: "index_cylinders_on_machine_id", using: :btree

    create_table "lacquering_aniloxes" do |t|
      t.integer  "machine_id"
      t.string   "value"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "lacquering_aniloxes", ["machine_id"], name: "index_lacquering_aniloxes_on_machine_id", using: :btree

    create_table "lacquering_indices" do |t|
      t.integer  "machine_id"
      t.integer  "lacquering_type_id"
      t.integer  "lacquering_anilox_id"
      t.integer  "lacquering_polymer_id"
      t.float    "coverage_ratio",        default: 0.0
      t.float    "dry_weight",            default: 0.0
      t.float    "wet_weight",            default: 0.0
      t.datetime "created_at",                          null: false
      t.datetime "updated_at",                          null: false
    end

    add_index "lacquering_indices", ["lacquering_anilox_id"], name: "index_lacquering_indices_on_lacquering_anilox_id", using: :btree
    add_index "lacquering_indices", ["lacquering_polymer_id"], name: "index_lacquering_indices_on_lacquering_polymer_id", using: :btree
    add_index "lacquering_indices", ["lacquering_type_id"], name: "index_lacquering_indices_on_lacquering_type_id", using: :btree
    add_index "lacquering_indices", ["machine_id"], name: "index_lacquering_indices_on_machine_id", using: :btree

    create_table "lacquering_names" do |t|
      t.integer  "machine_id"
      t.string   "value"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "lacquering_names", ["machine_id"], name: "index_lacquering_names_on_machine_id", using: :btree

    create_table "lacquering_polymers" do |t|
      t.integer  "machine_id"
      t.string   "value"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "lacquering_polymers", ["machine_id"], name: "index_lacquering_polymers_on_machine_id", using: :btree

    create_table "lacquering_types" do |t|
      t.integer  "machine_id"
      t.string   "value"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "lacquering_types", ["machine_id"], name: "index_lacquering_types_on_machine_id", using: :btree

    create_table "lamination_cylinders" do |t|
      t.float    "value"
      t.integer  "lamination_machine_id"
      t.datetime "created_at",            null: false
      t.datetime "updated_at",            null: false
    end

    add_index "lamination_cylinders", ["lamination_machine_id"], name: "index_lamination_cylinders_on_lamination_machine_id", using: :btree

    create_table "lamination_glue_systems" do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "lamination_glue_types" do |t|
      t.string   "name"
      t.integer  "lamination_type_id"
      t.datetime "created_at",         null: false
      t.datetime "updated_at",         null: false
    end

    add_index "lamination_glue_types", ["lamination_type_id"], name: "index_lamination_glue_types_on_lamination_type_id", using: :btree

    create_table "lamination_glues" do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "lamination_indices" do |t|
      t.integer  "lamination_machine_id"
      t.float    "value"
      t.datetime "created_at",                  null: false
      t.datetime "updated_at",                  null: false
      t.integer  "lamination_glue_system_1_id"
      t.integer  "lamination_glue_system_2_id"
    end

    add_index "lamination_indices", ["lamination_glue_system_1_id"], name: "index_lamination_indices_on_lamination_glue_system_1_id", using: :btree
    add_index "lamination_indices", ["lamination_glue_system_2_id"], name: "index_lamination_indices_on_lamination_glue_system_2_id", using: :btree
    add_index "lamination_indices", ["lamination_machine_id"], name: "index_lamination_indices_on_lamination_machine_id", using: :btree

    create_table "lamination_machines" do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "lamination_roto_glue_names" do |t|
      t.string   "name"
      t.datetime "created_at",    null: false
      t.datetime "updated_at",    null: false
      t.string   "resin_name"
      t.string   "hardener_name"
    end

    create_table "lamination_roto_glue_types" do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "lamination_roto_indices" do |t|
      t.integer  "lamination_roto_linearity_id"
      t.integer  "lamination_roto_glue_type_id"
      t.float    "value_dry"
      t.float    "value_wet"
      t.datetime "created_at",                   null: false
      t.datetime "updated_at",                   null: false
    end

    add_index "lamination_roto_indices", ["lamination_roto_glue_type_id"], name: "index_lamination_roto_indices_on_lamination_roto_glue_type_id", using: :btree
    add_index "lamination_roto_indices", ["lamination_roto_linearity_id"], name: "index_lamination_roto_indices_on_lamination_roto_linearity_id", using: :btree

    create_table "lamination_roto_linearities" do |t|
      t.string   "value"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "lamination_roto_presers" do |t|
      t.integer  "value"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "lamination_types" do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "machine_parameters", comment: "Machine params for product structures" do |t|
      t.integer  "machine_id"
      t.integer  "product_structure_id"
      t.float    "gross_percent",              default: 0.0
      t.float    "good_percent",               default: 0.0
      t.float    "after_lamination_1_percent", default: 0.0
      t.float    "after_lamination_2_percent", default: 0.0
      t.float    "after_cut_percent",          default: 0.0
      t.datetime "created_at",                               null: false
      t.datetime "updated_at",                               null: false
    end

    add_index "machine_parameters", ["machine_id"], name: "index_machine_parameters_on_machine_id", using: :btree
    add_index "machine_parameters", ["product_structure_id"], name: "index_machine_parameters_on_product_structure_id", using: :btree

    create_table "machines" do |t|
      t.string   "name"
      t.datetime "created_at",                          null: false
      t.datetime "updated_at",                          null: false
      t.integer  "calculation_coefficient", default: 0
      t.integer  "max_colors_count",        default: 0
    end

    create_table "orders" do |t|
      t.integer  "template_id"
      t.string   "workflow_state"
      t.integer  "amount"
      t.float    "width"
      t.float    "height"
      t.integer  "color_preset_id"
      t.integer  "machine_id"
      t.integer  "cylinder_id"
      t.integer  "item_count_on_width"
      t.float    "canvas_width"
      t.datetime "created_at",                                       null: false
      t.datetime "updated_at",                                       null: false
      t.string   "name"
      t.float    "ink_amount"
      t.string   "panton_1",                         default: ""
      t.string   "panton_2",                         default: ""
      t.string   "number"
      t.integer  "client_id"
      t.boolean  "lacquering",                       default: false
      t.integer  "lacquering_anilox_id"
      t.integer  "lacquering_name_id"
      t.integer  "lacquering_polymer_id"
      t.integer  "lacquering_type_id"
      t.integer  "lamination_machine_1_id"
      t.integer  "lamination_cylinder_1_id"
      t.integer  "lamination_type_1_id"
      t.integer  "lamination_glue_1_id"
      t.integer  "lamination_glue_system_1_id"
      t.integer  "lamination_glue_type_1_id"
      t.float    "canvas_width_lamination_1"
      t.float    "canvas_width_lamination_2"
      t.integer  "lamination_glue_system_2_id"
      t.integer  "lamination_type_2_id"
      t.integer  "lamination_cylinder_2_id"
      t.integer  "lamination_glue_2_id"
      t.integer  "lamination_glue_type_2_id"
      t.integer  "lamination_machine_2_id"
      t.integer  "lamination_roto_glue_name_1_id"
      t.integer  "lamination_roto_glue_type_1_id"
      t.integer  "lamination_roto_linearity_1_id"
      t.integer  "lamination_roto_preser_1_id"
      t.integer  "lamination_roto_glue_viscosity_1"
      t.float    "lamination_roto_acetate_1"
      t.float    "lamination_roto_resin_1"
      t.float    "lamination_roto_hardener_1"
      t.integer  "lamination_roto_glue_name_2_id"
      t.integer  "lamination_roto_glue_type_2_id"
      t.integer  "lamination_roto_linearity_2_id"
      t.integer  "lamination_roto_preser_2_id"
      t.integer  "lamination_roto_glue_viscosity_2"
      t.float    "lamination_roto_acetate_2"
      t.float    "lamination_roto_resin_2"
      t.float    "lamination_roto_hardener_2"
    end

    add_index "orders", ["color_preset_id"], name: "index_orders_on_color_preset_id", using: :btree
    add_index "orders", ["cylinder_id"], name: "index_orders_on_cylinder_id", using: :btree
    add_index "orders", ["machine_id"], name: "index_orders_on_machine_id", using: :btree
    add_index "orders", ["template_id"], name: "index_orders_on_template_id", using: :btree

    create_table "product_structures" do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string   "uid"
    end

    add_index "product_structures", ["uid"], name: "index_product_structures_on_uid", using: :btree

    create_table "templates" do |t|
      t.text     "name"
      t.integer  "product_structure_id"
      t.datetime "created_at",                                         null: false
      t.datetime "updated_at",                                         null: false
      t.float    "raw_material_weight",                  default: 0.0
      t.float    "raw_material_weight_for_lamination_1", default: 0.0
      t.float    "raw_material_weight_for_lamination_2", default: 0.0
      t.string   "background_for_print",                 default: ""
      t.string   "background_for_lamination_1",          default: ""
      t.string   "background_for_lamination_2",          default: ""
      t.string   "lamination_scheme"
    end

    add_index "templates", ["product_structure_id"], name: "index_templates_on_product_structure_id", using: :btree

    create_table "users" do |t|
      t.string   "email",                  default: "", null: false
      t.string   "encrypted_password",     default: "", null: false
      t.string   "reset_password_token"
      t.datetime "reset_password_sent_at"
      t.datetime "remember_created_at"
      t.integer  "sign_in_count",          default: 0,  null: false
      t.datetime "current_sign_in_at"
      t.datetime "last_sign_in_at"
      t.inet     "current_sign_in_ip"
      t.inet     "last_sign_in_ip"
      t.string   "role"
      t.datetime "created_at",                          null: false
      t.datetime "updated_at",                          null: false
      t.string   "first_name"
      t.string   "last_name"
    end

    add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
    add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

    add_foreign_key "additional_parameters", "orders"
    add_foreign_key "cylinders", "machines"
    add_foreign_key "lacquering_aniloxes", "machines"
    add_foreign_key "lacquering_indices", "lacquering_aniloxes"
    add_foreign_key "lacquering_indices", "lacquering_polymers"
    add_foreign_key "lacquering_indices", "lacquering_types"
    add_foreign_key "lacquering_indices", "machines"
    add_foreign_key "lacquering_names", "machines"
    add_foreign_key "lacquering_polymers", "machines"
    add_foreign_key "lacquering_types", "machines"
    add_foreign_key "lamination_cylinders", "lamination_machines"
    add_foreign_key "lamination_glue_types", "lamination_types"
    add_foreign_key "lamination_indices", "lamination_machines"
    add_foreign_key "lamination_roto_indices", "lamination_roto_glue_types"
    add_foreign_key "lamination_roto_indices", "lamination_roto_linearities"
    add_foreign_key "machine_parameters", "machines"
    add_foreign_key "machine_parameters", "product_structures"
    add_foreign_key "orders", "color_presets"
    add_foreign_key "orders", "cylinders"
    add_foreign_key "orders", "machines"
    add_foreign_key "orders", "templates"
    add_foreign_key "templates", "product_structures"
  end
end
