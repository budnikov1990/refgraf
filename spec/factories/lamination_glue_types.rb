FactoryGirl.define do
  factory :lamination_glue_type do
    lamination_type
    name 'test type glue'
  end
end
