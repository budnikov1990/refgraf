class AzoteProvider < ActiveRecord::Base
  has_many :orders, dependent: :restrict_with_error

  monetize :price_cents

  validates :name, presence: true
  validates :price, presence: true
  validates :price_currency, presence: true
end
