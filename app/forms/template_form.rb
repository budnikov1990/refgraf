class TemplateForm
  attr_reader :template, :attrs

  def initialize(options={})
    @template = options.fetch(:template, build_template)
    @attrs = options.fetch(:attrs, {})
    @template.assign_attributes(attrs)
    build_nested if template.machine_template_spendings.length.eql?(0) || template.machine_template_speeds.length.eql?(0)
  end

  def save
    @template.save
  end

  private

  def build_template
    Template.new(template_attrs)
  end

  def build_nested
    template.assign_attributes(template_attrs)
  end

  def template_attrs
    {
      machine_template_speeds_attributes: Machine.all.map do |machine|
        { machine_id: machine.id }
      end,
      machine_template_spendings_attributes: Machine.all.map do |machine|
        { machine_id: machine.id }
      end
    }
  end
end
