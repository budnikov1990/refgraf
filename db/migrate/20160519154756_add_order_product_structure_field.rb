class AddOrderProductStructureField < ActiveRecord::Migration
  def change
    add_column :orders, :product_structure_id, :integer
  end
end
