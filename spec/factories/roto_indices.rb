FactoryGirl.define do
  factory :roto_index do
    value_dry 1.5
    value_wet 1.5
    roto_linearity
  end
end
