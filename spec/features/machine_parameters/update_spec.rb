require 'features_helper'

RSpec.feature 'Update machine_parameter' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:machine_parameter) { create(:machine_parameter) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_machine_parameter_path(machine_parameter, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'machine_parameter_gross_percent', with: 5
        click_on save_button

        expect(current_path).to eq(edit_machine_parameter_path(machine_parameter, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        select '', from: 'machine_parameter_product_structure_id'
        click_on save_button
        expect(current_path).to eq(machine_parameter_path(machine_parameter, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
