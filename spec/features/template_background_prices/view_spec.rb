require 'features_helper'

RSpec.feature 'View list of template_background_prices' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:template_background_price) { create(:template_background_price) }

    background do
      login admin
      visit template_background_prices_path
    end

    scenario 'User sees list of template_background_prices' do
      within "#template_background_price_#{template_background_price.id}" do
        expect(page).to have_content template_background_price.template_background_provider.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
