require 'features_helper'

RSpec.feature 'Destroy azote_provider' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:azote_provider) { create(:azote_provider) }

    background do
      login admin
      visit azote_providers_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the azote_provider', js: true do
      within "#azote_provider_#{azote_provider.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(azote_providers_path(locale: I18n.locale))
      expect(page).to have_no_selector("#azote_provider_#{azote_provider.id}")
    end
  end
end
