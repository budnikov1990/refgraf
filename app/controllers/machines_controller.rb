class MachinesController < ApplicationController
  before_action :set_and_authorize_machine, only: [:edit, :update, :destroy]

  # GET /machines
  def index
    authorize Machine
    @machines = policy_scope(Machine)
    @machines = @machines.order(id: :asc)
    @machines = @machines.page(params[:page]).per(25)
  end

  # GET /machines/new
  def new
    authorize Machine
    @machine = Machine.new
  end

  # POST /machines
  def create
    authorize Machine
    @machine = Machine.new(machine_params)
    if @machine.save
      redirect_to machines_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /machines/:id/edit
  def edit
  end

  # PATCH/PUT /machines/:id
  def update
    if @machine.update(machine_params)
      redirect_to edit_machine_url(@machine), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /machines/:id
  def destroy
    if @machine.destroy
      redirect_to machines_url, notice: t('shared.destroyed')
    else
      redirect_to machines_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def machine_params
    params.require(:machine).permit(
      :name, :calculation_coefficient, :max_colors_count,
      :use_roto, :use_lacquering_polymer,
      :price_clear, :price_clear_currency,
      :hour_cost, :hour_cost_currency,
      :gum_cleaning_202, :gum_cleaning_204, :gum_cleaning_101_203,
      cylinders_attributes: [:id, :name, :perimeter, :_destroy]
    )
  end

  def set_and_authorize_machine
    @machine = Machine.find(params[:id])
    authorize @machine
  end
end
