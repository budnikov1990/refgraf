FactoryGirl.define do
  factory :lamination_index do
    lamination_glue_system_1_id nil
    lamination_glue_system_2_id nil
    lamination_machine
    value 1.5
  end
end
