class AddOrderRotoFields < ActiveRecord::Migration
  def change
    add_column :orders, :roto, :boolean, default: false
    add_column :orders, :roto_linearity_id, :integer
    add_column :orders, :roto_preser_id, :integer
    add_column :orders, :roto_paint_id, :integer
  end
end
