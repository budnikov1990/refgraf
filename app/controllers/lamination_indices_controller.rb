class LaminationIndicesController < ApplicationController
  before_action :set_and_authorize_lamination_index, only: [:edit, :update, :destroy]

  # GET /lamination_indices
  def index
    authorize LaminationIndex
    @lamination_indices = policy_scope(LaminationIndex)
    @lamination_indices = @lamination_indices.order(id: :asc)
    @lamination_indices = @lamination_indices.page(params[:page]).per(25)
  end

  # GET /lamination_indices/new
  def new
    authorize LaminationIndex
    @lamination_index = LaminationIndex.new
  end

  # POST /lamination_indices
  def create
    authorize LaminationIndex
    @lamination_index = LaminationIndex.new(lamination_index_params)
    if @lamination_index.save
      redirect_to lamination_indices_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lamination_indices/:id/edit
  def edit
  end

  # PATCH/PUT /lamination_indices/:id
  def update
    if @lamination_index.update(lamination_index_params)
      redirect_to edit_lamination_index_url(@lamination_index),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lamination_indices/:id
  def destroy
    if @lamination_index.destroy
      redirect_to lamination_indices_url, notice: t('shared.destroyed')
    else
      redirect_to lamination_indices_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lamination_index_params
    params.require(:lamination_index).permit(
      :value, :lamination_glue_system_1_id, :lamination_glue_system_2_id,
      :lamination_machine_id, :vpzp
    )
  end

  def set_and_authorize_lamination_index
    @lamination_index = LaminationIndex.find(params[:id])
    authorize @lamination_index
  end
end
