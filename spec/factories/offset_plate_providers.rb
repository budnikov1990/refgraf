FactoryGirl.define do
  factory :offset_plate_provider do
    name 'test'

    transient do
      offset_plate_prices_count 1
    end

    after :build do |offset_plate_provider, evaluator|
      evaluator.offset_plate_prices_count.times do
        offset_plate = create(:offset_plate)
        offset_plate_provider.offset_plate_prices.build(
          attributes_for(:offset_plate_price, offset_plate_provider: offset_plate_provider, offset_plate_id: offset_plate.id)
        )
      end
    end
  end
end
