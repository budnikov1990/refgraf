class OrderFinishCutUpdate
  include Interactor
  include TransactionalInteractor

  def call
    context.order.finish_cut!
    log
  end

  private

  delegate :params, :user, :order, to: :context

  def log
    Activity.create!(
      key: 'order.finish_cut',
      trackable: order,
      owner: user,
      data: log_params
    )
  end

  def log_params
    {
      id: context.order.id,
      title: context.order.name,
      number: context.order.number,
      previous_state: 'cutting',
      new_state: 'cut'
    }
  end
end
