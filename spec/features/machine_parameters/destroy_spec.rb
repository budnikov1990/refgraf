require 'features_helper'

RSpec.feature 'Destroy machine_parameter' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:machine_parameter) { create(:machine_parameter) }

    background do
      login admin
      visit machine_parameters_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the machine_parameter', js: true do
      within "#machine_parameter_#{machine_parameter.id}" do
        find('.dropdown-toggle').click
        find('.fa-trash').click
      end
      expect(current_path).to eq(machine_parameters_path(locale: I18n.locale))
      expect(page).to have_no_selector("#machine_parameter_#{machine_parameter.id}")
    end
  end
end
