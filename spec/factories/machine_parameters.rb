FactoryGirl.define do
  factory :machine_parameter do
    machine
    product_structure
    gross_percent 1
    good_percent 1
    after_lamination_1_percent 1
    after_lamination_2_percent 2
    after_cut_percent 1.5
  end
end
