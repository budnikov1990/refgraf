require 'features_helper'

RSpec.feature 'Create new client' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_client_button) { t('clients.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit clients_path(locale: I18n.locale)
      click_on add_client_button
    end

    context 'With valid data' do
      scenario 'Admin creates new client' do
        fill_in 'client_name', with: 'test1234'
        fill_in 'client_packing_method', with: 'test1234'
        click_on create_button

        expect(current_path).to eq(clients_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#client_#{Client.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new client' do
        click_on create_button
        expect(current_path).to eq(clients_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
