FactoryGirl.define do
  factory :acetate_provider do
    name 'test'

    transient do
      acetate_names_count 1
    end

    after :build do |acetate_provider, evaluator|
      evaluator.acetate_names_count.times do
        acetate_provider.acetate_names.build(
          attributes_for(:acetate_name, acetate_provider: acetate_provider)
        )
      end
    end
  end
end
