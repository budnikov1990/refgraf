class TemplateBackgroundPricesController < ApplicationController
  before_action :set_and_authorize_template_background_price,
                only: [:edit, :update, :destroy]

  # GET /template_background_prices
  def index
    authorize TemplateBackgroundPrice
    @template_background_prices = policy_scope(TemplateBackgroundPrice)
    @template_background_prices = @template_background_prices.order(id: :asc)
    @template_background_prices =
      @template_background_prices.page(params[:page]).per(25)
  end

  # GET /template_background_prices/new
  def new
    authorize TemplateBackgroundPrice
    @template_background_price = TemplateBackgroundPrice.new
  end

  # POST /template_background_prices
  def create
    authorize TemplateBackgroundPrice
    @template_background_price =
      TemplateBackgroundPrice.new(template_background_price_params)
    if @template_background_price.save
      redirect_to template_background_prices_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /template_background_prices/:id/edit
  def edit
  end

  # PATCH/PUT /template_background_prices/:id
  def update
    if @template_background_price.update(template_background_price_params)
      redirect_to edit_template_background_price_url(@template_background_price),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /template_background_prices/:id
  def destroy
    if @template_background_price.destroy
      redirect_to template_background_prices_url, notice: t('shared.destroyed')
    else
      redirect_to template_background_prices_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def template_background_price_params
    params.require(:template_background_price).permit(
      :template_background_provider_id, :background_id,
      :price, :price_currency, :price_type,
      :pack_amount, :pack_unit
    )
  end

  def set_and_authorize_template_background_price
    @template_background_price = TemplateBackgroundPrice.find(params[:id])
    authorize @template_background_price
  end
end
