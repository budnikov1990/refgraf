require 'features_helper'

RSpec.feature 'View list of lamination_roto_glue_types' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_roto_glue_type) { create(:lamination_roto_glue_type) }

    background do
      login admin
      visit lamination_roto_glue_types_path
    end

    scenario 'User sees list of lamination_roto_glue_types' do
      within "#lamination_roto_glue_type_#{lamination_roto_glue_type.id}" do
        expect(page).to have_content lamination_roto_glue_type.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
