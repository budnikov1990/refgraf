require 'features_helper'

RSpec.feature 'Destroy lamination_roto_index' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_roto_index) { create(:lamination_roto_index) }

    background do
      login admin
      visit lamination_roto_indices_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lamination_roto_index', js: true do
      within "#lamination_roto_index_#{lamination_roto_index.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lamination_roto_indices_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lamination_roto_index_#{lamination_roto_index.id}")
    end
  end
end
