require 'rails_helper'

RSpec.describe RotoIndex, type: :model do
  it { is_expected.to validate_presence_of(:value_wet) }
  it { is_expected.to validate_presence_of(:value_dry) }
  it { is_expected.to validate_presence_of(:roto_linearity_id) }
end
