class OrderPreparedUpdate
  include Interactor
  include TransactionalInteractor

  def call
    context.order.additional_parameter.update_attributes(cant_prepare_materials: false)
    context.order.prepare_materials!
    log
  end

  private

  delegate :params, :user, :order, to: :context

  def log
    Activity.create!(
      key: 'order.prepared',
      trackable: order,
      owner: user,
      data: log_params
    )
  end

  def log_params
    {
      id: context.order.id,
      title: context.order.name,
      number: context.order.number,
      previous_state: 'new',
      new_state: 'in_preparation'
    }
  end
end
