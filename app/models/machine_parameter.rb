class MachineParameter < ActiveRecord::Base
  belongs_to :machine
  belongs_to :product_structure

  validates :machine_id, presence: true
  validates :product_structure_id, presence: true
  validates :gross_percent, presence: true,
                            numericality: {
                              greater_than_or_equal_to: 0,
                              less_than_or_equal_to: 100
                            }
  validates :good_percent, presence: true,
                           numericality: {
                             greater_than_or_equal_to: 0,
                             less_than_or_equal_to: 100
                           }
  validates :after_lamination_1_percent, presence: true,
                                         numericality: {
                                           greater_than_or_equal_to: 0,
                                           less_than_or_equal_to: 100
                                         }
  validates :after_lamination_2_percent, presence: true,
                                         numericality: {
                                           greater_than_or_equal_to: 0,
                                           less_than_or_equal_to: 100
                                         }
  validates :after_cut_percent, presence: true,
                                numericality: {
                                  greater_than_or_equal_to: 0,
                                  less_than_or_equal_to: 100
                                }

  def self.params(machine, product_structure)
    where(machine: machine, product_structure: product_structure)
  end
end
