FactoryGirl.define do
  factory :lacquering_anilox do
    machine
    value '100 l/cm'
  end
end
