class ColorPreset < ActiveRecord::Base
  has_many :orders, dependent: :restrict_with_error

  validates :name, presence: true, uniqueness: true
  validates :colors_number,
            presence: true,
            numericality: { greater_than_or_equal_to: 0 }
  validates :offset_plates_count,
            presence: true,
            numericality: { greater_than_or_equal_to: 0 }
end
