require 'features_helper'

RSpec.feature 'View list of lacquering_types' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lacquering_type) { create(:lacquering_type) }

    background do
      login admin
      visit lacquering_types_path
    end

    scenario 'User sees list of lacquering_types' do
      within "#lacquering_type_#{lacquering_type.id}" do
        expect(page).to have_content lacquering_type.value
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
