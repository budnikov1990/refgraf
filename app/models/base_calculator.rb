class BaseCalculator
  def initialize(params)
    @order = params[:order]
    @working_width ||= (@order.width * @order.item_count_on_width) / 1000
    @canvas_width ||= @order.canvas_width.to_d / 1000
    @machine_params = MachineParameter.params(
      @order.machine,
      @order.product_structure
    ).first
  end

  def netto
    @netto ||= ((@order.amount * 1000) / netto_part).round(2)
  end

  def amount_mb
    @amount_mb ||= (
      (netto * @machine_params.gross_percent) +
      @order.machine.calculation_coefficient +
      netto
    ).round(1)
  end

  def after_cutting_mb
    @after_cutting_mb ||= before_cutting_mb - (netto * cutting_index.value)
  end

  def cutting_index
    CuttingIndex.find_by(cutting_machine_id: @order.cutting_machine_id, product_structure_id: @order.product_structure_id)
  end

  def paint_brutto
    @paint_brutto ||= (
      base_ink_weight +
      panton_1_weight +
      panton_2_weight
    ).round(1)
  end

  def base_ink_weight
    @base_ink_weight ||= (amount_mb * @order.ink_amount * @working_width) / 1000
  end

  def panton_1_weight
    @panton_1_weight ||= (amount_mb * @order.panton_1_amount * @working_width) / 1000
  end

  def panton_2_weight
    @panton_2_weight ||= (amount_mb * @order.panton_2_amount * @working_width) / 1000
  end

  def base_print_time
    @base_print_time ||= amount_mb.to_d / @order.machine_template_speed.vpbp
  end

  def lamination_time
    @lamination_time ||= lamination_mb.to_d / lamination_index_model.vpzp
  end

  def threads_after_print
    @threads_after_print ||=
      if @order.amount < 1000
        (amount_mb.to_d / @order.machine_template_spending.thousand_less).ceil
      else
        (amount_mb.to_d / @order.machine_template_spending.thousand_more).ceil
      end
  end

  def calculated_time_202
    @calculated_time_202 ||= threads_after_print * @order.machine.gum_cleaning_202
  end

  def calculated_time_204
    @calculated_time_204 ||= threads_after_print * @order.machine.gum_cleaning_204
  end

  def calculated_time_101_203
    @calculated_time_101_203 ||= @order.color_preset.offset_plates_count * @order.machine.gum_cleaning_101_203
  end

  def order_estimate_time
    @order_estimate_time ||= base_print_time + calculated_time_202 + calculated_time_204 + calculated_time_101_203
  end

  def item_count_on_width_one_meter
    @item_count_on_width_one_meter ||= (1000 / @order.height).round(3)
  end

  def packages_amount_sum
    @packages_amount_sum ||= (
      material_after_cut * item_count_on_width_one_meter * @order.item_count_on_width
    ).round(3)
  end

  def weight
    @weight ||= (
    (amount_mb * @canvas_width * @order.template.background.raw_weight) / 1000
    ).round(1)
  end

  def area
    @area ||= (
    amount_mb * @canvas_width
    ).round(1)
  end

  def roto_paint_dry
    @roto_paint_dry ||=
      (
        amount_mb *
        (@order.roto_preser.value.to_f / 1000) *
        (roto_index_dry / 1000)
      ).round(1)
  end

  def roto_paint_wet
    @roto_paint_wet ||=
      (
        amount_mb *
        (@order.roto_preser.value.to_f / 1000) *
        (roto_index_wet / 1000)
      ).round(1)
  end

  def roto_paint_wet_percentage
    @roto_paint_wet_percentage ||= roto_paint_wet * (@order.roto_paint_percentage.to_d / 100)
  end

  def roto_acetate_wet_percentage
    @roto_acetate_wet_percentage ||= roto_paint_wet * (@order.roto_acetate_percentage.to_d / 100)
  end

  def roto_index_dry
    @roto_index_dry ||=
      roto_index.present? && @order.roto ? roto_index.value_dry.to_f : 0
  end

  def roto_index_wet
    @roto_index_wet ||=
      roto_index.present? && @order.roto ? roto_index.value_wet.to_f : 0
  end

  def lacquering_dry_weight
    @lacquering_dry_weight ||=
      lacquering.present? ? lacquering.dry_weight.to_f : 0
  end

  def lacquering_wet_weight
    @lacquering_wet_weight ||=
      lacquering.present? ? lacquering.wet_weight.to_f : 0
  end

  def lacquering_coverage_ratio
    @lacquering_coverage_ratio ||=
      lacquering.present? ? lacquering.coverage_ratio.to_f : 0
  end

  def lacquer_dry_weight
    @lacquer_dry_weight ||=
      if @order.machine.use_lacquering_polymer
        (
          amount_mb *
          (@order.lacquering_polymer.value.to_f / 1000) *
          (lacquering_dry_weight / 1000)
        ).round(1)
      else
        0
      end
  end

  def lacquer_wet_weight
    @lacquer_wet_weight ||=
      if @order.machine.use_lacquering_polymer
        (
          amount_mb *
          (@order.lacquering_polymer.value.to_f / 1000) *
          (lacquering_wet_weight / 1000)
        ).round(1)
      else
        0
      end
  end

  def lacquer_paint
    @lacquer_paint ||=
      if !@order.machine.use_lacquering_polymer && @order.lacquering
        (
          amount_mb *
          @working_width *
          (lacquering_coverage_ratio / 1000)
        ).round(1)
      else
        0
      end
  end

  private

  def lacquering_index
    if @order.lacquering
      if @order.machine.use_lacquering_polymer
        lacquering_dry_weight
      else
        lacquering_coverage_ratio
      end
    else
      0
    end
  end

  def lacquering
    @lacquering ||= LacqueringIndex.find_by(find_lacquering_params)
  end

  def roto_index
    @roto_index ||= RotoIndex.find_by(roto_linearity: @order.roto_linearity)
  end

  def find_lacquering_params
    @find_lacquering_params ||= {
      lacquering_type: @order.lacquering_type,
      lacquering_anilox: @order.lacquering_anilox,
      lacquering_polymer: @order.lacquering_polymer,
      machine: @order.machine
    }
  end
end
