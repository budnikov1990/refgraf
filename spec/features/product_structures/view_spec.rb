require 'features_helper'

RSpec.feature 'View list of sctructures' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:product_structure) { create(:product_structure) }

    background do
      login admin
      visit product_structures_path
    end

    scenario 'User sees list of structures' do
      within "#product_structure_#{product_structure.id}" do
        expect(page).to have_content product_structure.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
