class AddMachinesClearPrice < ActiveRecord::Migration
  def change
    change_table :machines do |t|
      t.monetize :price_clear
    end
  end
end
