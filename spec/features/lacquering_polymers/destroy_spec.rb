require 'features_helper'

RSpec.feature 'Destroy lacquering_polymer' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lacquering_polymer) { create(:lacquering_polymer) }

    background do
      login admin
      visit lacquering_polymers_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lacquering_polymer', js: true do
      within "#lacquering_polymer_#{lacquering_polymer.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lacquering_polymers_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lacquering_polymer_#{lacquering_polymer.id}")
    end
  end
end
