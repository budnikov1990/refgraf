require 'features_helper'

RSpec.feature 'View list of lacquering_aniloxes' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lacquering_anilox) { create(:lacquering_anilox) }

    background do
      login admin
      visit lacquering_aniloxes_path
    end

    scenario 'User sees list of lacquering_aniloxs' do
      within "#lacquering_anilox_#{lacquering_anilox.id}" do
        expect(page).to have_content lacquering_anilox.value
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
