require 'rails_helper'

RSpec.describe LaminationIndex, type: :model do
  it { is_expected.to validate_presence_of(:value) }
  it { is_expected.to validate_presence_of(:lamination_machine_id) }
end
