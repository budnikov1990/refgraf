require 'features_helper'

RSpec.feature 'Create new lamination_machine' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_lamination_machine_button) { t('lamination_machines.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit lamination_machines_path(locale: I18n.locale)
      click_on add_lamination_machine_button
    end

    context 'With valid data' do
      scenario 'Admin creates new lamination_machine without cylinders' do
        fill_in 'lamination_machine_name', with: 'test1234'
        click_on create_button

        expect(current_path).to eq(lamination_machines_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#lamination_machine_#{LaminationMachine.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new lamination_machine' do
        click_on create_button
        expect(current_path).to eq(lamination_machines_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
