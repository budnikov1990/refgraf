class ActualizeOrderForm
  include ActiveModel::Model
  include Virtus.model

  attr_reader :order

  attribute :machine_id, Integer
  attribute :cylinder_id, Integer
  attribute :cutting_machine_id, Integer
  attribute :item_count_on_width, Integer
  attribute :canvas_width, Integer
  attribute :ink_amount, Float
  attribute :panton_1_paint_id, Integer
  attribute :panton_2_paint_id, Integer
  attribute :panton_1_amount, Float
  attribute :panton_2_amount, Float

  attribute :lacquering, Boolean
  attribute :lacquering_anilox_id, Integer
  attribute :lacquering_polymer_id, Integer
  attribute :lacquering_type_id, Integer
  attribute :lacquering_name_id, Integer

  attribute :roto, Boolean
  attribute :roto_preser_id, Integer
  attribute :roto_paint_id, Integer
  attribute :roto_acetate_name_id, Integer
  attribute :roto_linearity_id, Integer
  attribute :roto_paint_percentage, Integer
  attribute :roto_acetate_percentage, Integer

  validate :check_canvas_width
  validates :machine, :machine_id, presence: true
  validates :cutting_machine, :cutting_machine_id, presence: true
  validates :cylinder, :cylinder_id, presence: true
  validates :item_count_on_width, presence: true, numericality: { greater_than: 0 }
  validates :canvas_width, presence: true, numericality: { greater_than: 0 }
  validates :ink_amount, presence: true,
                         numericality: {
                           greater_than_or_equal_to: 0,
                           less_than_or_equal_to: 10
                         }
  validates :panton_1_paint_id, presence: true, if: :check_panton_1?
  validates :panton_2_paint_id, presence: true, if: :check_panton_2?

  validates :lacquering_name, :lacquering_name_id, presence: true, if: :lacquering?
  validates :lacquering_type, :lacquering_type_id, presence: true, if: :lacquering?
  validates :lacquering_polymer, :lacquering_polymer_id, presence: true,
                                                         if: :lacquering_with_polymer?

  validates :roto_preser, :roto_preser_id, presence: true, if: :roto?
  validates :roto_paint, :roto_paint_id, presence: true, if: :roto?
  validates :roto_linearity, :roto_linearity_id, presence: true, if: :roto?

  def initialize(options = {})
    @order = options[:order]
    @user = options[:user]
    apply_order
    super(options[:params])
  end

  def save
    if valid?
      persist!
    else
      false
    end
  end

  private

  def persist!
    ActiveRecord::Base.transaction { persist_without_transaction }
    true
  rescue
    false
  end

  def persist_without_transaction
    new_params = insert_params.merge(lamination_params)
    new_params = new_params.merge(lacquering_params)
    new_params = new_params.merge(roto_params)
    order.update!(new_params)
    order.actualize!
    log
  end

  def insert_params
    @insert_params = {
      machine: machine, cylinder: cylinder, cutting_machine: cutting_machine,
      item_count_on_width: item_count_on_width, canvas_width: canvas_width,
      roto_paint_percentage: roto_paint_percentage, roto_acetate_percentage: roto_acetate_percentage,
      ink_amount: ink_amount, lacquering: lacquering,
      panton_1_amount: panton_1_amount, panton_2_amount: panton_2_amount,
      panton_1_paint: panton_1_paint, panton_2_paint: panton_2_paint
    }
  end

  def lacquering_params
    @lacquering_params ||= {
      lacquering_name: lacquering_name,
      lacquering_anilox: lacquering_anilox, lacquering_type: lacquering_type,
      lacquering_polymer: lacquering_polymer
    }
  end

  def roto_params
    @roto_params ||= {
      roto_preser: roto_preser, roto_linearity: roto_linearity,
      roto_paint: roto_paint, roto: roto, roto_acetate_name: roto_acetate_name
    }
  end

  def machine
    @machine ||= Machine.find_by(id: machine_id)
  end

  def cutting_machine
    @cutting_machine ||= CuttingMachine.find_by(id: cutting_machine_id)
  end

  def cylinder
    @cylinder ||= Cylinder.find_by(id: cylinder_id)
  end

  def panton_1_paint
    @panton_1_paint ||= Paint.find_by(id:panton_1_paint_id)
  end

  def panton_2_paint
    @panton_2_paint ||= Paint.find_by(id: panton_2_paint_id)
  end

  def lacquering_name
    @lacquering_name ||= Paint.find_by(id: lacquering_name_id)
  end

  def lacquering_anilox
    @lacquering_anilox ||= LacqueringAnilox.find_by(id: lacquering_anilox_id)
  end

  def lacquering_type
    @lacquering_type ||= LacqueringType.find_by(id: lacquering_type_id)
  end

  def lacquering_polymer
    @lacquering_polymer ||= LacqueringPolymer.find_by(id: lacquering_polymer_id)
  end

  def roto_linearity
    @roto_linearity ||= RotoLinearity.find_by(id: roto_linearity_id)
  end

  def roto_preser
    @roto_preser ||= RotoPreser.find_by(id: roto_preser_id)
  end

  def roto_paint
    @roto_paint ||= Paint.find_by(id: roto_paint_id)
  end

  def roto_acetate_name
    @roto_acetate_name ||= AcetateName.find_by(id: roto_acetate_name_id)
  end

  def apply_order
    attributes.each do |attribute, _|
      public_send("#{attribute}=", order.public_send(attribute))
    end
  end

  def lacquering?
    lacquering
  end

  def roto?
    roto
  end

  def machine_for_polymer?
    !machine.blank? && machine.use_lacquering_polymer
  end

  def lacquering_with_polymer?
    lacquering? && machine_for_polymer?
  end

  def check_panton_1?
    order.color_preset.name == 'CMYK_PMS1' || check_panton_2?
  end

  def check_panton_2?
    order.color_preset.name == 'CMYK_PMS2'
  end

  def check_canvas_width
    if canvas_width.present? && item_count_on_width.present?
      counted_canvas_width = order.width * item_count_on_width
      if canvas_width < counted_canvas_width
        errors.add(:canvas_width,
                   I18n.t('shared.errors.not_suitable_canvas_width',
                          p: counted_canvas_width.to_i
                         )
                  )
      end
    end
  end

  def log
    Activity.create!(
      key: 'order.actualize',
      trackable: order,
      owner: @user,
      data: log_params
    )
  end

  def log_params
    {
      id: order.id,
      title: order.name,
      number: order.number
    }
  end
end
