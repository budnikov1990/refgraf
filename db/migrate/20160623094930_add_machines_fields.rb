class AddMachinesFields < ActiveRecord::Migration
  def change
    add_column :machines, :use_azote, :boolean, default: false
    add_column :machines, :azote_count, :integer
  end
end
