class CreateAcetateProviders < ActiveRecord::Migration
  def change
    create_table :acetate_providers do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
