FactoryGirl.define do
  factory :template_background_price do
    template
    template_background_provider
    background_print_price 12
    background_print_pack_amount 1
    background_print_pack_unit 'l'
    background_lamination1_price 1
    background_lamination1_pack_amount 1
    background_lamination1_pack_unit 'l'
    background_lamination2_price 4
    background_lamination2_pack_amount 1
    background_lamination2_pack_unit 'l'
  end
end
