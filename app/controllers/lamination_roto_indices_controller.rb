class LaminationRotoIndicesController < ApplicationController
  before_action :set_and_authorize_lamination_roto_index,
                only: [:edit, :update, :destroy]

  # GET /lamination_roto_indices
  def index
    authorize LaminationRotoIndex
    @lamination_roto_indices = policy_scope(LaminationRotoIndex)
    @lamination_roto_indices = @lamination_roto_indices.order(id: :asc)
    @lamination_roto_indices = @lamination_roto_indices.page(params[:page]).per(25)
  end

  # GET /lamination_roto_indices/new
  def new
    authorize LaminationRotoIndex
    @lamination_roto_index = LaminationRotoIndex.new
  end

  # POST /lamination_roto_indices
  def create
    authorize LaminationRotoIndex
    @lamination_roto_index = LaminationRotoIndex.new(lamination_roto_index_params)
    if @lamination_roto_index.save
      redirect_to lamination_roto_indices_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lamination_roto_indices/:id/edit
  def edit
  end

  # PATCH/PUT /lamination_roto_indices/:id
  def update
    if @lamination_roto_index.update(lamination_roto_index_params)
      redirect_to edit_lamination_roto_index_url(@lamination_roto_index),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lamination_roto_indices/:id
  def destroy
    if @lamination_roto_index.destroy
      redirect_to lamination_roto_indices_url, notice: t('shared.destroyed')
    else
      redirect_to lamination_roto_indices_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lamination_roto_index_params
    params.require(:lamination_roto_index).permit(
      :lamination_roto_linearity_id, :lamination_roto_glue_type_id,
      :value_wet, :value_dry
    )
  end

  def set_and_authorize_lamination_roto_index
    @lamination_roto_index = LaminationRotoIndex.find(params[:id])
    authorize @lamination_roto_index
  end
end
