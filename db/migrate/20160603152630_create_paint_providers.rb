class CreatePaintProviders < ActiveRecord::Migration
  def change
    create_table :paint_providers do |t|
      t.string :name
      t.string :yellow
      t.string :magenta
      t.string :cyan
      t.string :black
      t.monetize :price_yellow
      t.monetize :price_magenta
      t.monetize :price_cyan
      t.monetize :price_black

      t.timestamps null: false
    end
  end
end
