require 'features_helper'

RSpec.feature 'Create new lacquering_anilox' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_lacquering_anilox_button) { t('lacquering_aniloxes.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }
    given!(:machine) { create(:machine) }

    background do
      login admin
      visit lacquering_aniloxes_path(locale: I18n.locale)
      click_on add_lacquering_anilox_button
    end

    context 'With valid data' do
      scenario 'Admin creates new lacquering_anilox' do
        fill_in 'lacquering_anilox_value', with: 'test1234'
        select machine.name, from: 'lacquering_anilox_machine_id'
        click_on create_button

        expect(current_path).to eq(lacquering_aniloxes_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#lacquering_anilox_#{LacqueringAnilox.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new lacquering_anilox' do
        click_on create_button
        expect(current_path).to eq(lacquering_aniloxes_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
