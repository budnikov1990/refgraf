class LaminationType < ActiveRecord::Base
  has_many :lamination_glue_types, dependent: :restrict_with_error
  has_many :orders_duplex,
           class_name: 'Order',
           foreign_key: 'lamination_type_1_id',
           dependent: :restrict_with_error

  has_many :orders_triplex,
           class_name: 'Order',
           foreign_key: 'lamination_type_2_id',
           dependent: :restrict_with_error

  validates :name, presence: true
end
