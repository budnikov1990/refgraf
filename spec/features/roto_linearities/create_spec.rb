require 'features_helper'

RSpec.feature 'Create new roto_linearity' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_roto_linearity_button) { t('roto_linearities.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit roto_linearities_path(locale: I18n.locale)
      click_on add_roto_linearity_button
    end

    context 'With valid data' do
      scenario 'Admin creates new roto_linearity' do
        fill_in 'roto_linearity_name', with: 'test 123'
        click_on create_button

        expect(current_path).to eq(roto_linearities_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#roto_linearity_#{RotoLinearity.last.id}" do
          expect(page).to have_content 'test 123'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new roto_linearity' do
        click_on create_button
        expect(current_path).to eq(roto_linearities_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
