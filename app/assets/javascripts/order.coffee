$ ->
  showHideLamination1Roto = ->
    if $('.lamination-glue-system_1-select').size() > 0
      glue_system = $('.lamination-glue-system_1-select option:selected').text()
      machine = $('.lamination-machine_1-select option:selected').text()
      use_lamination_roto = $('.lamination-machine_1-select option:selected').data('use-lamination-roto')

      if glue_system == 'rozpuszczalnikowy' && use_lamination_roto
        $('.no-lamination1-roto').hide()
        $('.lamination-cylinder_1-select').closest('.form-group').hide()
        $('.lamination1-roto').show()
      else
        $('.no-lamination1-roto').show()
        $('.lamination-cylinder_1-select').closest('.form-group').show()
        $('.lamination1-roto').hide()

  showHideLamination2Roto = ->
    if $('.lamination-glue-system_2-select').size() > 0
      glue_system = $('.lamination-glue-system_2-select option:selected').text()
      machine = $('.lamination-machine_2-select option:selected').text()
      use_lamination_roto = $('.lamination-machine_2-select option:selected').data('use-lamination-roto')

      if glue_system == 'rozpuszczalnikowy' && use_lamination_roto
        $('.no-lamination2-roto').hide()
        $('.lamination-cylinder_2-select').closest('.form-group').hide()
        $('.lamination2-roto').show()
      else
        $('.no-lamination2-roto').show()
        $('.lamination-cylinder_1-select').closest('.form-group').show()
        $('.lamination2-roto').hide()

  showHideLacquering = (elem) ->
    checked = elem.is(':checked')
    $('.lacquering').toggle(checked)

  showHideRoto = (elem) ->
    checked = elem.is(':checked')
    $('.roto').toggle(checked)

  changeResinHardener = (elem, lamiantion) ->
    if elem.val() > 0
      $labelResin = $('.lamination-roto-resin_' + lamiantion).closest('.form-group').find('label')
      $labelResin.text(elem.find('option:selected').data('resin'))

      $labelHardener = $('.lamination-roto-hardener_' + lamiantion).closest('.form-group').find('label')
      $labelHardener.text(elem.find('option:selected').data('hardener'))

  showHideRotoBox = (elem) ->
    use_roto = elem.find('option:selected').data('use-roto')
    $('.order-roto').toggle(use_roto)

  $(document).on 'change', '.lacquering-toggle', ->
    showHideLacquering($(this))

  $(document).on 'change', '.roto-toggle', ->
    showHideRoto($(this))

  $(document).on 'click', '.cant_prepare_materials_comment_save', ->
    order_id = $(this).data('order-id')
    $.ajax(
      url : '/orders/' + order_id + '/cant_prepare_materials_update',
      type : 'PATCH',
      data : $('#cant_prepare_materials_form').serialize(),
      success : (response, textStatus, jqXhr) ->
        # console.log(response)
    )

  $(document).on 'click', '.finish_print_save', ->
    order_id = $(this).data('order-id')
    error = false
    $('#finish_print_form').find('.required').each (i, elem) ->
      $(elem).closest('.form-group').removeClass('has-error')
      if $(elem).val() == ''
        $(elem).closest('.form-group').addClass('has-error')
        error = true
    if !error
      $.ajax(
        url : '/orders/' + order_id + '/finish_print_update',
        type : 'PATCH',
        data : $('#finish_print_form').serialize(),
        success : (response, textStatus, jqXhr) ->
          # console.log(response)
      )

  $(document).on 'change', '.lamination-roto-glue-name_1-select', ->
    changeResinHardener($(this), 1)

  $(document).on 'change', '.lamination-roto-glue-name_2-select', ->
    changeResinHardener($(this), 2)

  $(document).on 'change', '.lamination-glue-system_1-select', ->
    showHideLamination1Roto()
  $(document).on 'change', '.lamination-glue-system_2-select', ->
    showHideLamination2Roto()

  $(document).on 'change', '.lamination-machine_1-select', ->
    showHideLamination1Roto()
  $(document).on 'change', '.lamination-machine_2-select', ->
    showHideLamination2Roto()

  $(document).on 'change', '.machines-select', ->
    showHideRotoBox($(this))

  showHideLacquering($('.lacquering-toggle'))
  showHideRoto($('.roto-toggle'))
  showHideRotoBox($('.machines-select'))
  showHideLamination1Roto()
  showHideLamination2Roto()
  changeResinHardener($('.lamination-roto-glue-name_1-select'), 1)
  changeResinHardener($('.lamination-roto-glue-name_2-select'), 2)

  rowNumbers = (elem) ->
    $(elem).each (idx) ->
      $(this).prepend('<td></td>')
      $(this).children().first().html(idx + 1)
  rowNumbers('#tab_main table tr')
  rowNumbers('#tab_for_print table tr')
  rowNumbers('#tab_lamination1 table tr')
  rowNumbers('#tab_lamination2 table tr')
  rowNumbers('#tab_after_print table tr')