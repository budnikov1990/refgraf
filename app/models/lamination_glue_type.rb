class LaminationGlueType < ActiveRecord::Base
  belongs_to :lamination_type
  has_many :orders_duplex,
           class_name: 'Order',
           foreign_key: 'lamination_glue_type_1_id',
           dependent: :restrict_with_error
  has_many :orders_triplex,
           class_name: 'Order',
           foreign_key: 'lamination_glue_type_2_id',
           dependent: :restrict_with_error

  validates :name, presence: true
  validates :lamination_type_id, presence: true
end
