class LaminationIndex < ActiveRecord::Base
  belongs_to :lamination_glue_system_1, class_name: 'LaminationGlueSystem'
  belongs_to :lamination_glue_system_2, class_name: 'LaminationGlueSystem'
  belongs_to :lamination_machine

  validates :value, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :lamination_glue_system_1_id, presence: true
  validates :lamination_machine_id, presence: true
end
