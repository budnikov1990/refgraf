class AddCuttingMachineToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :cutting_machine_id, :integer

    add_index :orders, :cutting_machine_id
  end
end
