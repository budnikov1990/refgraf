require 'features_helper'

RSpec.feature 'Destroy roto_paint' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:roto_paint) { create(:roto_paint) }

    background do
      login admin
      visit roto_paints_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the roto_paint', js: true do
      within "#roto_paint_#{roto_paint.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(roto_paints_path(locale: I18n.locale))
      expect(page).to have_no_selector("#roto_paint_#{roto_paint.id}")
    end
  end
end
