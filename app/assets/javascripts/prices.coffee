$ ->
  showHideTemplatePrices = (elem) ->
    $elem = elem.find('option:selected')
    product_structure = $elem.data('product-structure-name')
    if product_structure == 'simplex'
      $('.background_print h3').html($elem.data('print'))
      $('.background_print').show()
    else if product_structure == 'duplex'
      $('.background_print h3').html($elem.data('print'))
      $('.background_print').show()

      $('.background_lamination1 h3').html($elem.data('lamination1'))
      $('.background_lamination1').show()
    else if product_structure == 'triplex'
      $('.background_print h3').html($elem.data('print'))
      $('.background_print').show()

      $('.background_lamination1 h3').html($elem.data('lamination1'))
      $('.background_lamination1').show()

      $('.background_lamination2 h3').html($elem.data('lamination2'))
      $('.background_lamination2').show()

  #$(document).on 'change', '.template-background-price', ->
    #showHideTemplatePrices($(this))

  #if $('.template-background-price').size() > 0
    #showHideTemplatePrices($('.template-background-price'))
