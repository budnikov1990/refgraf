class LaminationRotoIndex < ActiveRecord::Base
  belongs_to :lamination_roto_linearity
  belongs_to :lamination_roto_glue_type

  validates :value_dry, presence: true, numericality: { greater_than: 0 }
  validates :value_wet, presence: true, numericality: { greater_than: 0 }
  validates :lamination_roto_linearity_id, presence: true
  validates :lamination_roto_glue_type_id, presence: true
end
