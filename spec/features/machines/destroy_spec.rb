require 'features_helper'

RSpec.feature 'Destroy machine' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:machine) { create(:machine) }

    background do
      login admin
      visit machines_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the machine', js: true do
      within "#machine_#{machine.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(machines_path(locale: I18n.locale))
      expect(page).to have_no_selector("#machine_#{machine.id}")
    end
  end
end
