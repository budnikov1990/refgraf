require 'features_helper'

RSpec.feature 'View list of lamination_roto_presers' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_roto_preser) { create(:lamination_roto_preser) }

    background do
      login admin
      visit lamination_roto_presers_path
    end

    scenario 'User sees list of lamination_roto_presers' do
      within "#lamination_roto_preser_#{lamination_roto_preser.id}" do
        expect(page).to have_content lamination_roto_preser.value
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
