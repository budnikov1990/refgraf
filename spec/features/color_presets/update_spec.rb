require 'features_helper'

RSpec.feature 'Update color_preset' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:color_preset) { create(:color_preset) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_color_preset_path(color_preset, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'color_preset_name', with: 'test123456'
        click_on save_button

        expect(current_path).to eq(edit_color_preset_path(color_preset, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        fill_in 'color_preset_name', with: ''
        click_on save_button
        expect(current_path).to eq(color_preset_path(color_preset, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
