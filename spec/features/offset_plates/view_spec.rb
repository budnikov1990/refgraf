require 'features_helper'

RSpec.feature 'View list of offset_plates' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:offset_plate) { create(:offset_plate) }

    background do
      login admin
      visit offset_plates_path
    end

    scenario 'User sees list of offset_plates' do
      within "#offset_plate_#{offset_plate.id}" do
        expect(page).to have_content offset_plate.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
