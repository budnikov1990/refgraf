require 'features_helper'

RSpec.feature 'Update machine' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:machine) { create(:machine) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_machine_path(machine, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'machine_name', with: 'test123456'
        fill_in 'machine_calculation_coefficient', with: 400
        click_on save_button

        expect(current_path).to eq(edit_machine_path(machine, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        fill_in 'machine_name', with: ''
        click_on save_button
        expect(current_path).to eq(machine_path(machine, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
