class OffsetPlatesController < ApplicationController
  before_action :set_and_authorize_offset_plate, only: [:edit, :update, :destroy]

  # GET /offset_plates
  def index
    authorize OffsetPlate
    @offset_plates = policy_scope(OffsetPlate)
    @offset_plates = @offset_plates.order(id: :asc)
    @offset_plates = @offset_plates.page(params[:page]).per(25)
  end

  # GET /offset_plates/new
  def new
    authorize OffsetPlate
    @offset_plate = OffsetPlate.new
  end

  # POST /offset_plates
  def create
    authorize OffsetPlate
    @offset_plate = OffsetPlate.new(offset_plate_params)
    if @offset_plate.save
      redirect_to offset_plates_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /offset_plates/:id/edit
  def edit
  end

  # PATCH/PUT /offset_plates/:id
  def update
    if @offset_plate.update(offset_plate_params)
      redirect_to edit_offset_plate_url(@offset_plate), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /offset_plates/:id
  def destroy
    if @offset_plate.destroy
      redirect_to offset_plates_url, notice: t('shared.destroyed')
    else
      redirect_to offset_plates_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def offset_plate_params
    params.require(:offset_plate).permit(:name, :machine_id, :cylinder_id)
  end

  def set_and_authorize_offset_plate
    @offset_plate = OffsetPlate.find(params[:id])
    authorize @offset_plate
  end
end
