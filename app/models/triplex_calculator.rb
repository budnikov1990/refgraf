class TriplexCalculator < DuplexCalculator
  alias triplex_amount_mb good_material

  def initialize(params)
    super(params)
    @canvas_width_triplex ||= @order.canvas_width_lamination_2.to_f / 1000
  end

  def netto_part
    @netto_part ||=
      @working_width * @order.template.background.raw_weight +
      @working_width * @order.template.background_for_lamination_1.raw_weight +
      @working_width * @order.template.background_for_lamination_2.raw_weight +
      @working_width * @order.ink_amount +
      @working_width * @order.panton_1_amount +
      @working_width * @order.panton_2_amount +
      @working_width * lamination_index +
      @working_width * lacquering_index +
      @working_width * roto_index_dry +
      @working_width * lamination2_roto_index_dry +
      @working_width * lamination1_roto_index_dry
  end

  def lamination_mb
    duplex_amount_mb + triplex_amount_mb
  end

  def before_cutting_mb
    material_after_second_lamination
  end

  def material_after_second_lamination
    @material_after_second_lamination ||=
      (
        material_after_first_lamination -
        (netto * @machine_params.after_lamination_2_percent)
      ).round(1)
  end

  def triplex_weight
    @triplex_weight ||= (
    (
    triplex_amount_mb *
        @canvas_width_triplex *
        @order.template.background_for_lamination_2.raw_weight
    ) / 1000
    ).round(1)
  end

  def triplex_area
    @triplex_area ||= (
    triplex_amount_mb * @canvas_width_triplex
    ).round(1)
  end

  def material_after_cut
    @material_after_cut ||= (
    material_after_second_lamination -
        (netto * @machine_params.after_cut_percent)
    ).round(1)
  end

  def glue_dry_weight_roto
    super
    if @order.lamination_roto_preser_2.present?
      @glue_dry_weight_roto +=
        (
          amount_mb *
          (lamination2_roto_index_dry / 1000) *
          (@order.lamination_roto_preser_2.value.to_f / 1000)
        ).round(1)
    end
    @glue_dry_weight_roto
  end

  def glue_wet_weight_roto
    super
    if @order.lamination_roto_preser_2.present?
      @glue_dry_weight_roto +=
        (
          amount_mb *
          (lamination2_roto_index_wet / 1000) *
          (@order.lamination_roto_preser_2.value.to_f / 1000)
        ).round(1)
    end
    @glue_dry_weight_roto
  end

  def lamination_index_model
    @lamination_index_model ||= LaminationIndex.find_by(
      lamination_glue_system_1: @order.lamination_glue_system_1,
      lamination_glue_system_2: @order.lamination_glue_system_2,
      lamination_machine: @order.lamination_machine_2
    )
  end

  def lamination_index
    index = lamination_index_model
    index.present? ? index.value.to_f : 0
  end

  def lamination2_roto_index_dry
    lamination2_roto_index.present? ? lamination2_roto_index.value_dry.to_f : 0
  end

  def lamination2_roto_index_wet
    lamination2_roto_index.present? ? lamination2_roto_index.value_wet.to_f : 0
  end

  def princess_solventless_glue2
    @princess_solventless_glue2 ||=
      if @order.lamination_cylinder_2.present?
        (
          amount_mb *
          (@order.lamination_cylinder_2.value.to_f / 1000) *
          (lamination_index / 1000)
        ).round(1)
      else
        0
      end
  end

  private

  def lamination2_roto_index
    @lamination2_roto_index ||= LaminationRotoIndex.find_by(
      lamination_roto_linearity: @order.lamination_roto_linearity_2,
      lamination_roto_glue_type: @order.lamination_roto_glue_type_2
    )
  end
end
