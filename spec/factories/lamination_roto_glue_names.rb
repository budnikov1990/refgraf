FactoryGirl.define do
  factory :lamination_roto_glue_name do
    name 'Liofol LA 3644 / LA 6055'
    resin_name 'Liofol LA 3966-21'
    hardener_name 'LA6054-21'
  end
end
