class AddOrderFieldsForPrice < ActiveRecord::Migration
  def change
    add_column :orders, :need_price_calculation, :boolean, default: false

    add_column :orders, :yellow_percent, :integer
    add_column :orders, :magenta_percent, :integer
    add_column :orders, :cyan_percent, :integer
    add_column :orders, :black_percent, :integer

    add_column :orders, :yellow_provider_id, :integer
    add_column :orders, :magenta_provider_id, :integer
    add_column :orders, :cyan_provider_id, :integer
    add_column :orders, :black_provider_id, :integer

    add_column :orders, :background_for_print_provider_id, :integer
  end
end
