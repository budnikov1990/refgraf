class AddMachinesSettingsFields < ActiveRecord::Migration
  def change
    add_column :machines, :use_roto, :boolean, default: false
    add_column :machines, :use_lacquering_polymer, :boolean, default: false

    add_column :lamination_machines, :use_lamination_roto, :boolean, default: false
  end
end
