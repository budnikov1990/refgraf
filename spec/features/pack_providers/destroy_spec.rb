require 'features_helper'

RSpec.feature 'Destroy pack_provider' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:pack_provider) { create(:pack_provider) }

    background do
      login admin
      visit pack_providers_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the pack_provider', js: true do
      within "#pack_provider_#{pack_provider.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(pack_providers_path(locale: I18n.locale))
      expect(page).to have_no_selector("#pack_provider_#{pack_provider.id}")
    end
  end
end
