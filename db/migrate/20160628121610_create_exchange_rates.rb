class CreateExchangeRates < ActiveRecord::Migration
  def change
    create_table :exchange_rates do |t|
      t.string :from
      t.string :to
      t.decimal :rate, precision: 15, scale: 6

      t.timestamps null: false
    end
    add_index :exchange_rates, :from
    add_index :exchange_rates, :to
    add_index :exchange_rates, [:from, :to]
  end
end
