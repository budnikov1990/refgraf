class LaminationMachine < ActiveRecord::Base
  has_many :lamination_cylinders, dependent: :destroy, inverse_of: :lamination_machine
  has_many :lamination_indices, dependent: :restrict_with_error
  has_many :orders_duplex,
           class_name: 'Order',
           foreign_key: 'lamination_machine_1_id',
           dependent: :restrict_with_error

  has_many :orders_triplex,
           class_name: 'Order',
           foreign_key: 'lamination_machine_2_id',
           dependent: :restrict_with_error

  accepts_nested_attributes_for :lamination_cylinders,
                                reject_if: :all_blank,
                                allow_destroy: true

  monetize :hour_cost_cents

  validates :name, presence: true, uniqueness: true
end
