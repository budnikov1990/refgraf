class Activity < ActiveRecord::Base
  belongs_to :trackable, polymorphic: true, required: true
  belongs_to :owner, polymorphic: true

  validates :key, presence: true

  def localized_action(options = {})
    I18n.t("activities.#{key}.action", options)
  end
end
