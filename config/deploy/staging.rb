server 'ars.rrv.ru', user: 'w3dev-refgraf', roles: %w(app db web), port: 2221
set :deploy_to, '/www/refgraf.ars.vision'

# Puma setup
set :puma_bind, 'tcp://0.0.0.0:8105'
