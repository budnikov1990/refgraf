require 'features_helper'

RSpec.feature 'View list of offset_plate_providers' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:offset_plate_provider) { create(:offset_plate_provider) }

    background do
      login admin
      visit offset_plate_providers_path
    end

    scenario 'User sees list of offset_plate_providers' do
      within "#offset_plate_provider_#{offset_plate_provider.id}" do
        expect(page).to have_content offset_plate_provider.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
