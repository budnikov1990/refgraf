require 'features_helper'

RSpec.feature 'Destroy roto_preser' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:roto_preser) { create(:roto_preser) }

    background do
      login admin
      visit roto_presers_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the roto_preser', js: true do
      within "#roto_preser_#{roto_preser.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(roto_presers_path(locale: I18n.locale))
      expect(page).to have_no_selector("#roto_preser_#{roto_preser.id}")
    end
  end
end
