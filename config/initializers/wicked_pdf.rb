WickedPdf.config = {
    exe_path: Gem.bin_path('wkhtmltopdf-binary', 'wkhtmltopdf'),
    layout: 'pdf.html',
    disposition: 'attachment'
}
