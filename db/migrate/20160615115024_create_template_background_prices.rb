class CreateTemplateBackgroundPrices < ActiveRecord::Migration
  def change
    create_table :template_background_prices do |t|
      t.references :template, index: true, foreign_key: true
      t.references :template_background_provider, index: {name: 'back_providers_on_back_prices'}, foreign_key: true
      t.monetize :background_print_price
      t.integer :background_print_pack_amount
      t.string :background_print_pack_unit
      t.monetize :background_lamination1_price
      t.integer :background_lamination1_pack_amount
      t.string :background_lamination1_pack_unit
      t.monetize :background_lamination2_price
      t.integer :background_lamination2_pack_amount
      t.string :background_lamination2_pack_unit

      t.timestamps null: false
    end
  end
end
