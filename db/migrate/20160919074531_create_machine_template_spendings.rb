class CreateMachineTemplateSpendings < ActiveRecord::Migration
  def change
    create_table :machine_template_spendings do |t|
      t.integer :thousand_more
      t.integer :thousand_less
      t.references :machine, index: true, foreign_key: true
      t.references :template, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
