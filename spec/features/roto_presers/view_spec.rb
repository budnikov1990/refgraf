require 'features_helper'

RSpec.feature 'View list of roto_presers' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:roto_preser) { create(:roto_preser) }

    background do
      login admin
      visit roto_presers_path
    end

    scenario 'User sees list of roto_presers' do
      within "#roto_preser_#{roto_preser.id}" do
        expect(page).to have_content roto_preser.value
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
