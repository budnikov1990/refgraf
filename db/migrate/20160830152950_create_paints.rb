class CreatePaints < ActiveRecord::Migration
  def change
    create_table :paints do |t|
      t.string :paint_type
      t.string :name
      t.monetize :price
      t.integer :pack_amount
      t.string :pack_unit
      t.references :paint_provider, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
