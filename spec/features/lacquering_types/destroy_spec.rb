require 'features_helper'

RSpec.feature 'Destroy lacquering_type' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lacquering_type) { create(:lacquering_type) }

    background do
      login admin
      visit lacquering_types_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lacquering_type', js: true do
      within "#lacquering_type_#{lacquering_type.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lacquering_types_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lacquering_type_#{lacquering_type.id}")
    end
  end
end
