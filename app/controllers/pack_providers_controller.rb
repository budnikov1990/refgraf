class PackProvidersController < ApplicationController
  before_action :set_and_authorize_pack_provider, only: [:edit, :update, :destroy]

  # GET /pack_providers
  def index
    authorize PackProvider
    @pack_providers = policy_scope(PackProvider)
    @pack_providers = @pack_providers.order(id: :asc)
    @pack_providers = @pack_providers.page(params[:page]).per(25)
  end

  # GET /pack_providers/new
  def new
    authorize PackProvider
    @pack_provider = PackProvider.new
  end

  # POST /pack_providers
  def create
    authorize PackProvider
    @pack_provider = PackProvider.new(pack_provider_params)
    if @pack_provider.save
      redirect_to pack_providers_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /pack_providers/:id/edit
  def edit
  end

  # PATCH/PUT /pack_providers/:id
  def update
    if @pack_provider.update(pack_provider_params)
      redirect_to edit_pack_provider_url(@pack_provider), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /pack_providers/:id
  def destroy
    if @pack_provider.destroy
      redirect_to pack_providers_url, notice: t('shared.destroyed')
    else
      redirect_to pack_providers_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def pack_provider_params
    params.require(:pack_provider).permit(:name)
  end

  def set_and_authorize_pack_provider
    @pack_provider = PackProvider.find(params[:id])
    authorize @pack_provider
  end
end
