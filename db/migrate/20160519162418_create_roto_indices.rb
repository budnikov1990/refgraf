class CreateRotoIndices < ActiveRecord::Migration
  def change
    create_table :roto_indices do |t|
      t.float :value_dry
      t.float :value_wet
      t.references :roto_linearity, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
