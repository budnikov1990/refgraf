require 'rails_helper'

RSpec.describe Cylinder, type: :model do
  it { is_expected.to validate_presence_of(:machine) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:perimeter) }
  it { is_expected.to validate_numericality_of(:perimeter).is_greater_than(0) }
end
