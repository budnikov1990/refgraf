require 'features_helper'

RSpec.feature 'Create new lacquering_polymer' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_lacquering_polymer_button) { t('lacquering_polymers.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }
    given!(:machine) { create(:machine) }

    background do
      login admin
      visit lacquering_polymers_path(locale: I18n.locale)
      click_on add_lacquering_polymer_button
    end

    context 'With valid data' do
      scenario 'Admin creates new lacquering_polymer' do
        fill_in 'lacquering_polymer_value', with: 'test1234'
        select machine.name, from: 'lacquering_polymer_machine_id'
        click_on create_button

        expect(current_path).to eq(lacquering_polymers_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#lacquering_polymer_#{LacqueringPolymer.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new lacquering_polymer' do
        click_on create_button
        expect(current_path).to eq(lacquering_polymers_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
