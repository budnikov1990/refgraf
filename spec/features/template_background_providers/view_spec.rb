require 'features_helper'

RSpec.feature 'View list of template_background_providers' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:template_background_provider) { create(:template_background_provider) }

    background do
      login admin
      visit template_background_providers_path
    end

    scenario 'User sees list of template_background_providers' do
      within "#template_background_provider_#{template_background_provider.id}" do
        expect(page).to have_content template_background_provider.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
