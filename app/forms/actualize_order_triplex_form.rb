class ActualizeOrderTriplexForm < ActualizeOrderDuplexForm
  attribute :lamination_machine_2_id, Integer
  attribute :lamination_cylinder_2_id, Integer
  attribute :lamination_type_2_id, Integer
  attribute :lamination_glue_2_id, Integer
  attribute :lamination_glue_system_2_id, Integer
  attribute :lamination_glue_type_2_id, Integer
  attribute :canvas_width_lamination_2, Integer

  attribute :lamination_roto_glue_name_2_id, Integer
  attribute :lamination_roto_glue_type_2_id, Integer
  attribute :lamination_roto_linearity_2_id, Integer
  attribute :lamination_roto_preser_2_id, Integer
  attribute :lamination_roto_glue_viscosity_2, Integer
  attribute :lamination_roto_acetate_2, Float
  attribute :lamination_roto_resin_2, Float
  attribute :lamination_roto_hardener_2, Float

  validate :check_canvas_width_lamination_2
  validates :canvas_width_lamination_2, presence: true

  validates :lamination_machine_2, :lamination_machine_2_id, presence: true
  validates :lamination_cylinder_2, :lamination_cylinder_2_id,
            presence: true, unless: :check_lamination2_roto?
  validates :lamination_type_2, :lamination_type_2_id, presence: true
  validates :lamination_glue_2, :lamination_glue_2_id,
            presence: true,
            unless: :check_lamination2_roto?
  validates :lamination_glue_system_2, :lamination_glue_system_2_id, presence: true
  validates :lamination_glue_type_2, :lamination_glue_type_2_id, presence: true

  validates :lamination_roto_glue_name_2, :lamination_roto_glue_name_2_id,
            presence: true,
            if: :check_lamination2_roto?
  validates :lamination_roto_glue_type_2, :lamination_roto_glue_type_2_id,
            presence: true,
            if: :check_lamination2_roto?
  validates :lamination_roto_linearity_2, :lamination_roto_linearity_2_id,
            presence: true,
            if: :check_lamination2_roto?
  validates :lamination_roto_preser_2, :lamination_roto_preser_2_id,
            presence: true,
            if: :check_lamination2_roto?

  validates :lamination_roto_glue_viscosity_2,
            presence: true, if: :check_lamination2_roto?
  validates :lamination_roto_acetate_2, presence: true, if: :check_lamination2_roto?
  validates :lamination_roto_resin_2, presence: true, if: :check_lamination2_roto?
  validates :lamination_roto_hardener_2, presence: true, if: :check_lamination2_roto?

  private

  def lamination_params
    super
    params = {
      lamination_machine_2: lamination_machine_2,
      lamination_cylinder_2: lamination_cylinder_2,
      lamination_glue_2: lamination_glue_2, lamination_type_2: lamination_type_2,
      lamination_glue_system_2: lamination_glue_system_2,
      lamination_glue_type_2: lamination_glue_type_2,
      canvas_width_lamination_2: canvas_width_lamination_2,
      lamination_roto_glue_name_2: lamination_roto_glue_name_2,
      lamination_roto_glue_type_2: lamination_roto_glue_type_2,
      lamination_roto_linearity_2: lamination_roto_linearity_2,
      lamination_roto_preser_2: lamination_roto_preser_2,
      lamination_roto_glue_viscosity_2: lamination_roto_glue_viscosity_2,
      lamination_roto_acetate_2: lamination_roto_acetate_2,
      lamination_roto_resin_2: lamination_roto_resin_2,
      lamination_roto_hardener_2: lamination_roto_hardener_2
    }
    @lamination_params = @lamination_params.merge(params)
  end

  def lamination_machine_2
    @lamination_machine_2 ||= LaminationMachine.find_by(id: lamination_machine_2_id)
  end

  def lamination_cylinder_2
    @lamination_cylinder_2 ||= LaminationCylinder.find_by(id: lamination_cylinder_2_id)
  end

  def lamination_glue_2
    @lamination_glue_2 ||= LaminationGlue.find_by(id: lamination_glue_2_id)
  end

  def lamination_glue_system_2
    @lamination_glue_system_2 ||=
      LaminationGlueSystem.find_by(id: lamination_glue_system_2_id)
  end

  def lamination_type_2
    @lamination_type_2 ||= LaminationType.find_by(id: lamination_type_2_id)
  end

  def lamination_glue_type_2
    @lamination_glue_type_2 ||= LaminationGlueType.find_by(id: lamination_glue_type_2_id)
  end

  def lamination_roto_glue_name_2
    @lamination_roto_glue_name_2 ||=
      LaminationRotoGlueName.find_by(id: lamination_roto_glue_name_2_id)
  end

  def lamination_roto_glue_type_2
    @lamination_roto_glue_type_2 ||=
      LaminationRotoGlueType.find_by(id: lamination_roto_glue_type_2_id)
  end

  def lamination_roto_linearity_2
    @lamination_roto_linearity_2 ||=
      LaminationRotoLinearity.find_by(id: lamination_roto_linearity_2_id)
  end

  def lamination_roto_preser_2
    @lamination_roto_preser_2 ||=
      LaminationRotoPreser.find_by(id: lamination_roto_preser_2_id)
  end

  def check_canvas_width_lamination_2
    if canvas_width_lamination_2.present? && item_count_on_width.present?
      counted_canvas_width = order.width * item_count_on_width
      if canvas_width_lamination_2 <= counted_canvas_width
        errors.add(:canvas_width_lamination_2,
                   I18n.t('shared.errors.not_suitable_canvas_width_lamination',
                          p: counted_canvas_width.to_i
                         )
                  )
      end
    end
  end

  def check_lamination2_roto?
    (!lamination_machine_2.blank? &&
      !lamination_glue_system_2.blank?) &&
      (lamination_machine_2.use_lamination_roto &&
      lamination_glue_system_2.name == 'rozpuszczalnikowy')
  end
end
