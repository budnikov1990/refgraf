class OrderStartLaminationSecondUpdate
  include Interactor
  include TransactionalInteractor

  def call
    context.order.start_lamination_second!
    log
  end

  private

  delegate :params, :user, :order, to: :context

  def log
    Activity.create!(
      key: 'order.start_lamination_second',
      trackable: order,
      owner: user,
      data: log_params
    )
  end

  def log_params
    {
      id: context.order.id,
      title: context.order.name,
      number: context.order.number,
      new_state: 'laminating_second'
    }
  end
end
