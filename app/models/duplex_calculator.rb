class DuplexCalculator < BaseCalculator
  def initialize(params)
    super(params)
    @canvas_width_duplex ||= @order.canvas_width_lamination_1.to_f / 1000
  end

  def netto_part
    @netto_part ||=
      @working_width * @order.template.background.raw_weight +
      @working_width * @order.template.background_for_lamination_1.raw_weight +
      @working_width * @order.ink_amount +
      @working_width * @order.panton_1_amount +
      @working_width * @order.panton_2_amount +
      @working_width * lamination_index +
      @working_width * lacquering_index +
      @working_width * roto_index_dry +
      @working_width * lamination1_roto_index_dry
  end

  def lamination_mb
    duplex_amount_mb
  end

  def before_cutting_mb
    material_after_first_lamination
  end

  def good_material
    @good_material ||= (
      amount_mb -
      @order.machine.calculation_coefficient -
      (
        (
          amount_mb -
            @order.machine.calculation_coefficient
        ) * @machine_params.good_percent)
    ).round(1)
  end
  alias duplex_amount_mb good_material

  def material_after_first_lamination
    @material_after_first_lamination ||=
      (
        good_material - (netto * @machine_params.after_lamination_1_percent)
      ).round(1)
  end

  def duplex_weight
    @duplex_weight ||= (
      (
        duplex_amount_mb *
          @canvas_width_duplex *
          @order.template.background_for_lamination_1.raw_weight
      ) / 1000
    ).round(1)
  end

  def duplex_area
    @duplex_area ||= (
    duplex_amount_mb * @canvas_width_duplex
    ).round(1)
  end

  def material_after_cut
    @material_after_cut ||= (
    material_after_first_lamination -
      (netto * @machine_params.after_cut_percent)
    ).round(1)
  end

  def glue_dry_weight_roto
    @glue_dry_weight_roto = 0
    if @order.lamination_roto_preser_1.present?
      @glue_dry_weight_roto =
        (
          amount_mb *
          (lamination1_roto_index_dry / 1000) *
          (@order.lamination_roto_preser_1.value.to_f / 1000)
        ).round(1)
    end
  end

  def glue_wet_weight_roto
    @glue_wet_weight_roto = 0
    if @order.lamination_roto_preser_1.present?
      @glue_dry_weight_roto =
        (
          amount_mb *
          (lamination1_roto_index_wet / 1000) *
          (@order.lamination_roto_preser_1.value.to_f / 1000)
        ).round(1)
    end
  end

  def lamination_index_model
    @lamination_index_model ||= LaminationIndex.find_by(
      lamination_glue_system_1: @order.lamination_glue_system_1,
      lamination_machine: @order.lamination_machine_1
    )
  end

  def lamination_index
    index = lamination_index_model
    index.present? ? index.value.to_f : 0
  end

  def lamination1_roto_index_dry
    lamination1_roto_index.present? ? lamination1_roto_index.value_dry.to_f : 0
  end

  def lamination1_roto_index_wet
    lamination1_roto_index.present? ? lamination1_roto_index.value_wet.to_f : 0
  end

  def princess_solventless_glue1
    @princess_solventless_glue1 ||=
      if @order.lamination_cylinder_1.present?
        (
          amount_mb *
          (@order.lamination_cylinder_1.value.to_f / 1000) *
          (lamination_index / 1000)
        ).round(1)
      else
        0
      end
  end

  private

  def lamination1_roto_index
    @lamination1_roto_index ||= LaminationRotoIndex.find_by(
      lamination_roto_linearity: @order.lamination_roto_linearity_1,
      lamination_roto_glue_type: @order.lamination_roto_glue_type_1
    )
  end
end
