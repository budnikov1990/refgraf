require 'features_helper'

RSpec.feature 'Update acetate_provider' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:acetate_provider) { create(:acetate_provider) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_acetate_provider_path(acetate_provider, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'acetate_provider_name', with: 'test123456'
        click_on save_button

        expect(current_path).to eq(edit_acetate_provider_path(acetate_provider, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        fill_in 'acetate_provider_name', with: ''
        click_on save_button
        expect(current_path).to eq(acetate_provider_path(acetate_provider, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
