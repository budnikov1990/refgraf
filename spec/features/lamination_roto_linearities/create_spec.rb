require 'features_helper'

RSpec.feature 'Create new lamination_roto_linearity' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_lamination_roto_linearity_button) { t('lamination_roto_linearities.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit lamination_roto_linearities_path(locale: I18n.locale)
      click_on add_lamination_roto_linearity_button
    end

    context 'With valid data' do
      scenario 'Admin creates new lamination_roto_linearity' do
        fill_in 'lamination_roto_linearity_value', with: 456
        click_on create_button

        expect(current_path).to eq(lamination_roto_linearities_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#lamination_roto_linearity_#{LaminationRotoLinearity.last.id}" do
          expect(page).to have_content '456'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new lamination_roto_linearity' do
        click_on create_button
        expect(current_path).to eq(lamination_roto_linearities_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
