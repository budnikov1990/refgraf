require 'rails_helper'

RSpec.describe Paint, type: :model do
  it 'validations' do
    %w(name price_cents pack_unit).each do |field|
      is_expected.to validate_presence_of(field.to_sym)
    end
  end

  it 'relations' do
    is_expected.to belong_to(:paint_provider)
  end
end
