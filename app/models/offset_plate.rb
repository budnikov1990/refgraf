class OffsetPlate < ActiveRecord::Base
  belongs_to :machine, required: true
  belongs_to :cylinder, required: true
  has_many :offset_plate_prices, dependent: :restrict_with_error

  validates :name, presence: true
  validates :machine_id, presence: true
  validates :cylinder_id, presence: true
end
