require 'features_helper'

RSpec.feature 'Destroy color_preset' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:color_preset) { create(:color_preset) }

    background do
      login admin
      visit color_presets_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the color_preset', js: true do
      within "#color_preset_#{color_preset.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(color_presets_path(locale: I18n.locale))
      expect(page).to have_no_selector("#color_preset_#{color_preset.id}")
    end
  end
end
