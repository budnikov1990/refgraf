class OffsetPlatePrice < ActiveRecord::Base
  belongs_to :offset_plate
  belongs_to :offset_plate_provider

  monetize :price_cents

  validates :offset_plate_id, presence: true
  validates :price, presence: true
  validates :price_currency, presence: true
end
