class LaminationRotoPresersController < ApplicationController
  before_action :set_and_authorize_lamination_roto_preser,
                only: [:edit, :update, :destroy]

  # GET /lamination_roto_presers
  def index
    authorize LaminationRotoPreser
    @lamination_roto_presers = policy_scope(LaminationRotoPreser)
    @lamination_roto_presers = @lamination_roto_presers.order(id: :asc)
    @lamination_roto_presers = @lamination_roto_presers.page(params[:page]).per(25)
  end

  # GET /lamination_roto_presers/new
  def new
    authorize LaminationRotoPreser
    @lamination_roto_preser = LaminationRotoPreser.new
  end

  # POST /lamination_roto_presers
  def create
    authorize LaminationRotoPreser
    @lamination_roto_preser = LaminationRotoPreser.new(lamination_roto_preser_params)
    if @lamination_roto_preser.save
      redirect_to lamination_roto_presers_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lamination_roto_presers/:id/edit
  def edit
  end

  # PATCH/PUT /lamination_roto_presers/:id
  def update
    if @lamination_roto_preser.update(lamination_roto_preser_params)
      redirect_to edit_lamination_roto_preser_url(@lamination_roto_preser),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lamination_roto_presers/:id
  def destroy
    if @lamination_roto_preser.destroy
      redirect_to lamination_roto_presers_url, notice: t('shared.destroyed')
    else
      redirect_to lamination_roto_presers_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lamination_roto_preser_params
    params.require(:lamination_roto_preser).permit(:value)
  end

  def set_and_authorize_lamination_roto_preser
    @lamination_roto_preser = LaminationRotoPreser.find(params[:id])
    authorize @lamination_roto_preser
  end
end
