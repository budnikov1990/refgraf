FactoryGirl.define do
  factory :machine do
    name 'test machine'
    calculation_coefficient 500
    max_colors_count 2
    use_roto false
    use_lacquering_polymer false

    transient do
      cylinders_count 1
    end

    after :build do |machine, evaluator|
      evaluator.cylinders_count.times do
        machine.cylinders.build(
          attributes_for(:cylinder, machine: machine)
        )
      end
    end
  end
end
