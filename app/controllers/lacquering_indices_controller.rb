class LacqueringIndicesController < ApplicationController
  before_action :set_and_authorize_lacquering_index, only: [:edit, :update, :destroy]

  # GET /lacquering_indices
  def index
    authorize LacqueringIndex
    @lacquering_indices = policy_scope(LacqueringIndex)
    @lacquering_indices = @lacquering_indices.order(id: :asc)
    @lacquering_indices = @lacquering_indices.page(params[:page]).per(25)
  end

  # GET /lacquering_indices/new
  def new
    authorize LacqueringIndex
    @lacquering_index = LacqueringIndex.new
  end

  # POST /lacquering_indices
  def create
    authorize LacqueringIndex
    @lacquering_index = LacqueringIndex.new(lacquering_index_params)
    if @lacquering_index.save
      redirect_to lacquering_indices_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lacquering_indices/:id/edit
  def edit
  end

  # PATCH/PUT /lacquering_indices/:id
  def update
    if @lacquering_index.update(lacquering_index_params)
      redirect_to edit_lacquering_index_url(@lacquering_index),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lacquering_indices/:id
  def destroy
    if @lacquering_index.destroy
      redirect_to lacquering_indices_url, notice: t('shared.destroyed')
    else
      redirect_to lacquering_indices_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lacquering_index_params
    params.require(:lacquering_index).permit(
      :machine_id, :lacquering_type_id, :lacquering_anilox_id, :lacquering_polymer_id,
      :coverage_ratio, :dry_weight, :wet_weight
    )
  end

  def set_and_authorize_lacquering_index
    @lacquering_index = LacqueringIndex.find(params[:id])
    authorize @lacquering_index
  end
end
