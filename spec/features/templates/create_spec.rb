require 'features_helper'

RSpec.feature 'Create new template' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:product_structure) { create(:product_structure) }
    given(:add_template_button) { t('templates.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit templates_path(locale: I18n.locale)
      click_on add_template_button
    end

    context 'With valid data' do
      scenario 'Admin creates new template' do
        fill_in 'template_name', with: 'test1234'
        select product_structure.name, from: 'template_product_structure_id'
        click_on create_button

        expect(current_path).to eq(templates_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#template_#{Template.last.id}" do
          expect(page).to have_content 'test1234'
          expect(page).to have_content product_structure.name
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new template' do
        click_on create_button
        expect(current_path).to eq(templates_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
