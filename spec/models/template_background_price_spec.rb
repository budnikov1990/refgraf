require 'rails_helper'

RSpec.describe TemplateBackgroundPrice, type: :model do
  it { is_expected.to validate_presence_of(:template_id) }
  it { is_expected.to validate_presence_of(:template_background_provider_id) }
end
