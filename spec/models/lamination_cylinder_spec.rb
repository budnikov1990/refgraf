require 'rails_helper'

RSpec.describe LaminationCylinder, type: :model do
  it { is_expected.to validate_presence_of(:lamination_machine) }
  it { is_expected.to validate_presence_of(:value) }
end
