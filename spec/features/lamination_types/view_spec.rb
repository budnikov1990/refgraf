require 'features_helper'

RSpec.feature 'View list of lamination_types' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_type) { create(:lamination_type) }

    background do
      login admin
      visit lamination_types_path
    end

    scenario 'User sees list of lamination_types' do
      within "#lamination_type_#{lamination_type.id}" do
        expect(page).to have_content lamination_type.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
