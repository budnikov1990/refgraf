require 'features_helper'

RSpec.feature 'Create new lamination_glue_type' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_lamination_glue_type_button) { t('lamination_glue_types.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }
    given!(:lamination_type) { create(:lamination_type) }

    background do
      login admin
      visit lamination_glue_types_path(locale: I18n.locale)
      click_on add_lamination_glue_type_button
    end

    context 'With valid data' do
      scenario 'Admin creates new lamination_glue_type' do
        select lamination_type.name, from: 'lamination_glue_type_lamination_type_id'
        fill_in 'lamination_glue_type_name', with: 'test1234'
        click_on create_button

        expect(current_path).to eq(lamination_glue_types_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#lamination_glue_type_#{LaminationGlueType.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new lamination_glue_type' do
        click_on create_button
        expect(current_path).to eq(lamination_glue_types_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
