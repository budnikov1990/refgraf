require 'features_helper'

RSpec.feature 'Update template_background_price' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:template) { create(:template) }
    given!(:template_background_provider) { create(:template_background_provider) }
    given!(:template_background_price) { create(:template_background_price, template: template, template_background_provider: template_background_provider) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_template_background_price_path(template_background_price, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        select template.name, from: 'template_background_price_template_id'
        select template_background_provider.name, from: 'template_background_price_template_background_provider_id'
        click_on save_button

        expect(current_path).to eq(edit_template_background_price_path(template_background_price, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        select '', from: 'template_background_price_template_background_provider_id'
        click_on save_button
        expect(current_path).to eq(template_background_price_path(template_background_price, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
