require 'features_helper'

RSpec.feature 'View list of lamination_indices' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_glue_system_1) { create(:lamination_glue_system) }
    given!(:lamination_index) { create(:lamination_index, lamination_glue_system_1_id: lamination_glue_system_1.id) }

    background do
      login admin
      visit lamination_indices_path
    end

    scenario 'User sees list of lamination_indices' do
      within "#lamination_index_#{lamination_index.id}" do
        expect(page).to have_content lamination_index.value
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
