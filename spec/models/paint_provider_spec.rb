require 'rails_helper'

RSpec.describe PaintProvider, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  # it { is_expected.to validate_presence_of(:yellow) }
  # it { is_expected.to validate_presence_of(:magenta) }
  # it { is_expected.to validate_presence_of(:cyan) }
  # it { is_expected.to validate_presence_of(:black) }
  # it { is_expected.to monetize(:price_yellow) }
  # it { is_expected.to monetize(:price_magenta) }
  # it { is_expected.to monetize(:price_cyan) }
  # it { is_expected.to monetize(:price_black) }

  it 'relations' do
    is_expected.to have_many(:paints)
  end
end
