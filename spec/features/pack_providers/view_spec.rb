require 'features_helper'

RSpec.feature 'View list of pack_providers' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:pack_provider) { create(:pack_provider) }

    background do
      login admin
      visit pack_providers_path
    end

    scenario 'User sees list of pack_providers' do
      within "#pack_provider_#{pack_provider.id}" do
        expect(page).to have_content pack_provider.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
