require 'rails_helper'

RSpec.describe LaminationRotoGlueNamePolicy do
  subject { described_class }

  context 'By administrator' do
    let(:user) { build(:user, role: 'admin') }

    permissions :index?, :new?, :create? do
      it 'grants access' do
        expect(subject).to permit(user, LaminationRotoGlueName)
      end
    end

    permissions :edit?, :update? do
      let(:record) { build(:lamination_roto_glue_name) }
      let(:user) { build(:user, role: 'admin') }

      it 'grants access' do
        expect(subject).to permit(user, record)
      end
    end

    permissions :destroy? do
      let(:record) { build(:lamination_roto_glue_name) }
      let(:user) { build(:user, role: 'admin') }

      it 'grants access if other user is destroyed' do
        expect(subject).to permit(user, record)
      end
    end
  end

  %w(technologist operator storeman).each do |role|
    context "By #{role}" do
      let(:user) { build(:user, role: role) }
      let(:record) { build(:lamination_roto_glue_name) }

      permissions :index?, :new?, :create? do
        it 'denies access' do
          expect(subject).not_to permit(user, LaminationRotoGlueName)
        end
      end

      permissions :edit?, :update?, :destroy? do
        let(:record) { build(:lamination_roto_glue_name) }

        it 'denies access' do
          expect(subject).not_to permit(user, record)
        end
      end
    end
  end
end
