require 'features_helper'

RSpec.feature 'View list of lacquering_indices' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lacquering_index) { create(:lacquering_index) }

    background do
      login admin
      visit lacquering_indices_path
    end

    scenario 'User sees list of lacquering_indices' do
      within "#lacquering_index_#{lacquering_index.id}" do
        expect(page).to have_content lacquering_index.coverage_ratio
        expect(page).to have_content lacquering_index.dry_weight
        expect(page).to have_content lacquering_index.wet_weight
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
