lacqueringPolymers = []
lacqueringTypes = []
lacqueringAniloxes = []
lacqueringNames = []
cylinders = []
laminationCylinders1 = []
laminationGlueTypes1 = []
laminationCylinders2 = []
laminationGlueTypes2 = []
orderTemplates = []

$ ->
  filterCylinders = (elem) ->
    filterValue = parseInt(elem.val()) || 'none'
    options = []
    options.push($('<option value="" selected></option>'))
    $.each cylinders, (i, cylinder) ->
      if cylinder[0] == filterValue
        options.push(cylinder[1])
    if (options.length == 0)
      $('.cylinders-select').closest('.form-group').hide()
      $('.cylinders-select').empty()
    else
      $('.cylinders-select').empty().html(options)
      $('.cylinders-select').closest('.form-group').show()
      $('.cylinders-select option').first().prop('selected', 'selected')

  filterLaminationCylinders1 = (elem) ->
    filterValue = parseInt(elem.val()) || 'none'
    options = []
    options.push($('<option value="" selected></option>'))
    $.each laminationCylinders1, (i, cylinder) ->
      if cylinder[0] == filterValue
        options.push(cylinder[1])
    if (options.length == 0)
      $('.lamination-cylinder_1-select').closest('.form-group').hide()
      $('.lamination-cylinder_1-select').empty()
    else
      $('.lamination-cylinder_1-select').empty().html(options)
      $('.lamination-cylinder_1-select').closest('.form-group').show()
      $('.lamination-cylinder_1-select option').first().prop('selected', 'selected')

  filterLaminationCylinders2 = (elem) ->
    filterValue = parseInt(elem.val()) || 'none'
    options = []
    options.push($('<option value="" selected></option>'))
    $.each laminationCylinders2, (i, cylinder) ->
      if cylinder[0] == filterValue
        options.push(cylinder[1])
    if (options.length == 0)
      $('.lamination-cylinder_2-select').closest('.form-group').hide()
      $('.lamination-cylinder_2-select').empty()
    else
      $('.lamination-cylinder_2-select').empty().html(options)
      $('.lamination-cylinder_2-select').closest('.form-group').show()
      $('.lamination-cylinder_2-select option').first().prop('selected', 'selected')

  filterlaminationGlueTypes1 = (elem) ->
    filterValue = parseInt(elem.val()) || 'none'
    options = []
    options.push($('<option value="" selected></option>'))
    $.each laminationGlueTypes1, (i, type) ->
      if type[0] == filterValue
        options.push(type[1])
    if (options.length == 0)
      $('.lamination-glue-type_1-select').closest('.form-group').hide()
      $('.lamination-glue-type_1-select').empty()
    else
      $('.lamination-glue-type_1-select').empty().html(options)
      $('.lamination-glue-type_1-select').closest('.form-group').show()
      $('.lamination-glue-type_1-select option').first().prop('selected', 'selected')

  filterlaminationGlueTypes2 = (elem) ->
    filterValue = parseInt(elem.val()) || 'none'
    options = []
    options.push($('<option value="" selected></option>'))
    $.each laminationGlueTypes2, (i, type) ->
      if type[0] == filterValue
        options.push(type[1])
    if (options.length == 0)
      $('.lamination-glue-type_2-select').closest('.form-group').hide()
      $('.lamination-glue-type_2-select').empty()
    else
      $('.lamination-glue-type_2-select').empty().html(options)
      $('.lamination-glue-type_2-select').closest('.form-group').show()
      $('.lamination-glue-type_2-select option').first().prop('selected', 'selected')

  filterLacqueringName = (elem) ->
    filterValue = parseInt(elem.val()) || 'none'
    options = []
    options.push($('<option value="" selected></option>'))
    $.each lacqueringNames, (i, type) ->
      if type[0] == filterValue
        options.push(type[1])
    if (options.length == 0)
      $('.lacquering-name-select').closest('.form-group').hide()
      $('.lacquering-name-select').empty()
    else
      $('.lacquering-name-select').empty().html(options)
      $('.lacquering-name-select').closest('.form-group').show()
      $('.lacquering-name-select option').first().prop('selected', 'selected')

  filterLacqueringType = (elem) ->
    filterValue = parseInt(elem.val()) || 'none'
    options = []
    options.push($('<option value="" selected></option>'))
    $.each lacqueringTypes, (i, type) ->
      if type[0] == filterValue
        options.push(type[1])
    if (options.length == 0)
      $('.lacquering-type-select').closest('.form-group').hide()
      $('.lacquering-type-select').empty()
    else
      $('.lacquering-type-select').empty().html(options)
      $('.lacquering-type-select').closest('.form-group').show()
      $('.lacquering-type-select option').first().prop('selected', 'selected')

  filterLacqueringAnilox = (elem) ->
    filterValue = parseInt(elem.val()) || 'none'
    options = []
    options.push($('<option value="" selected></option>'))
    $.each lacqueringAniloxes, (i, anilox) ->
      if anilox[0] == filterValue
        options.push(anilox[1])
    if (options.length == 1)
      $('.lacquering-anilox-select').closest('.form-group').hide()
      $('.lacquering-anilox-select').empty()
    else
      $('.lacquering-anilox-select').empty().html(options)
      $('.lacquering-anilox-select').closest('.form-group').show()
      $('.lacquering-anilox-select option').first().prop('selected', 'selected')

  filterLacqueringPolymer = (elem) ->
    filterValue = parseInt(elem.val()) || 'none'
    options = []
    options.push($('<option value="" selected></option>'))
    $.each lacqueringPolymers, (i, polymer) ->
      if polymer[0] == filterValue
        options.push(polymer[1])
    if (options.length == 1)
      $('.lacquering-polymer-select').closest('.form-group').hide()
      $('.lacquering-polymer-select').empty()
    else
      $('.lacquering-polymer-select').empty().html(options)
      $('.lacquering-polymer-select').closest('.form-group').show()
      $('.lacquering-polymer-select option').first().prop('selected', 'selected')

  filterOrderTemplate = (elem) ->
    filterValue = parseInt(elem.val()) || 'none'
    options = []
    options.push($('<option value="" selected></option>'))
    $.each orderTemplates, (i, template) ->
      if template[0] == filterValue
        options.push(template[1])
    if (options.length == 1)
      $('.order-template').closest('.form-group').hide()
      $('.order-template').empty()
    else
      $('.order-template').empty().html(options)
      $('.order-template').closest('.form-group').show()
      $('.order-template option').first().prop('selected', 'selected')

  filterInputParams = (elem) ->
    filterValue = elem.find('option:selected').text() || 'none'
    usePolymer = elem.find('option:selected').data('use-lacquering-polymer')
    $(".use-polymer, .not-use-polymer").closest('.form-group').hide()
    if usePolymer
      $(".use-polymer").closest('.form-group').show()
    else
      $(".not-use-polymer").closest('.form-group').show()


  if $('.lacquering-type-select').size() > 0
    $('.lacquering-type-select option').each ->
      $elem = $(this)
      value = $elem.val()
      name = $elem.html()
      if value != ''
        lacqueringTypes.push([parseInt($elem.data('machine-id')), $elem])

  if $('.lacquering-anilox-select').size() > 0
    $('.lacquering-anilox-select option').each ->
      $elem = $(this)
      value = $elem.val()
      name = $elem.html()
      if value != ''
        lacqueringAniloxes.push([parseInt($elem.data('machine-id')), $elem])

  if $('.lacquering-polymer-select').size() > 0
    $('.lacquering-polymer-select option').each ->
      $elem = $(this)
      value = $elem.val()
      name = $elem.html()
      if value != ''
        lacqueringPolymers.push([parseInt($elem.data('machine-id')), $elem])

  if $('.lacquering-name-select').size() > 0
    $('.lacquering-name-select option').each ->
      $elem = $(this)
      value = $elem.val()
      name = $elem.html()
      if value != ''
        lacqueringNames.push([parseInt($elem.data('machine-id')), $elem])

  if $('.cylinders-select').size() > 0
    $('.cylinders-select option').each ->
      $elem = $(this)
      value = $elem.val()
      name = $elem.html()
      if value != ''
        cylinders.push([parseInt($elem.data('machine-id')), $elem])

  if $('.lamination-cylinder_1-select').size() > 0
    $('.lamination-cylinder_1-select option').each ->
      $elem = $(this)
      value = $elem.val()
      name = $elem.html()
      if value != ''
        laminationCylinders1.push([parseInt($elem.data('machine-id')), $elem])

  if $('.lamination-cylinder_2-select').size() > 0
    $('.lamination-cylinder_2-select option').each ->
      $elem = $(this)
      value = $elem.val()
      name = $elem.html()
      if value != ''
        laminationCylinders2.push([parseInt($elem.data('machine-id')), $elem])

  if $('.lamination-glue-type_1-select').size() > 0
    $('.lamination-glue-type_1-select option').each ->
      $elem = $(this)
      value = $elem.val()
      name = $elem.html()
      if value != ''
        laminationGlueTypes1.push([parseInt($elem.data('lamination-type-id')), $elem])

  if $('.lamination-glue-type_2-select').size() > 0
    $('.lamination-glue-type_2-select option').each ->
      $elem = $(this)
      value = $elem.val()
      name = $elem.html()
      if value != ''
        laminationGlueTypes2.push([parseInt($elem.data('lamination-type-id')), $elem])

  if $('.order-template').size() > 0
    $('.order-template option').each ->
      $elem = $(this)
      value = $elem.val()
      name = $elem.html()
      if value != ''
        orderTemplates.push([parseInt($elem.data('product-structure-id')), $elem])

  $(document).on 'change', '.lacquering-machines-select', ->
    filterLacqueringType($(this))
    filterLacqueringAnilox($(this))
    filterLacqueringPolymer($(this))
    filterInputParams($(this))
    filterLacqueringName($(this))

  $(document).on 'change', '.machines-select', ->
    filterCylinders($(this))

  $(document).on 'change', '.lamination-machine_1-select', ->
    filterLaminationCylinders1($(this))

  $(document).on 'change', '.lamination-type_1-select', ->
    filterlaminationGlueTypes1($(this))

  $(document).on 'change', '.lamination-machine_2-select', ->
    filterLaminationCylinders2($(this))

  $(document).on 'change', '.lamination-type_2-select', ->
    filterlaminationGlueTypes2($(this))

  $(document).on 'change', '.order-product-structure', ->
    filterOrderTemplate($(this))

  filterLacqueringType($('.lacquering-machines-select'))
  filterLacqueringAnilox($('.lacquering-machines-select'))
  filterLacqueringPolymer($('.lacquering-machines-select'))
  filterInputParams($('.lacquering-machines-select'))
  filterLacqueringName($('.lacquering-machines-select'))
  filterCylinders($('.machines-select'))
  filterLaminationCylinders1($('.lamination-machine_1-select'))
  filterlaminationGlueTypes1($('.lamination-type_1-select'))
  filterLaminationCylinders2($('.lamination-machine_2-select'))
  filterlaminationGlueTypes2($('.lamination-type_2-select'))
  filterOrderTemplate($('.order-product-structure'))
