require 'features_helper'

RSpec.feature 'Destroy lamination_roto_linearity' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_roto_linearity) { create(:lamination_roto_linearity) }

    background do
      login admin
      visit lamination_roto_linearities_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lamination_roto_linearity', js: true do
      within "#lamination_roto_linearity_#{lamination_roto_linearity.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lamination_roto_linearities_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lamination_roto_linearity_#{lamination_roto_linearity.id}")
    end
  end
end
