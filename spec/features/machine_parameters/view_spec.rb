require 'features_helper'

RSpec.feature 'View list of machine_parameters' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:machine_parameter) { create(:machine_parameter) }

    background do
      login admin
      visit machine_parameters_path
    end

    scenario 'User sees list of machine_parameters' do
      within "#machine_parameter_#{machine_parameter.id}" do
        expect(page).to have_content machine_parameter.machine.name
        expect(page).to have_content machine_parameter.product_structure.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
