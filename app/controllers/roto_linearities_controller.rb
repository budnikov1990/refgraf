class RotoLinearitiesController < ApplicationController
  before_action :set_and_authorize_roto_linearity, only: [:edit, :update, :destroy]

  # GET /roto_linearities
  def index
    authorize RotoLinearity
    @roto_linearities = policy_scope(RotoLinearity)
    @roto_linearities = @roto_linearities.order(id: :asc)
    @roto_linearities = @roto_linearities.page(params[:page]).per(25)
  end

  # GET /roto_linearities/new
  def new
    authorize RotoLinearity
    @roto_linearity = RotoLinearity.new
  end

  # POST /roto_linearities
  def create
    authorize RotoLinearity
    @roto_linearity = RotoLinearity.new(roto_linearity_params)
    if @roto_linearity.save
      redirect_to roto_linearities_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /roto_linearities/:id/edit
  def edit
  end

  # PATCH/PUT /roto_linearities/:id
  def update
    if @roto_linearity.update(roto_linearity_params)
      redirect_to edit_roto_linearity_url(@roto_linearity), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /roto_linearities/:id
  def destroy
    if @roto_linearity.destroy
      redirect_to roto_linearities_url, notice: t('shared.destroyed')
    else
      redirect_to roto_linearities_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def roto_linearity_params
    params.require(:roto_linearity).permit(:name)
  end

  def set_and_authorize_roto_linearity
    @roto_linearity = RotoLinearity.find(params[:id])
    authorize @roto_linearity
  end
end
