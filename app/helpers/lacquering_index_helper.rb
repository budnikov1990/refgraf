module LacqueringIndexHelper
  def lacquering_type_data_for_select
    LacqueringType.all.map { |c| [c.value, c.id, data: { machine_id: c.machine.id }] }
  end

  def lacquering_anilox_data_for_select
    LacqueringAnilox.all.map { |c| [c.value, c.id, data: { machine_id: c.machine.id }] }
  end

  def lacquering_polymer_data_for_select
    LacqueringPolymer.all.map { |c| [c.value, c.id, data: { machine_id: c.machine.id }] }
  end

  def lacquering_name_data_for_select
    Paint.where(paint_type: 'lacquer').all.map { |c| [c.name, c.id, data: { machine_id: c.machine.id }] }
  end
end
