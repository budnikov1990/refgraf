FactoryGirl.define do
  factory :lamination_roto_index do
    lamination_roto_linearity
    lamination_roto_glue_type
    value_dry 1.5
    value_wet 1.5
  end
end
