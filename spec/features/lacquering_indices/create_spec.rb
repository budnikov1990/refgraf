require 'features_helper'

RSpec.feature 'Create new lacquering_index' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_lacquering_index_button) { t('lacquering_indices.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }
    given!(:machine) { create(:machine) }
    given!(:lacquering_type) { create(:lacquering_type, machine: machine) }
    given!(:lacquering_anilox) { create(:lacquering_anilox, machine: machine) }

    background do
      login admin
      visit lacquering_indices_path(locale: I18n.locale)
      click_on add_lacquering_index_button
    end

    context 'With valid data' do
      scenario 'Admin creates new lacquering_index' do
        select machine.name, from: 'lacquering_index_machine_id'
        select lacquering_type.value, from: 'lacquering_index_lacquering_type_id'
        select lacquering_anilox.value, from: 'lacquering_index_lacquering_anilox_id'
        click_on create_button

        expect(current_path).to eq(lacquering_indices_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#lacquering_index_#{LacqueringIndex.last.id}" do
          expect(page).to have_content machine.name
          expect(page).to have_content lacquering_type.value
          expect(page).to have_content lacquering_anilox.value
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new lacquering_index' do
        click_on create_button
        expect(current_path).to eq(lacquering_indices_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
