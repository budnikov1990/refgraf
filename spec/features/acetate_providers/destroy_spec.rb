require 'features_helper'

RSpec.feature 'Destroy acetate_provider' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:acetate_provider) { create(:acetate_provider) }

    background do
      login admin
      visit acetate_providers_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the acetate_provider', js: true do
      within "#acetate_provider_#{acetate_provider.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(acetate_providers_path(locale: I18n.locale))
      expect(page).to have_no_selector("#acetate_provider_#{acetate_provider.id}")
    end
  end
end
