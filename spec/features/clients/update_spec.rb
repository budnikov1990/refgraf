require 'features_helper'

RSpec.feature 'Update client' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:client) { create(:client) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_client_path(client, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'client_name', with: 'test1234'
        fill_in 'client_packing_method', with: 'test1234'
        click_on save_button

        expect(current_path).to eq(edit_client_path(client, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        fill_in 'client_name', with: ''
        click_on save_button
        expect(current_path).to eq(client_path(client, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
