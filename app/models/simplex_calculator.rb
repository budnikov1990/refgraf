class SimplexCalculator < BaseCalculator
  def netto_part
    @netto_part ||=
      @working_width * @order.template.background.raw_weight +
      @working_width * @order.ink_amount +
      @working_width * @order.panton_1_amount +
      @working_width * @order.panton_2_amount +
      @working_width * lacquering_index +
      @working_width * roto_index_dry
  end

  def lamination_mb
    0
  end

  def before_cutting_mb
    good_material
  end

  def good_material
    @good_material ||= (
    amount_mb -
        (netto * @machine_params.good_percent) -
        @order.machine.calculation_coefficient
    ).round(1)
  end

  def material_after_cut
    @material_after_cut ||= (
    good_material -
        (netto * @machine_params.after_cut_percent)
    ).round(1)
  end
end
