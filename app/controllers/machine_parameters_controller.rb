class MachineParametersController < ApplicationController
  before_action :set_and_authorize_machine_parameter, only: [:edit, :update, :destroy]

  # GET /machine_parameters
  def index
    authorize MachineParameter
    @machine_parameters = policy_scope(MachineParameter)
    @machine_parameters = @machine_parameters.order(id: :asc)
    @machine_parameters = @machine_parameters.page(params[:page]).per(25)
  end

  # GET /machine_parameters/new
  def new
    authorize MachineParameter
    @machine_parameter = MachineParameter.new
  end

  # POST /machine_parameters
  def create
    authorize MachineParameter
    @machine_parameter = MachineParameter.new(machine_parameter_params)
    if @machine_parameter.save
      redirect_to machine_parameters_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /machine_parameters/:id/edit
  def edit
  end

  # PATCH/PUT /machine_parameters/:id
  def update
    if @machine_parameter.update(machine_parameter_params)
      redirect_to edit_machine_parameter_url(@machine_parameter),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /machine_parameters/:id
  def destroy
    if @machine_parameter.destroy
      redirect_to machine_parameters_url, notice: t('shared.destroyed')
    else
      redirect_to machine_parameters_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def machine_parameter_params
    params.require(:machine_parameter).permit(
      :machine_id, :product_structure_id,
      :gross_percent, :good_percent, :after_cut_percent,
      :after_lamination_1_percent, :after_lamination_2_percent
    )
  end

  def set_and_authorize_machine_parameter
    @machine_parameter = MachineParameter.find(params[:id])
    authorize @machine_parameter
  end
end
