class ExchangeRatesUpdater
  def update
    bank.expire_rates!
  end

  private

  def bank
    @bank ||= begin
      mclb = Money::Bank::CurrencylayerBank.new(ExchangeRate)
      mclb.access_key = Settings::Currencylayer.access_key
      mclb
    end
  end
end
