FactoryGirl.define do
  factory :paint_price do
    price ""
    pack_amount 1
    pack_unit "MyString"
    paint_provider_references "MyString"
    paint nil
  end
end
