require 'features_helper'

RSpec.feature 'Update user' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:user) { create(:user, role: 'operator', email: 'old@example.com') }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_user_path(user, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates user info' do
        select t('user.role.technologist'), from: 'user_role'
        fill_in 'user_email', with: 'new@example.com'
        click_on save_button

        expect(current_path).to eq(edit_user_path(user, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update user info' do
        fill_in 'user_email', with: 'not a real email'
        click_on save_button
        expect(current_path).to eq(user_path(user, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
