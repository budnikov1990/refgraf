require 'features_helper'

RSpec.feature 'Create new azote_provider' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given(:add_azote_provider_button) { t('azote_providers.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit azote_providers_path(locale: I18n.locale)
      click_on add_azote_provider_button
    end

    context 'With valid data' do
      scenario 'Admin creates new azote_provider' do
        fill_in 'azote_provider_name', with: 'test1234'
        click_on create_button

        expect(current_path).to eq(azote_providers_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#azote_provider_#{AzoteProvider.last.id}" do
          expect(page).to have_content 'test1234'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new azote_provider' do
        click_on create_button
        expect(current_path).to eq(azote_providers_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
