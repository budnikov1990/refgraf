class WelcomeController < ApplicationController
  skip_after_action :verify_policy_scoped

  # GET /
  def index
    authorize :dashboard
    send("setup_#{current_user.role}")
  end

  private

  def setup_admin
  end

  def setup_technologist
    @orders = Order.where(workflow_state: %w(new))
  end

  def setup_operator
    @orders = Order.where(
      workflow_state: %w(
        prepared printing printed cutting cut
        laminating_first laminated_first
        laminating_second laminated_second
      )
    )
  end

  def setup_storeman
    @orders = Order.where(workflow_state: %w(in_preparation cut))
  end

  def setup_manager
    @orders = Order.where(
      workflow_state: %w(
        in_preparation prepared printing printed cutting cut
        laminating_first laminated_first
        laminating_second laminated_second
      )
    )
  end
end
