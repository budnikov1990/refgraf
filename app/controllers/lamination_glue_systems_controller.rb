class LaminationGlueSystemsController < ApplicationController
  before_action :set_and_authorize_lamination_glue_system,
                only: [:edit, :update, :destroy]

  # GET /lamination_glue_systems
  def index
    authorize LaminationGlueSystem
    @lamination_glue_systems = policy_scope(LaminationGlueSystem)
    @lamination_glue_systems = @lamination_glue_systems.order(id: :asc)
    @lamination_glue_systems = @lamination_glue_systems.page(params[:page]).per(25)
  end

  # GET /lamination_glue_systems/new
  def new
    authorize LaminationGlueSystem
    @lamination_glue_system = LaminationGlueSystem.new
  end

  # POST /lamination_glue_systems
  def create
    authorize LaminationGlueSystem
    @lamination_glue_system = LaminationGlueSystem.new(lamination_glue_system_params)
    if @lamination_glue_system.save
      redirect_to lamination_glue_systems_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lamination_glue_systems/:id/edit
  def edit
  end

  # PATCH/PUT /lamination_glue_systems/:id
  def update
    if @lamination_glue_system.update(lamination_glue_system_params)
      redirect_to edit_lamination_glue_system_url(@lamination_glue_system),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lamination_glue_systems/:id
  def destroy
    if @lamination_glue_system.destroy
      redirect_to lamination_glue_systems_url, notice: t('shared.destroyed')
    else
      redirect_to lamination_glue_systems_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lamination_glue_system_params
    params.require(:lamination_glue_system).permit(:name)
  end

  def set_and_authorize_lamination_glue_system
    @lamination_glue_system = LaminationGlueSystem.find(params[:id])
    authorize @lamination_glue_system
  end
end
