class LaminationTypesController < ApplicationController
  before_action :set_and_authorize_lamination_type, only: [:edit, :update, :destroy]

  # GET /lamination_types
  def index
    authorize LaminationType
    @lamination_types = policy_scope(LaminationType)
    @lamination_types = @lamination_types.order(id: :asc)
    @lamination_types = @lamination_types.page(params[:page]).per(25)
  end

  # GET /lamination_types/new
  def new
    authorize LaminationType
    @lamination_type = LaminationType.new
  end

  # POST /lamination_types
  def create
    authorize LaminationType
    @lamination_type = LaminationType.new(lamination_type_params)
    if @lamination_type.save
      redirect_to lamination_types_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lamination_types/:id/edit
  def edit
  end

  # PATCH/PUT /lamination_types/:id
  def update
    if @lamination_type.update(lamination_type_params)
      redirect_to edit_lamination_type_url(@lamination_type), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lamination_types/:id
  def destroy
    if @lamination_type.destroy
      redirect_to lamination_types_url, notice: t('shared.destroyed')
    else
      redirect_to lamination_types_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lamination_type_params
    params.require(:lamination_type).permit(:name)
  end

  def set_and_authorize_lamination_type
    @lamination_type = LaminationType.find(params[:id])
    authorize @lamination_type
  end
end
