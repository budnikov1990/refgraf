class LacqueringName < ActiveRecord::Base
  belongs_to :machine
  has_many :orders

  validates :value, presence: true
  validates :machine_id, presence: true
end
