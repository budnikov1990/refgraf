class OffsetPlateProvider < ActiveRecord::Base
  has_many :offset_plate_prices, dependent: :destroy
  has_many :orders, dependent: :restrict_with_error

  accepts_nested_attributes_for :offset_plate_prices,
                                reject_if: :all_blank,
                                allow_destroy: true

  validates :name, presence: true
end
