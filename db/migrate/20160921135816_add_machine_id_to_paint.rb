class AddMachineIdToPaint < ActiveRecord::Migration
  def change
    add_reference :paints, :machine, index: true, foreign_key: true
  end
end
