Rails.application.routes.draw do

  scope '(:locale)', locale: /en|pl/ do
    devise_for :users, controllers: { sessions: 'customized_devise/sessions' }

    root 'welcome#index'
    resources :users, except: :show
    resources :product_structures, except: :show
    resources :color_presets, except: :show
    resources :clients, except: :show
    resources :templates, except: :show
    resources :backgrounds, except: :show
    resources :machines, except: :show
    resources :clients, except: :show
    resources :machine_parameters, except: :show
    resources :lacquering_aniloxes, except: :show
    resources :lacquering_names, except: :show
    resources :lacquering_polymers, except: :show
    resources :lacquering_types, except: :show
    resources :lacquering_indices, except: :show
    resources :lamination_glues, except: :show
    resources :lamination_machines, except: :show
    resources :lamination_types, except: :show
    resources :lamination_glue_types, except: :show
    resources :lamination_glue_systems, except: :show
    resources :lamination_indices, except: :show
    resources :lamination_roto_glue_names, except: :show
    resources :lamination_roto_glue_types, except: :show
    resources :lamination_roto_linearities, except: :show
    resources :lamination_roto_presers, except: :show
    resources :lamination_roto_indices, except: :show
    resources :cutting_indices, except: :show
    resources :cutting_machines, except: :show
    resources :roto_linearities, except: :show
    resources :roto_paints, except: :show
    resources :roto_presers, except: :show
    resources :roto_indices, except: :show
    resources :paint_providers, except: :show
    resources :paints, except: :show
    resources :acetate_providers, except: :show
    resources :template_background_providers, except: :show
    resources :template_background_prices, except: :show
    resources :offset_plates, except: :show
    resources :offset_plate_providers, except: :show
    resources :azote_providers, except: :show
    resources :pack_providers, except: :show
    resources :orders do
      member do
        get :actualize_edit
        get :pdf_main
        get :pdf_for_print
        get :pdf_after_print
        get :pdf_lamination_1
        get :pdf_lamination_2
        get :price_edit
        patch :actualize_update
        patch :prepared_update
        patch :rollback_update
        patch :start_print_update
        patch :finish_print_update
        patch :start_cut_update
        patch :finish_cut_update
        patch :give_away_update
        patch :update_order_additional_info
        patch :cant_prepare_materials_update
        patch :start_lamination_first_update
        patch :finish_lamination_first_update
        patch :start_lamination_second_update
        patch :finish_lamination_second_update
        patch :price_update
      end
    end
  end
end
