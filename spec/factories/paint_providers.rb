FactoryGirl.define do
  factory :paint_provider do
    name 'test'
    yellow 'test 1'
    magenta 'test 2'
    cyan 'test 3'
    black 'test 4'
    price_yellow 1
    price_magenta 2
    price_cyan 3
    price_black 4
    price_yellow_currency 'EUR'
    price_magenta_currency 'EUR'
    price_cyan_currency 'EUR'
    price_black_currency 'EUR'
  end
end
