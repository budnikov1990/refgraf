# Drukarnia

## Project setup

1. Copy config files:

        $ cp config/database.yml.example config/database.yml
        $ cp config/secrets.yml.example config/secrets.yml

2. Install bundle

        $ gem install bundler
        $ bundle

3. Create and prepare database

        $ rake db:create
        $ rake db:migrate
        $ rake db:seed

4. Install git hooks

        $ overcommit --install
        $ overcommit --sign

## Tests

*Please, always run tests before commiting your changes*

You have to install PhantomJS 1.9.8 library first. In the version 2.0.0 fileupload functionality is broken.

    $ brew tap homebrew/versions
    $ brew update
    $ brew install phantomjs198

Now you can run the tests with the following command:

    $ rspec

## Deploy

If any of Puma options were updated in deploy config you have to upload a new config to the server with the following command:

    $ cap staging puma:config

Then run deploy process as usual:

    $ ssh-add
    $ cap staging deploy