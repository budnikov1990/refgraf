class AcetateProvidersController < ApplicationController
  before_action :set_and_authorize_acetate_provider, only: [:edit, :update, :destroy]

  # GET /acetate_providers
  def index
    authorize AcetateProvider
    @acetate_providers = policy_scope(AcetateProvider)
    @acetate_providers = @acetate_providers.order(id: :asc)
    @acetate_providers = @acetate_providers.page(params[:page]).per(25)
  end

  # GET /acetate_providers/new
  def new
    authorize AcetateProvider
    @acetate_provider = AcetateProvider.new
  end

  # POST /acetate_providers
  def create
    authorize AcetateProvider
    @acetate_provider = AcetateProvider.new(acetate_provider_params)
    if @acetate_provider.save
      redirect_to acetate_providers_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /acetate_providers/:id/edit
  def edit
  end

  # PATCH/PUT /acetate_providers/:id
  def update
    if @acetate_provider.update(acetate_provider_params)
      redirect_to edit_acetate_provider_url(@acetate_provider),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /acetate_providers/:id
  def destroy
    if @acetate_provider.destroy
      redirect_to acetate_providers_url, notice: t('shared.destroyed')
    else
      redirect_to acetate_providers_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def acetate_provider_params
    params.require(:acetate_provider).permit(
      :name,
      acetate_names_attributes: [
        :id, :name, :price, :price_currency,
        :pack_amount, :pack_unit, :_destroy
      ]
    )
  end

  def set_and_authorize_acetate_provider
    @acetate_provider = AcetateProvider.find(params[:id])
    authorize @acetate_provider
  end
end
