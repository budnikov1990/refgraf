require 'features_helper'

RSpec.feature 'Destroy template_background_provider' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:template_background_provider) { create(:template_background_provider) }

    background do
      login admin
      visit template_background_providers_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the template_background_provider', js: true do
      within "#template_background_provider_#{template_background_provider.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(template_background_providers_path(locale: I18n.locale))
      expect(page).to have_no_selector("#template_background_provider_#{template_background_provider.id}")
    end
  end
end
