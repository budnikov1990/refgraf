class LaminationGlueSystem < ActiveRecord::Base
  has_many :lamination_indices_1,
           class_name: 'LaminationIndex',
           foreign_key: 'lamination_glue_system_1_id',
           dependent: :restrict_with_error
  has_many :lamination_indices_2,
           class_name: 'LaminationIndex',
           foreign_key: 'lamination_glue_system_2_id',
           dependent: :restrict_with_error
  has_many :orders_duplex,
           class_name: 'Order',
           foreign_key: 'lamination_glue_system_1_id',
           dependent: :restrict_with_error
  has_many :orders_triplex,
           class_name: 'Order',
           foreign_key: 'lamination_glue_system_2_id',
           dependent: :restrict_with_error

  validates :name, presence: true
end
