# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

def create_users
  User.where(email: 'admin@drukarnia.pl').first_or_create!(
    role: 'admin',
    password: 'qwerty123',
    first_name: 'admin',
    last_name: 'admin'
  )
  User.where(email: 'technologist@drukarnia.pl').first_or_create!(
    role: 'technologist',
    password: 'qwerty123',
    first_name: 'technologist',
    last_name: 'technologist'
  )
  User.where(email: 'operator@drukarnia.pl').first_or_create!(
    role: 'operator',
    password: 'qwerty123',
    first_name: 'operator',
    last_name: 'operator'
  )
  User.where(email: 'storeman@drukarnia.pl').first_or_create!(
    role: 'storeman',
    password: 'qwerty123',
    first_name: 'storeman',
    last_name: 'storeman'
  )

  User.where(email: 'manager@drukarnia.pl').first_or_create!(
      role: 'manager',
      password: 'qwerty123',
      first_name: 'manager',
      last_name: 'manager'
  )
end

def create_product_structures
  %w(simplex duplex triplex).each do |structure|
    ProductStructure.where(uid: structure).first_or_create!(
      name: structure,
      uid: structure
    )
  end
end

def create_colors
  YAML.load_file("#{Rails.root}/db/seeds/color_presets.yml").each do |row|
    ColorPreset.where(name: row['name']).first_or_create!(
      name: row['name'], colors_number: row['colors_number'],
      offset_plates_count: row['offset_plates']
    )
  end
end

def build_template
  OpenStruct.new index: nil,
                 background_id: nil,
                 background_for_lamination_1_id: nil,
                 background_for_lamination_2_id: nil,
                 product_structure: nil,
                 lamination_scheme: nil
end

def create_templates
  Template.delete_all
  ActiveRecord::Base.connection.reset_pk_sequence!('templates')
  schemes = []

  YAML.load_file("#{Rails.root}/db/seeds/template_schemat.yml").each_with_index do |row|
    schemes.push(row['val'])
  end

  r1 = []

  YAML.load_file("#{Rails.root}/db/seeds/template_r1.yml").each_with_index do |row|
    r1.push(row['val'])
  end

  r2 = []

  YAML.load_file("#{Rails.root}/db/seeds/template_r2.yml").each_with_index do |row|
    r2.push(row['val'])
  end

  r3 = []

  YAML.load_file("#{Rails.root}/db/seeds/template_r3.yml").each_with_index do |row|
    r3.push(row['val'])
  end

  raw_names = []

  YAML.load_file("#{Rails.root}/db/seeds/template_names.yml").each_with_index do |row, index|
    val = row['val']

    if val =~ /^1\./
      name = val
      raw_names.push(name)
    else
      name = raw_names.last + "\n#{val}"
      raw_names[raw_names.length - 1] = name
    end
  end

  raw_names.each_with_index do |val, index|
    template = Template.new
    template.lamination_scheme = schemes[index]

    names = val.split(/\n/)

    names.each_with_index do |name, index_2|
      raw_name = name.gsub(/\d\.\s/, '')

      raw_weight = if index_2.eql?(0)
                     r1[index] || 0
                   elsif index_2.eql?(1)
                     r2[index] || 0
                   elsif index_2.eql?(2)
                     r3[index] || 0
                   end

      background = Background.where(name: raw_name).first

      if background.nil?
        background = Background.new(name: raw_name, raw_weight: raw_weight)
        background.save
      end

      if index_2.eql?(0)
        template.product_structure = ProductStructure.find_by_uid('simplex')
        template.background_id = background.id
      elsif index_2.eql?(1)
        template.product_structure = ProductStructure.find_by_uid('duplex')
        template.background_for_lamination_1_id = background.id
      elsif index_2.eql?(2)
        template.product_structure = ProductStructure.find_by_uid('triplex')
        template.background_for_lamination_2_id = background.id
      end
    end

    template.save
  end
end

def create_machines_with_cylinders
  YAML.load_file("#{Rails.root}/db/seeds/machines.yml").each do |row|
    machine = Machine.where(name: row['name']).first_or_create!(
      name: row['name'],
      calculation_coefficient: row['coefficient'],
      max_colors_count: row['max_colors_count'],
      use_roto: row['use_roto'],
      use_lacquering_polymer: row['use_lacquering_polymer'],
      use_azote: row['use_azote'],
      azote_count: row['azote_count'],
      price_clear: row['price_clear'],
      price_clear_currency: row['price_clear_currency']
    )
    row['cylinders'].each do |cylinder|
      Cylinder
        .where(name: cylinder['name'], perimeter: cylinder['perimeter'])
        .first_or_create!(
          name: cylinder['name'],
          perimeter: cylinder['perimeter'],
          machine: machine
        )
    end
  end
end

def create_clients
  YAML.load_file("#{Rails.root}/db/seeds/clients.yml").each do |row|
    Client
      .where(name: row['name'], packing_method: row['packing_method'])
      .first_or_create!
  end
end

def create_machine_parameters
  YAML.load_file("#{Rails.root}/db/seeds/machine_parameters.yml").each do |row|
    machine = Machine.find_by(name: row['machine'])
    product_structure = ProductStructure.find_by(uid: row['product_structure'])
    MachineParameter.where(machine: machine, product_structure: product_structure)
        .first_or_create!(
            machine: machine,
            product_structure: product_structure,
            gross_percent: row['gross_percent'],
            good_percent: row['good_percent'],
            after_lamination_1_percent: row['after_lamination_1_percent'],
            after_lamination_2_percent: row['after_lamination_2_percent'],
            after_cut_percent: row['after_cut_percent']
        )
  end
end

def create_lacquering_aniloxes
  YAML.load_file("#{Rails.root}/db/seeds/lacquering_aniloxes.yml").each do |row|
    machine = Machine.find_by(name: row['machine'])
    LacqueringAnilox.where(machine: machine, value: row['value']).first_or_create!
  end
end

def create_lacquering_names
  YAML.load_file("#{Rails.root}/db/seeds/lacquering_names.yml").each do |row|
    machine = Machine.find_by(name: row['machine'])
    LacqueringName.where(machine: machine, value: row['value']).first_or_create!
  end
end

def create_lacquering_polymers
  YAML.load_file("#{Rails.root}/db/seeds/lacquering_polymers.yml").each do |row|
    machine = Machine.find_by(name: row['machine'])
    LacqueringPolymer.where(machine: machine, value: row['value']).first_or_create!
  end
end

def create_lacquering_types
  YAML.load_file("#{Rails.root}/db/seeds/lacquering_types.yml").each do |row|
    machine = Machine.find_by(name: row['machine'])
    LacqueringType.where(machine: machine, value: row['value']).first_or_create!
  end
end

def create_lacquering_indices
  YAML.load_file("#{Rails.root}/db/seeds/lacquering_indices.yml").each do |row|
    machine = Machine.find_by(name: row['machine'])
    lacquering_type = LacqueringType.find_by(value: row['lacquering_type'])

    where = {
        machine: machine,
        lacquering_type: lacquering_type
    }

    insert = {
        machine: machine,
        lacquering_type: lacquering_type,
        coverage_ratio: row['coverage_ratio'],
        dry_weight: row['dry_weight'],
        wet_weight: row['wet_weight']
    }

    unless row['lacquering_anilox'].blank?
      lacquering_anilox = LacqueringAnilox.find_by(value: row['lacquering_anilox'])
      where[:lacquering_anilox] = lacquering_anilox
      insert[:lacquering_anilox] = lacquering_anilox
    end

    unless row['lacquering_polymer'].blank?
      lacquering_polymer = LacqueringPolymer.find_by(value: row['lacquering_polymer'])
      where[:lacquering_polymer] = lacquering_polymer
      insert[:lacquering_polymer] = lacquering_polymer
    end

    LacqueringIndex.where(where).first_or_create!(insert)
  end
end

def create_lamination_glues
  YAML.load_file("#{Rails.root}/db/seeds/lamination_glues.yml").each do |row|
    LaminationGlue.where(name: row['name']).first_or_create!
  end
end

def create_lamination_machines_with_cylinders
  YAML.load_file("#{Rails.root}/db/seeds/lamination_machines.yml").each do |row|
    machine = LaminationMachine.where(name: row['name']).first_or_create!(
      name: row['name'],
      use_lamination_roto: row['use_lamination_roto']
    )
    row['cylinders'].each do |cylinder|
      LaminationCylinder
          .where(value: cylinder['value'], lamination_machine: machine).first_or_create!
    end
  end
end

def create_lamination_types
  YAML.load_file("#{Rails.root}/db/seeds/lamination_types.yml").each do |row|
    LaminationType.where(name: row['name']).first_or_create!
  end
end

def create_lamination_glue_types
  YAML.load_file("#{Rails.root}/db/seeds/lamination_glue_types.yml").each do |row|
    type = LaminationType.find_by(name: row['type'])
    LaminationGlueType.where(
      name: row['name'], lamination_type: type
    ).first_or_create!
  end
end

def create_lamination_glue_systems
  YAML.load_file("#{Rails.root}/db/seeds/lamination_glue_systems.yml").each do |row|
    LaminationGlueSystem.where(name: row['name']).first_or_create!
  end
end

def create_lamination_indices
  YAML.load_file("#{Rails.root}/db/seeds/lamination_indices.yml").each do |row|
    glue_system_1 = LaminationGlueSystem.find_by(
      name: row['lamination_glue_system_1']
    )
    glue_system_2 = LaminationGlueSystem.find_by(
      name: row['lamination_glue_system_2']
    )
    machine = LaminationMachine.find_by(
        name: row['lamination_machine']
    )
    LaminationIndex.where(
      lamination_glue_system_1: glue_system_1,
      lamination_glue_system_2: glue_system_2,
      lamination_machine: machine,
      value: row['value']
    ).first_or_create!
  end
end

def create_lamination_roto_glue_names
  YAML.load_file("#{Rails.root}/db/seeds/lamination_roto_glue_names.yml").each do |row|
    LaminationRotoGlueName.where(name: row['name'])
      .first_or_create!(
        name: row['name'],
        resin_name: row['resin_name'],
        hardener_name: row['hardener_name']
      )
  end
end

def create_lamination_roto_glue_types
  YAML.load_file("#{Rails.root}/db/seeds/lamination_roto_glue_types.yml").each do |row|
    LaminationRotoGlueType.where(name: row['name']).first_or_create!
  end
end

def create_lamination_roto_linearities
  YAML.load_file("#{Rails.root}/db/seeds/lamination_roto_linearities.yml").each do |row|
    LaminationRotoLinearity.where(value: row['value']).first_or_create!
  end
end

def create_lamination_roto_presers
  YAML.load_file("#{Rails.root}/db/seeds/lamination_roto_presers.yml").each do |row|
    LaminationRotoPreser.where(value: row['value']).first_or_create!
  end
end

def create_lamination_roto_indicess
  YAML.load_file("#{Rails.root}/db/seeds/lamination_roto_indices.yml").each do |row|
    lamination_roto_linearity = LaminationRotoLinearity.find_by(
      value: row['lamination_roto_linearity']
    )
    lamination_roto_glue_type = LaminationRotoGlueType.find_by(
      name: row['lamination_roto_glue_type']
    )
    LaminationRotoIndex.where(
        lamination_roto_linearity: lamination_roto_linearity,
        lamination_roto_glue_type: lamination_roto_glue_type,
        value_dry: row['value_dry'],
        value_wet: row['value_wet']
    ).first_or_create!
  end
end

def create_roto_linearities
  YAML.load_file("#{Rails.root}/db/seeds/roto_linearities.yml").each do |row|
    RotoLinearity.where(name: row['name']).first_or_create!
  end
end

def create_roto_paints
  YAML.load_file("#{Rails.root}/db/seeds/roto_paints.yml").each do |row|
    RotoPaint.where(name: row['name']).first_or_create!
  end
end

def create_roto_presers
  YAML.load_file("#{Rails.root}/db/seeds/roto_presers.yml").each do |row|
    RotoPreser.where(value: row['value']).first_or_create!
  end
end

def create_roto_indices
  YAML.load_file("#{Rails.root}/db/seeds/roto_indices.yml").each do |row|
    linearity = RotoLinearity.find_by(name: row['linearity'])
    RotoIndex.where(roto_linearity: linearity)
      .first_or_create!(
        roto_linearity: linearity,
        value_dry: row['value_dry'],
        value_wet: row['value_wet']
      )
  end
end

def create_paints
  providers = []
  names = []
  cents = []
  currencies = []
  machines = []
  types = []
  ymcb = []

  YAML.load_file("#{Rails.root}/db/seeds/paint_sources.yml").each do |row|
    providers.push(row['val'])
  end

  YAML.load_file("#{Rails.root}/db/seeds/paint_names.yml").each do |row|
    names.push(row['val'])
  end

  YAML.load_file("#{Rails.root}/db/seeds/paint_cents.yml").each do |row|
    cents.push(row['val'])
  end

  YAML.load_file("#{Rails.root}/db/seeds/paint_currencies.yml").each do |row|
    currencies.push(row['val'])
  end

  YAML.load_file("#{Rails.root}/db/seeds/paint_types.yml").each do |row|
    types.push(row['val'])
  end

  YAML.load_file("#{Rails.root}/db/seeds/paint_machines.yml").each do |row|
    machines.push(row['val'])
  end

  YAML.load_file("#{Rails.root}/db/seeds/colors_ymcb.yml").each do |row|
    ymcb.push(OpenStruct.new(type: 'yellow', name: row['yellow']))
    ymcb.push(OpenStruct.new(type: 'magenta', name: row['magenta']))
    ymcb.push(OpenStruct.new(type: 'cyan', name: row['cyan']))
    ymcb.push(OpenStruct.new(type: 'black', name: row['black']))
  end

  names.each_with_index do |name, index|
    paint_type = case types[index]
    when ''
      pt = ''

      ymcb.each do |color|
        if color.name == name
          pt = color.type
        end
      end

      pt = 'additional' if pt.blank?

      pt
    when 'PMS'
      'additional'
    when 'PMS Baza'
      'additional'
    when 'lakier'
      'lacquer'
    when 'Roto'
      'roto'
    end

    provider = PaintProvider.where(name: providers[index]).first_or_create!
    machine = Machine.find_by_name(machines[index])

    paint = Paint.where(paint_type: paint_type, name: name, machine_id: machine.id).first_or_create!

    PaintPrice.create(price_cents: cents[index] * 1000, price_currency: currencies[index], pack_unit: 'kg', paint_provider_id: provider.id, paint_id: paint.id)
  end
end

def create_acetate_providers
  YAML.load_file("#{Rails.root}/db/seeds/acetate_providers.yml").each do |row|
    provider = AcetateProvider.where(name: row['name']).first_or_create!
    row['acetate_names'].each do |ac_name|
      AcetateName.where(name: ac_name['name']).first_or_create!(
        name: ac_name['name'],
        price: ac_name['price'],
        price_currency: ac_name['currency'],
        acetate_provider: provider
      )
    end
  end
end

def create_offset_plates
  YAML.load_file("#{Rails.root}/db/seeds/offset_plates.yml").each do |row|
    machine = Machine.find_by(name: row['machine'])
    cylinder = Cylinder.find_by(name: row['cylinder'])
    OffsetPlate.where(
      name: row['name'],
      machine: machine,
      cylinder: cylinder
    ).first_or_create!
  end
end

def create_azote_providers
  YAML.load_file("#{Rails.root}/db/seeds/azote_providers.yml").each do |row|
    AzoteProvider.where(name: row['name']).first_or_create!(
      name: row['name'],
      price: row['price'],
      price_currency: row['price_currency'],
      pack_unit: row['pack_unit']
    )
  end
end

def create_pack_providers
  YAML.load_file("#{Rails.root}/db/seeds/pack_providers.yml").each do |row|
    PackProvider.where(name: row['name']).first_or_create!
  end
end

def create_cutting_machines
  YAML.load_file("#{Rails.root}/db/seeds/cutting_machines.yml").each do |row|
    CuttingMachine.where(name: row['name'], vpzp: row['vpzp'], hour_cost_cents: row['hour_cost'], hour_cost_currency: row['hour_cost_currency']).first_or_create!
  end
end

def create_cutting_indices
  YAML.load_file("#{Rails.root}/db/seeds/cutting_indices.yml").each do |row|
    CuttingIndex.where(cutting_machine_id: row['cutting_machine_id'], product_structure_id: row['product_structure_id'], value: row['value']).first_or_create!
  end
end

def update_gum_cleaning
  YAML.load_file("#{Rails.root}/db/seeds/machines_gum_cleaning.yml").each do |row|
    Machine.find(row['id']).update(gum_cleaning_202: row['gum_cleaning_202'], gum_cleaning_204: row['gum_cleaning_204'], gum_cleaning_101_203: row['gum_cleaning_101_203'])
  end
end

def create_machine_template_speeds
  [1, 2].each do |machine|
    YAML.load_file("#{Rails.root}/db/seeds/machine_#{machine}_vpbp.yml").each_with_index do |row, index|
      MachineTemplateSpeed.where(template_id: index + 1, machine_id: machine, vpbp: row['val']).first_or_create!
    end
  end
end

def create_machine_template_spendings
  [1, 2].each do |machine|
    more = []
    less = []

    YAML.load_file("#{Rails.root}/db/seeds/machine_#{machine}_thousand_less.yml").each do |row|
      less.push(row['val'])
    end

    YAML.load_file("#{Rails.root}/db/seeds/machine_#{machine}_thousand_more.yml").each do |row|
      more.push(row['val'])
    end

    more.each_with_index do |obj, index|
      MachineTemplateSpending.where(template_id: index + 1, machine_id: machine, thousand_less: less[index], thousand_more: more[index]).first_or_create!
    end
  end
end

def create_template_background_price
  providers = []

  YAML.load_file("#{Rails.root}/db/seeds/background_provider.yml").each_with_index do |row|
    providers.push(row['val'])
  end

  cents = []

  YAML.load_file("#{Rails.root}/db/seeds/background_cents.yml").each_with_index do |row|
    cents.push(row['val'] * 1000)
  end

  currencies = []

  YAML.load_file("#{Rails.root}/db/seeds/background_currency.yml").each_with_index do |row|
    currencies.push(row['val'])
  end

  names = []

  YAML.load_file("#{Rails.root}/db/seeds/background_name.yml").each_with_index do |row|
    names.push(row['val'])
  end

  template_names = []

  YAML.load_file("#{Rails.root}/db/seeds/template_cost_name.yml").each_with_index do |row|
    template_names.push(row['val'])
  end

  template_real_names = []

  YAML.load_file("#{Rails.root}/db/seeds/template_cost_real_name.yml").each_with_index do |row|
    template_real_names.push(row['val'])
  end

  template_real_names.each_with_index do |name, indexr|
    background = Background.where(name: name).first

    price_name = template_names[indexr]

    if price_name.present? && background.present?

      names.each_with_index do |name, index|
        if name == price_name
          bp = TemplateBackgroundProvider.where(name: providers[index]).first_or_create!

          prices = background.template_background_prices.where(template_background_provider_id: bp.id)

          if prices.length.eql?(0)
            price = background.template_background_prices.build(template_background_provider_id: bp.id)
          else
            price = prices.first
          end

          if cents[index] > 0
            price.price_cents = cents[index]
            price.price_currency = currencies[index]
            price.pack_unit = 'kg'
            price.save
          end
        end
      end
    end
  end
end

create_users
create_product_structures
create_colors
create_templates
create_machines_with_cylinders
create_clients
create_machine_parameters
create_lacquering_aniloxes
create_lacquering_names
create_lacquering_polymers
create_lacquering_types
create_lacquering_indices
create_lamination_glues
create_lamination_machines_with_cylinders
create_lamination_types
create_lamination_glue_types
create_lamination_glue_systems
create_lamination_indices
create_lamination_roto_glue_names
create_lamination_roto_glue_types
create_lamination_roto_linearities
create_lamination_roto_presers
create_lamination_roto_indicess
create_roto_linearities
create_roto_paints
create_roto_presers
create_roto_indices
create_paints
create_acetate_providers
create_offset_plates
create_azote_providers
create_pack_providers
create_cutting_machines
create_cutting_indices
update_gum_cleaning
create_machine_template_speeds
create_machine_template_spendings
create_template_background_price
