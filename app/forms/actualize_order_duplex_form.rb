class ActualizeOrderDuplexForm < ActualizeOrderForm
  attribute :lamination_machine_1_id, Integer
  attribute :lamination_cylinder_1_id, Integer
  attribute :lamination_type_1_id, Integer
  attribute :lamination_glue_1_id, Integer
  attribute :lamination_glue_system_1_id, Integer
  attribute :lamination_glue_type_1_id, Integer
  attribute :canvas_width_lamination_1, Integer

  attribute :lamination_roto_glue_name_1_id, Integer
  attribute :lamination_roto_glue_type_1_id, Integer
  attribute :lamination_roto_linearity_1_id, Integer
  attribute :lamination_roto_preser_1_id, Integer
  attribute :lamination_roto_glue_viscosity_1, Integer
  attribute :lamination_roto_acetate_1, Float
  attribute :lamination_roto_resin_1, Float
  attribute :lamination_roto_hardener_1, Float

  validate :check_canvas_width_lamination_1
  validates :canvas_width_lamination_1, presence: true

  validates :lamination_machine_1, :lamination_machine_1_id, presence: true
  validates :lamination_cylinder_1, :lamination_cylinder_1_id,
            presence: true, unless: :check_lamination1_roto?
  validates :lamination_type_1, :lamination_type_1_id, presence: true
  validates :lamination_glue_1, :lamination_glue_1_id,
            presence: true,
            unless: :check_lamination1_roto?
  validates :lamination_glue_system_1, :lamination_glue_system_1_id, presence: true
  validates :lamination_glue_type_1, :lamination_glue_type_1_id, presence: true

  validates :lamination_roto_glue_name_1, :lamination_roto_glue_name_1_id,
            presence: true,
            if: :check_lamination1_roto?
  validates :lamination_roto_glue_type_1, :lamination_roto_glue_type_1_id,
            presence: true,
            if: :check_lamination1_roto?
  validates :lamination_roto_linearity_1, :lamination_roto_linearity_1_id,
            presence: true,
            if: :check_lamination1_roto?
  validates :lamination_roto_preser_1, :lamination_roto_preser_1_id,
            presence: true,
            if: :check_lamination1_roto?

  validates :lamination_roto_glue_viscosity_1,
            presence: true, if: :check_lamination1_roto?
  validates :lamination_roto_acetate_1, presence: true, if: :check_lamination1_roto?
  validates :lamination_roto_resin_1, presence: true, if: :check_lamination1_roto?
  validates :lamination_roto_hardener_1, presence: true, if: :check_lamination1_roto?

  private

  def lamination_params
    @lamination_params = {
      lamination_machine_1: lamination_machine_1,
      lamination_cylinder_1: lamination_cylinder_1,
      lamination_glue_1: lamination_glue_1, lamination_type_1: lamination_type_1,
      lamination_glue_system_1: lamination_glue_system_1,
      lamination_glue_type_1: lamination_glue_type_1,
      canvas_width_lamination_1: canvas_width_lamination_1,
      lamination_roto_glue_name_1: lamination_roto_glue_name_1,
      lamination_roto_glue_type_1: lamination_roto_glue_type_1,
      lamination_roto_linearity_1: lamination_roto_linearity_1,
      lamination_roto_preser_1: lamination_roto_preser_1,
      lamination_roto_glue_viscosity_1: lamination_roto_glue_viscosity_1,
      lamination_roto_acetate_1: lamination_roto_acetate_1,
      lamination_roto_resin_1: lamination_roto_resin_1,
      lamination_roto_hardener_1: lamination_roto_hardener_1
    }
  end

  def lamination_machine_1
    @lamination_machine ||= LaminationMachine.find_by(id: lamination_machine_1_id)
  end

  def lamination_cylinder_1
    @lamination_cylinder ||= LaminationCylinder.find_by(id: lamination_cylinder_1_id)
  end

  def lamination_glue_1
    @lamination_glue ||= LaminationGlue.find_by(id: lamination_glue_1_id)
  end

  def lamination_glue_system_1
    @lamination_glue_system ||=
      LaminationGlueSystem.find_by(id: lamination_glue_system_1_id)
  end

  def lamination_type_1
    @lamination_type ||= LaminationType.find_by(id: lamination_type_1_id)
  end

  def lamination_glue_type_1
    @lamination_glue_type ||= LaminationGlueType.find_by(id: lamination_glue_type_1_id)
  end

  def lamination_roto_glue_name_1
    @lamination_roto_glue_name_1 ||=
      LaminationRotoGlueName.find_by(id: lamination_roto_glue_name_1_id)
  end

  def lamination_roto_glue_type_1
    @lamination_roto_glue_type_1 ||=
      LaminationRotoGlueType.find_by(id: lamination_roto_glue_type_1_id)
  end

  def lamination_roto_linearity_1
    @lamination_roto_linearity_1 ||=
      LaminationRotoLinearity.find_by(id: lamination_roto_linearity_1_id)
  end

  def lamination_roto_preser_1
    @lamination_roto_preser_1 ||=
      LaminationRotoPreser.find_by(id: lamination_roto_preser_1_id)
  end

  def check_canvas_width_lamination_1
    if canvas_width_lamination_1.present? && item_count_on_width.present?
      counted_canvas_width = order.width * item_count_on_width
      if canvas_width_lamination_1 <= counted_canvas_width
        errors.add(:canvas_width_lamination_1,
                   I18n.t('shared.errors.not_suitable_canvas_width_lamination',
                          p: counted_canvas_width.to_i
                         )
                  )
      end
    end
  end

  def check_lamination1_roto?
    (!lamination_machine_1.blank? &&
      !lamination_glue_system_1.blank?) &&
      (lamination_machine_1.use_lamination_roto &&
      lamination_glue_system_1.name == 'rozpuszczalnikowy')
  end
end
