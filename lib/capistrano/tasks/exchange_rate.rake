namespace :exchange_rate do
  desc 'Download the latest exchange rate from currencylayer.com'
  task :update do
    on roles(:app), in: :sequence, wait: 5 do
      within release_path do
        execute :bundle, "exec rake exchange_rate:update RAILS_ENV=#{fetch(:stage)}"
      end
    end
  end
end
