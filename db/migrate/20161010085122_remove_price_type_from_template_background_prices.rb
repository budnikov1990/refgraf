class RemovePriceTypeFromTemplateBackgroundPrices < ActiveRecord::Migration
  def change
    remove_column :template_background_prices, :price_type, :string
  end
end
