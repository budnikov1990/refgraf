class OffsetPlateProvidersController < ApplicationController
  before_action :set_and_authorize_offset_plate_provider, only: [:edit, :update, :destroy]

  # GET /offset_plate_providers
  def index
    authorize OffsetPlateProvider
    @offset_plate_providers = policy_scope(OffsetPlateProvider)
    @offset_plate_providers = @offset_plate_providers.order(id: :asc)
    @offset_plate_providers = @offset_plate_providers.page(params[:page]).per(25)
  end

  # GET /offset_plate_providers/new
  def new
    authorize OffsetPlateProvider
    @offset_plate_provider = OffsetPlateProvider.new
  end

  # POST /offset_plate_providers
  def create
    authorize OffsetPlateProvider
    @offset_plate_provider = OffsetPlateProvider.new(offset_plate_provider_params)
    if @offset_plate_provider.save
      redirect_to offset_plate_providers_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /offset_plate_providers/:id/edit
  def edit
  end

  # PATCH/PUT /offset_plate_providers/:id
  def update
    if @offset_plate_provider.update(offset_plate_provider_params)
      redirect_to edit_offset_plate_provider_url(@offset_plate_provider),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /offset_plate_providers/:id
  def destroy
    if @offset_plate_provider.destroy
      redirect_to offset_plate_providers_url, notice: t('shared.destroyed')
    else
      redirect_to offset_plate_providers_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def offset_plate_provider_params
    params.require(:offset_plate_provider).permit(
      :name,
      offset_plate_prices_attributes: [
        :id, :price, :price_currency,
        :pack_amount, :pack_unit,
        :offset_plate_id, :_destroy
      ]
    )
  end

  def set_and_authorize_offset_plate_provider
    @offset_plate_provider = OffsetPlateProvider.find(params[:id])
    authorize @offset_plate_provider
  end
end
