require 'rails_helper'

RSpec.describe OrderPolicy do
  subject { described_class }

  context 'By technologist' do
    let(:user) { build(:user, role: 'technologist') }

    permissions :index?, :new?, :create? do
      it 'grants access' do
        expect(subject).to permit(user, Order)
      end
    end

    permissions :edit?, :update? do
      let!(:product_structure) { create(:product_structure) }
      let!(:template) { create(:template, product_structure: product_structure) }
      let(:record) { create(:order, product_structure: product_structure, template: template) }
      let(:user) { build(:user, role: 'technologist') }

      it 'grants access' do
        expect(subject).to permit(user, record)
      end
    end

    permissions :destroy? do
      let!(:product_structure) { create(:product_structure) }
      let!(:template) { create(:template, product_structure: product_structure) }
      let(:record) { create(:order, product_structure: product_structure, template: template) }
      let(:user) { build(:user, role: 'technologist') }

      it 'grants access if other order is destroyed' do
        expect(subject).to permit(user, record)
      end
    end
  end
end
