require 'features_helper'

RSpec.feature 'Destroy lamination_machine' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_machine) { create(:lamination_machine) }

    background do
      login admin
      visit lamination_machines_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lamination_machine', js: true do
      within "#lamination_machine_#{lamination_machine.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lamination_machines_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lamination_machine_#{lamination_machine.id}")
    end
  end
end
