require 'features_helper'

RSpec.feature 'View list of lamination_glue_systems' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_glue_system) { create(:lamination_glue_system) }

    background do
      login admin
      visit lamination_glue_systems_path
    end

    scenario 'User sees list of lamination_glue_systems' do
      within "#lamination_glue_system_#{lamination_glue_system.id}" do
        expect(page).to have_content lamination_glue_system.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
