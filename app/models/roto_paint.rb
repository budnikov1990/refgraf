class RotoPaint < ActiveRecord::Base
  has_many :orders, dependent: :restrict_with_error

  validates :name, presence: true
end
