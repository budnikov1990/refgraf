class TemplateBackgroundProvidersController < ApplicationController
  before_action :set_and_authorize_template_background_provider,
                only: [:edit, :update, :destroy]

  # GET /template_background_providers
  def index
    authorize TemplateBackgroundProvider
    @template_background_providers = policy_scope(TemplateBackgroundProvider)
    @template_background_providers =
      @template_background_providers.order(id: :asc)
    @template_background_providers =
      @template_background_providers.page(params[:page]).per(25)
  end

  # GET /template_background_providers/new
  def new
    authorize TemplateBackgroundProvider
    @template_background_provider = TemplateBackgroundProvider.new
  end

  # POST /template_background_providers
  def create
    authorize TemplateBackgroundProvider
    @template_background_provider =
      TemplateBackgroundProvider.new(template_background_provider_params)
    if @template_background_provider.save
      redirect_to template_background_providers_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /template_background_providers/:id/edit
  def edit
  end

  # PATCH/PUT /template_background_providers/:id
  def update
    if @template_background_provider.update(template_background_provider_params)
      redirect_to edit_template_background_provider_url(@template_background_provider),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /template_background_providers/:id
  def destroy
    if @template_background_provider.destroy
      redirect_to template_background_providers_url, notice: t('shared.destroyed')
    else
      redirect_to template_background_providers_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def template_background_provider_params
    params.require(:template_background_provider).permit(:name)
  end

  def set_and_authorize_template_background_provider
    @template_background_provider = TemplateBackgroundProvider.find(params[:id])
    authorize @template_background_provider
  end
end
