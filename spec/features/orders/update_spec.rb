require 'features_helper'

RSpec.feature 'Update order' do
  context 'with authenticated user' do
    given(:technologist) { create(:user, role: 'technologist') }
    given!(:product_structure) { create(:product_structure) }
    given!(:template) { create(:template, product_structure: product_structure) }
    given!(:order) { create(:order, product_structure: product_structure, template: template) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login technologist
      visit edit_order_path(order, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'order_form_name', with: 'test123456'
        click_on save_button

        expect(current_path).to eq(order_path(order, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        fill_in 'order_form_name', with: ''
        click_on save_button
        expect(current_path).to eq(order_path(order, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
