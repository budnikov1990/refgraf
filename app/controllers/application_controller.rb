class ApplicationController < ActionController::Base
  include Pundit

  # https://github.com/elabs/pundit#ensuring-policies-are-used
  after_action :verify_authorized
  after_action :verify_policy_scoped, only: :index

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Everything is restricted for guests
  before_action :authenticate_user!

  before_action :set_locale

  def default_url_options(options = {})
    { locale: I18n.locale }.merge(options)
  end

  protected

  def set_locale
    I18n.locale = params[:locale] ||
                  I18n.default_locale
  end
end
