require 'features_helper'

RSpec.feature 'View list of roto_paints' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:roto_paint) { create(:roto_paint) }

    background do
      login admin
      visit roto_paints_path
    end

    scenario 'User sees list of roto_paints' do
      within "#roto_paint_#{roto_paint.id}" do
        expect(page).to have_content roto_paint.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
