class LacqueringIndex < ActiveRecord::Base
  belongs_to :machine
  belongs_to :lacquering_type
  belongs_to :lacquering_anilox
  belongs_to :lacquering_polymer

  validates :machine_id, presence: true
  validates :lacquering_type_id, presence: true
end
