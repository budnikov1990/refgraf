class LacqueringPolymer < ActiveRecord::Base
  belongs_to :machine
  has_many :orders
  has_many :lacquering_indices, dependent: :restrict_with_error

  validates :value, presence: true
  validates :machine_id, presence: true
end
