class OrderPolicy < ApplicationPolicy
  def index?
    not_admin?
  end

  def show?
    not_admin?
  end

  def new?
    create?
  end

  def create?
    technologist?
  end

  def edit?
    update?
  end

  def update?
    technologist? && record.new?
  end

  def destroy?
    technologist? && record.new?
  end

  def actualize_edit?
    actualize_update?
  end

  def actualize_update?
    technologist? && record.new?
  end

  def price_edit?
    price_update?
  end

  def price_update?
    manager? && record.in_preparation?
  end

  def price_show_edit?
    manager? &&
      record.current_state >= :in_preparation
  end

  def price_show_tab?
    (manager? || technologist?) &&
      record.current_state >= :in_preparation &&
      record.need_price_calculation
  end

  def prepared_update?
    storeman? && record.in_preparation?
  end

  def rollback_update?
    operator? && record.prepared?
  end

  def start_print_update?
    operator? && record.prepared?
  end

  def finish_print_update?
    operator? && record.printing?
  end

  def start_lamination_first_update?
    operator? && record.printed? && (duplex? || triplex?)
  end

  def finish_lamination_first_update?
    operator? && record.laminating_first? && (duplex? || triplex?)
  end

  def start_lamination_second_update?
    operator? && record.laminated_first? && triplex?
  end

  def finish_lamination_second_update?
    operator? && record.laminating_second? && triplex?
  end

  def start_cut_update?
    if duplex?
      operator? && record.laminated_first?
    elsif triplex?
      operator? && record.laminated_second?
    else
      operator? && record.printed?
    end
  end

  def finish_cut_update?
    operator? && record.cutting?
  end

  def give_away_update?
    storeman? && record.cut?
  end

  def pdf_main?
    not_admin?
  end

  def pdf_lamination_1?
    lamination1?
  end

  def pdf_lamination_2?
    lamination2?
  end

  def pdf_for_print?
    (operator? || technologist?) && record.current_state > :new
  end

  def pdf_after_print?
    (operator? || technologist?) &&
      (
        (simplex? && record.current_state >= :printing) ||
        (duplex? && record.current_state >= :laminating_first) ||
        (triplex? && record.current_state >= :laminating_second)
      )
  end

  def update_order_additional_info?
    (operator? || technologist?) && record.current_state > :in_preparation
  end

  def show_log?
    technologist?
  end

  def cant_prepare_materials_update?
    storeman? &&
      record.in_preparation? &&
      !record.additional_parameter.cant_prepare_materials
  end

  def lamination1?
    (technologist? || operator?) && (duplex? || triplex?) &&
      record.current_state >= :printing
  end

  def lamination2?
    (technologist? || operator?) && triplex? &&
      record.current_state >= :laminating_first
  end

  def show_timeline?
    user.present? && !user.manager?
  end

  private

  def simplex?
    record.product_structure.uid == 'simplex'
  end

  def duplex?
    record.product_structure.uid == 'duplex'
  end

  def triplex?
    record.product_structure.uid == 'triplex'
  end

  def technologist?
    user.present? && user.technologist?
  end

  def operator?
    user.present? && user.operator?
  end

  def storeman?
    user.present? && user.storeman?
  end

  def not_admin?
    user.present? && !user.admin?
  end

  def manager?
    user.present? && user.manager?
  end
end
