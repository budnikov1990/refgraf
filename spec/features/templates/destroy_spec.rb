require 'features_helper'

RSpec.feature 'Destroy template' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:product_structure) { create(:product_structure) }
    given!(:template) { create(:template, product_structure: product_structure) }

    background do
      login admin
      visit templates_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the template', js: true do
      within "#template_#{template.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(templates_path(locale: I18n.locale))
      expect(page).to have_no_selector("#template_#{template.id}")
    end
  end
end
