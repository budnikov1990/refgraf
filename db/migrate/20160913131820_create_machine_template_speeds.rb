class CreateMachineTemplateSpeeds < ActiveRecord::Migration
  def change
    create_table :machine_template_speeds do |t|
      t.integer :vpbp
      t.references :template, index: true, foreign_key: true
      t.references :machine, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
