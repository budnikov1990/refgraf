require 'features_helper'

RSpec.feature 'Destroy lamination_roto_glue_name' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_roto_glue_name) { create(:lamination_roto_glue_name) }

    background do
      login admin
      visit lamination_roto_glue_names_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lamination_roto_glue_name', js: true do
      within "#lamination_roto_glue_name_#{lamination_roto_glue_name.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lamination_roto_glue_names_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lamination_roto_glue_name_#{lamination_roto_glue_name.id}")
    end
  end
end
