require 'rails_helper'

RSpec.describe UserPolicy do
  subject { described_class }

  context 'By administrator' do
    let(:user) { build(:user, role: 'admin') }

    permissions :index?, :new?, :create? do
      it 'grants access' do
        expect(subject).to permit(user, User)
      end
    end

    permissions :edit?, :update? do
      let(:record) { build(:user) }

      it 'grants access' do
        expect(subject).to permit(user, record)
      end
    end

    permissions :destroy? do
      let(:record) { create(:user) }

      it 'grants access if other user is destroyed' do
        expect(subject).to permit(user, record)
      end

      it 'denies access if user itself is destroyed' do
        expect(subject).not_to permit(user, user)
      end
    end
  end

  %w(technologist operator storeman).each do |role|
    context "By #{role}" do
      let(:user) { build(:user, role: role) }

      permissions :index?, :new?, :create? do
        it 'denies access' do
          expect(subject).not_to permit(user, User)
        end
      end

      permissions :edit?, :update?, :destroy? do
        let(:record) { build(:user) }

        it 'denies access' do
          expect(subject).not_to permit(user, record)
        end
      end
    end
  end
end
