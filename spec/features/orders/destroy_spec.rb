require 'features_helper'

RSpec.feature 'Destroy order' do
  context 'by authenticated user' do
    given(:technologist) { create(:user, role: 'technologist') }
    given!(:product_structure) { create(:product_structure) }
    given!(:template) { create(:template, product_structure: product_structure) }
    given!(:order) { create(:order, product_structure: product_structure, template: template) }
    given(:destroy_button) { t('orders.show.actions.destroy') }

    background do
      login technologist
      visit orders_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the order', js: true do
      within "#order_#{order.id}" do
        click_on order.name
      end
      within '#actions' do
        click_on destroy_button
      end
      expect(current_path).to eq(orders_path(locale: I18n.locale))
      expect(page).to have_no_selector("#order_#{order.id}")
    end
  end
end
