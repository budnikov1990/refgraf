require 'features_helper'

RSpec.feature 'View list of lacquering_polymers' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lacquering_polymer) { create(:lacquering_polymer) }

    background do
      login admin
      visit lacquering_polymers_path
    end

    scenario 'User sees list of lacquering_polymers' do
      within "#lacquering_polymer_#{lacquering_polymer.id}" do
        expect(page).to have_content lacquering_polymer.value
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
