require 'features_helper'

RSpec.feature 'Destroy roto_linearity' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:roto_linearity) { create(:roto_linearity) }

    background do
      login admin
      visit roto_linearities_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the roto_linearity', js: true do
      within "#roto_linearity_#{roto_linearity.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(roto_linearities_path(locale: I18n.locale))
      expect(page).to have_no_selector("#roto_linearity_#{roto_linearity.id}")
    end
  end
end
