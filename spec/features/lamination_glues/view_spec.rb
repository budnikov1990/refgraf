require 'features_helper'

RSpec.feature 'View list of lamination_glues' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_glue) { create(:lamination_glue) }

    background do
      login admin
      visit lamination_glues_path
    end

    scenario 'User sees list of lamination_glues' do
      within "#lamination_glue_#{lamination_glue.id}" do
        expect(page).to have_content lamination_glue.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
