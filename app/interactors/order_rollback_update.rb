class OrderRollbackUpdate
  include Interactor
  include TransactionalInteractor

  def call
    context.order.rollback!
    log
  end

  private

  delegate :params, :user, :order, to: :context

  def log
    Activity.create!(
      key: 'order.rollback',
      trackable: order,
      owner: user,
      data: log_params
    )
  end

  def log_params
    {
      id: context.order.id,
      title: context.order.name,
      number: context.order.number,
      previous_state: 'prepared',
      new_state: 'in_preparation'
    }
  end
end
