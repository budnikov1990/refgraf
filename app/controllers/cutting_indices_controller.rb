class CuttingIndicesController < ApplicationController
  before_action :set_and_authorize_cutting_index, only: [:edit, :update, :destroy]

  # GET /cutting_indices
  def index
    authorize CuttingIndex
    @cutting_indices = policy_scope(CuttingIndex)
    @cutting_indices = @cutting_indices.order(id: :asc)
    @cutting_indices = @cutting_indices.page(params[:page]).per(25)
  end

  # GET /cutting_indices/new
  def new
    authorize CuttingIndex
    @cutting_index = CuttingIndex.new
  end

  # POST /cutting_indices
  def create
    authorize CuttingIndex
    @cutting_index = CuttingIndex.new(cutting_index_params)
    if @cutting_index.save
      redirect_to cutting_indices_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /cutting_indices/:id/edit
  def edit
  end

  # PATCH/PUT /cutting_indices/:id
  def update
    if @cutting_index.update(cutting_index_params)
      redirect_to edit_cutting_index_url(@cutting_index), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /cutting_indices/:id
  def destroy
    if @cutting_index.destroy
      redirect_to cutting_indices_url, notice: t('shared.destroyed')
    else
      redirect_to cutting_indices_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def cutting_index_params
    params.require(:cutting_index).permit(
      :value, :cutting_machine_id, :product_structure_id
    )
  end

  def set_and_authorize_cutting_index
    @cutting_index = CuttingIndex.find(params[:id])
    authorize @cutting_index
  end
end
