class RotoLinearity < ActiveRecord::Base
  has_many :roto_indices, dependent: :restrict_with_error
  has_many :orders, dependent: :restrict_with_error

  validates :name, presence: true
end
