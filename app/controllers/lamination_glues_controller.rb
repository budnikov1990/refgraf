class LaminationGluesController < ApplicationController
  before_action :set_and_authorize_lamination_glue, only: [:edit, :update, :destroy]

  # GET /lamination_glues
  def index
    authorize LaminationGlue
    @lamination_glues = policy_scope(LaminationGlue)
    @lamination_glues = @lamination_glues.order(id: :asc)
    @lamination_glues = @lamination_glues.page(params[:page]).per(25)
  end

  # GET /lamination_glues/new
  def new
    authorize LaminationGlue
    @lamination_glue = LaminationGlue.new
  end

  # POST /lamination_glues
  def create
    authorize LaminationGlue
    @lamination_glue = LaminationGlue.new(lamination_glue_params)
    if @lamination_glue.save
      redirect_to lamination_glues_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lamination_glues/:id/edit
  def edit
  end

  # PATCH/PUT /lamination_glues/:id
  def update
    if @lamination_glue.update(lamination_glue_params)
      redirect_to edit_lamination_glue_url(@lamination_glue), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lamination_glues/:id
  def destroy
    if @lamination_glue.destroy
      redirect_to lamination_glues_url, notice: t('shared.destroyed')
    else
      redirect_to lamination_glues_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lamination_glue_params
    params.require(:lamination_glue).permit(:name)
  end

  def set_and_authorize_lamination_glue
    @lamination_glue = LaminationGlue.find(params[:id])
    authorize @lamination_glue
  end
end
