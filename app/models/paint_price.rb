class PaintPrice < ActiveRecord::Base
  belongs_to :paint
  belongs_to :paint_provider

  monetize :price_cents

  validates :price_cents, :pack_unit, presence: true
end
