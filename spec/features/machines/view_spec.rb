require 'features_helper'

RSpec.feature 'View list of machines' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:machine) { create(:machine) }

    background do
      login admin
      visit machines_path
    end

    scenario 'User sees list of machines' do
      within "#machine_#{machine.id}" do
        expect(page).to have_content machine.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
