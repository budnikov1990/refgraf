require 'rails_helper'

RSpec.describe MachineParameter, type: :model do
  it { is_expected.to validate_presence_of(:machine_id) }
  it { is_expected.to validate_presence_of(:product_structure_id) }
  it { is_expected.to validate_numericality_of(:gross_percent).is_greater_than_or_equal_to(0).is_less_than_or_equal_to(0) }
  it { is_expected.to validate_numericality_of(:good_percent).is_greater_than_or_equal_to(0).is_less_than_or_equal_to(0) }
  it { is_expected.to validate_numericality_of(:after_lamination_1_percent).is_greater_than_or_equal_to(0).is_less_than_or_equal_to(0) }
  it { is_expected.to validate_numericality_of(:after_lamination_2_percent).is_greater_than_or_equal_to(0).is_less_than_or_equal_to(0) }
  it { is_expected.to validate_numericality_of(:after_cut_percent).is_greater_than_or_equal_to(0).is_less_than_or_equal_to(0) }
end
