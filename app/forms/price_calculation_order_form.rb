class PriceCalculationOrderForm
  include ActiveModel::Model
  include Virtus.model

  attr_reader :order

  attribute :yellow_percent, Integer
  attribute :magenta_percent, Integer
  attribute :magenta_percent, Integer
  attribute :cyan_percent, Integer
  attribute :black_percent, Integer

  attribute :panton_1_provider_id, Integer
  attribute :panton_2_provider_id, Integer
  attribute :roto_paint_provider_id, Integer
  attribute :lacquering_provider_id, Integer

  attribute :yellow_provider_id, Integer
  attribute :magenta_provider_id, Integer
  attribute :cyan_provider_id, Integer
  attribute :black_provider_id, Integer

  attribute :background_for_print_provider_id, Integer
  attribute :background_for_lamination1_provider_id, Integer
  attribute :background_for_lamination2_provider_id, Integer
  attribute :offset_plate_provider_id, Integer
  attribute :azote_provider_id, Integer
  attribute :pack_provider_id, Integer

  attribute :price_additional, BigDecimal
  attribute :price_additional_currency, String

  attribute :price_pack, BigDecimal
  attribute :price_pack_currency, String
  attribute :pack_name, String
  attribute :pack_amount, Integer

  validate :check_paint_percent_sum

  validates :yellow_provider, :yellow_provider_id, presence: true
  validates :magenta_provider, :magenta_provider_id, presence: true
  validates :cyan_provider, :cyan_provider_id, presence: true
  validates :black_provider, :black_provider_id, presence: true
  validates :offset_plate_provider, :offset_plate_provider_id, presence: true
  validates :pack_provider, :pack_provider_id, presence: true

  validates :price_pack, presence: true
  validates :price_pack_currency, presence: true
  validates :pack_name, presence: true
  validates :pack_amount, presence: true

  validates :panton_1_provider_id, presence: true, if: :check_panton_1?
  validates :panton_2_provider_id, presence: true, if: :check_panton_2?

  validates :background_for_print_provider,
            :background_for_print_provider_id,
            presence: true

  validates :background_for_lamination1_provider,
            :background_for_lamination1_provider_id,
            presence: true,
            if: :duplex_or_triplex?

  validates :background_for_lamination2_provider,
            :background_for_lamination2_provider_id,
            presence: true,
            if: :triplex?

  validates :azote_provider,
            :azote_provider_id,
            presence: true,
            if: :use_azote?

  validates :yellow_percent,
            presence: true,
            numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }
  validates :magenta_percent,
            presence: true,
            numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }
  validates :cyan_percent,
            presence: true,
            numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }
  validates :black_percent,
            presence: true,
            numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }

  def initialize(options = {})
    @order = options[:order]
    @user = options[:user]
    apply_order
    super(options[:params])
  end

  def save
    if valid?
      persist!
    else
      false
    end
  end

  private

  def persist!
    ActiveRecord::Base.transaction { persist_without_transaction }
    true
  rescue
    false
  end

  def duplex_or_triplex?
    @order.duplex || @order.triplex
  end

  def triplex?
    @order.triplex
  end

  def check_panton_1?
    order.color_preset.name == 'CMYK_PMS1' || check_panton_2?
  end

  def check_panton_2?
    order.color_preset.name == 'CMYK_PMS2'
  end

  def persist_without_transaction
    order.update!(insert_params)
  end

  def insert_params
    @insert_params = {
      yellow_percent: yellow_percent, magenta_percent: magenta_percent,
      cyan_percent: cyan_percent, black_percent: black_percent,
      yellow_provider: yellow_provider, magenta_provider: magenta_provider,
      cyan_provider: cyan_provider, black_provider: black_provider,
      panton_1_provider: panton_1_provider, panton_2_provider: panton_2_provider,
      roto_paint_provider: roto_paint_provider, lacquering_provider_id: lacquering_provider_id,
      background_for_print_provider: background_for_print_provider,
      background_for_lamination1_provider: background_for_lamination1_provider,
      background_for_lamination2_provider: background_for_lamination2_provider,
      need_price_calculation: true, offset_plate_provider: offset_plate_provider,
      azote_provider: azote_provider, price_additional: price_additional,
      price_additional_currency: price_additional_currency,
      pack_provider: pack_provider, price_pack: price_pack,
      price_pack_currency: price_pack_currency, pack_name: pack_name,
      pack_amount: pack_amount
    }
  end

  def apply_order
    attributes.each do |attribute, _|
      public_send("#{attribute}=", order.public_send(attribute))
    end
  end

  def magenta_provider
    @magenta_provider ||= PaintProvider.find_by(id: magenta_provider_id)
  end

  def yellow_provider
    @yellow_provider ||= PaintProvider.find_by(id: yellow_provider_id)
  end

  def cyan_provider
    @cyan_provider ||= PaintProvider.find_by(id: cyan_provider_id)
  end

  def black_provider
    @black_provider ||= PaintProvider.find_by(id: black_provider_id)
  end

  def panton_1_provider
    @panton_1_provider ||= PaintProvider.find_by(id: panton_1_provider_id)
  end

  def panton_2_provider
    @panton_2_provider ||= PaintProvider.find_by(id: panton_2_provider_id)
  end

  def roto_paint_provider
    @roto_paint_provider ||= PaintProvider.find_by(id: roto_paint_provider_id)
  end

  def lacquering_provider
    @lacquering_provider ||= PaintProvider.find_by(id: lacquering_provider_id)
  end

  def background_for_print_provider
    @background_for_print_provider ||=
      TemplateBackgroundProvider.find_by(id: background_for_print_provider_id)
  end

  def background_for_lamination1_provider
    @background_for_lamination1_provider ||=
      TemplateBackgroundProvider.find_by(id: background_for_lamination1_provider_id)
  end

  def background_for_lamination2_provider
    @background_for_lamination2_provider ||=
      TemplateBackgroundProvider.find_by(id: background_for_lamination2_provider_id)
  end

  def offset_plate_provider
    @offset_plate_provider ||=
      OffsetPlateProvider.find_by(id: offset_plate_provider_id)
  end

  def azote_provider
    @azote_provider ||= AzoteProvider.find_by(id: azote_provider_id)
  end

  def pack_provider
    @pack_provider ||= PackProvider.find_by(id: pack_provider_id)
  end

  def check_paint_percent_sum
    sum = yellow_percent.to_i +
          magenta_percent.to_i +
          cyan_percent.to_i +
          black_percent.to_i
    if sum != 100
      errors.add(:yellow_percent, I18n.t('shared.errors.sum_percent', p: sum.to_i))
      errors.add(:magenta_percent, I18n.t('shared.errors.sum_percent', p: sum.to_i))
      errors.add(:cyan_percent, I18n.t('shared.errors.sum_percent', p: sum.to_i))
      errors.add(:black_percent, I18n.t('shared.errors.sum_percent', p: sum.to_i))
    end
  end

  def use_azote?
    order.machine.use_azote
  end
end
