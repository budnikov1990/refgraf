class TemplateBackgroundPrice < ActiveRecord::Base
  belongs_to :background
  belongs_to :template_background_provider

  monetize :price_cents

  validates :background_id, :template_background_provider_id,
            :price_cents, presence: true
end
