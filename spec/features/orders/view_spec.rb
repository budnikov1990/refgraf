require 'features_helper'

RSpec.feature 'View list of orders' do
  context 'by authenticated user' do
    given(:technologist) { create(:user, role: 'technologist') }
    given!(:product_structure) { create(:product_structure) }
    given!(:template) { create(:template, product_structure: product_structure) }
    given!(:order) { create(:order, product_structure: product_structure, template: template) }

    background do
      login technologist
      visit orders_path
    end

    scenario 'User sees list of orders' do
      within "#order_#{order.id}" do
        expect(page).to have_content order.name
      end
    end
  end
end
