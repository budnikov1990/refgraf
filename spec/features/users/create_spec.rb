require 'features_helper'

RSpec.feature 'Create new user' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given(:add_user_button) { t('users.index.actions.add') }
    given(:create_button) { t('shared.create') }
    given(:success_message) { t('shared.created') }

    background do
      login admin
      visit users_path(locale: I18n.locale)
      click_on add_user_button
    end

    context 'With valid data' do
      scenario 'Admin creates new user' do
        select t('user.role.operator'), from: 'user_role'
        fill_in 'user_email', with: 'new_user@example.com'
        fill_in 'user_first_name', with: 'first name'
        fill_in 'user_last_name', with: 'last name'
        fill_in 'user_password', with: 'qwerty123'
        fill_in 'user_password_confirmation', with: 'qwerty123'
        click_on create_button

        expect(current_path).to eq(users_path(locale: I18n.locale))
        expect(page).to have_content success_message
        within "#user_#{User.last.id}" do
          expect(page).to have_content t('user.role.operator')
          expect(page).to have_content 'new_user@example.com'
        end
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to create a new user' do
        click_on create_button
        expect(current_path).to eq(users_path(locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
