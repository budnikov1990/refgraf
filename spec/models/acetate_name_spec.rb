require 'rails_helper'

RSpec.describe AcetateName, type: :model do
  it { is_expected.to validate_presence_of(:acetate_provider) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to monetize(:price) }
  it { is_expected.to validate_presence_of(:price_currency) }
end
