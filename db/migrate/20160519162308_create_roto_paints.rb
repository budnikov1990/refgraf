class CreateRotoPaints < ActiveRecord::Migration
  def change
    create_table :roto_paints do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
