require 'features_helper'

RSpec.feature 'View list of lamination_roto_indices' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_roto_index) { create(:lamination_roto_index) }

    background do
      login admin
      visit lamination_roto_indices_path
    end

    scenario 'User sees list of lamination_roto_indices' do
      within "#lamination_roto_index_#{lamination_roto_index.id}" do
        expect(page).to have_content lamination_roto_index.value_wet
        expect(page).to have_content lamination_roto_index.value_dry
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
