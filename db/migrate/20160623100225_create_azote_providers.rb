class CreateAzoteProviders < ActiveRecord::Migration
  def change
    create_table :azote_providers do |t|
      t.string :name
      t.monetize :price
      t.integer :pack_amount
      t.string :pack_unit

      t.timestamps null: false
    end
  end
end
