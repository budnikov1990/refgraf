require 'features_helper'

RSpec.feature 'View list of clients' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:client) { create(:client) }

    background do
      login admin
      visit clients_path
    end

    scenario 'User sees list of clients' do
      within "#client_#{client.id}" do
        expect(page).to have_content client.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
