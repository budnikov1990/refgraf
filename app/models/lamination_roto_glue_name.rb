class LaminationRotoGlueName < ActiveRecord::Base
  has_many :orders_duplex,
           class_name: 'Order',
           foreign_key: 'lamination_roto_glue_name_1_id',
           dependent: :restrict_with_error

  has_many :orders_triplex,
           class_name: 'Order',
           foreign_key: 'lamination_roto_glue_name_2_id',
           dependent: :restrict_with_error

  validates :name, presence: true
  validates :resin_name, presence: true
  validates :hardener_name, presence: true
end
