class Client < ActiveRecord::Base
  has_many :orders

  validates :name, presence: true
  validates :packing_method, presence: true
end
