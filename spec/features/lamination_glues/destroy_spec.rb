require 'features_helper'

RSpec.feature 'Destroy lamination_glue' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lamination_glue) { create(:lamination_glue) }

    background do
      login admin
      visit lamination_glues_path(locale: I18n.locale)
    end

    scenario 'Admin destroys the lamination_glue', js: true do
      within "#lamination_glue_#{lamination_glue.id}" do
        find('.fa-trash').click
      end
      expect(current_path).to eq(lamination_glues_path(locale: I18n.locale))
      expect(page).to have_no_selector("#lamination_glue_#{lamination_glue.id}")
    end
  end
end
