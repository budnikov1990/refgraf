class Paint < ActiveRecord::Base
  has_many :paint_prices
  belongs_to :machine

  accepts_nested_attributes_for :paint_prices,
                                reject_if: :all_blank,
                                allow_destroy: true

  validates :name, :paint_type, presence: true
end
