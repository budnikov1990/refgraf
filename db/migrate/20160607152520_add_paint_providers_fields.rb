class AddPaintProvidersFields < ActiveRecord::Migration
  def change
    add_column :paint_providers, :yellow_pack_amount, :integer
    add_column :paint_providers, :yellow_pack_unit, :string

    add_column :paint_providers, :magenta_pack_amount, :integer
    add_column :paint_providers, :magenta_pack_unit, :string

    add_column :paint_providers, :cyan_pack_amount, :integer
    add_column :paint_providers, :cyan_pack_unit, :string

    add_column :paint_providers, :black_pack_amount, :integer
    add_column :paint_providers, :black_pack_unit, :string
  end
end
