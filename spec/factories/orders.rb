FactoryGirl.define do
  factory :order do
    template
    color_preset
    machine
    cylinder
    product_structure
    name 'Nowe zlecenie'
    amount 1
    width 1.5
    height 1.5
    client
    number 'test number'
    item_count_on_width 1
    canvas_width 1
  end
end
