require 'features_helper'

RSpec.feature 'Update lacquering_index' do
  context 'with authenticated user' do
    given(:admin) { create(:user, role: 'admin') }
    given!(:lacquering_index) { create(:lacquering_index) }
    given(:save_button) { t('shared.save') }
    given(:success_message) { t('shared.updated') }

    background do
      login admin
      visit edit_lacquering_index_path(lacquering_index, locale: I18n.locale)
    end

    context 'With valid data' do
      scenario 'Admin updates structure info' do
        fill_in 'lacquering_index_coverage_ratio', with: '22'
        click_on save_button

        expect(current_path).to eq(edit_lacquering_index_path(lacquering_index, locale: I18n.locale))
        expect(page).to have_content success_message
      end
    end

    context 'With invalid data' do
      scenario 'Admin fails to update structure info' do
        select '', from: 'lacquering_index_machine_id'
        click_on save_button
        expect(current_path).to eq(lacquering_index_path(lacquering_index, locale: I18n.locale))
        expect(page).to have_no_content success_message
      end
    end
  end
end
