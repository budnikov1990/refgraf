# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161010085122) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "acetate_names", force: :cascade do |t|
    t.string   "name"
    t.integer  "price_cents",         default: 0,     null: false
    t.string   "price_currency",      default: "EUR", null: false
    t.integer  "acetate_provider_id"
    t.integer  "pack_amount"
    t.string   "pack_unit"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "acetate_providers", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "activities", force: :cascade do |t|
    t.string   "key"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.json     "data",           default: {}, null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "activities", ["key"], name: "index_activities_on_key", using: :btree
  add_index "activities", ["owner_type", "owner_id"], name: "index_activities_on_owner_type_and_owner_id", using: :btree
  add_index "activities", ["trackable_type", "trackable_id"], name: "index_activities_on_trackable_type_and_trackable_id", using: :btree

  create_table "additional_parameters", force: :cascade do |t|
    t.integer  "order_id"
    t.boolean  "cant_prepare_materials",         default: false, null: false
    t.text     "cant_prepare_materials_comment", default: "",    null: false
    t.text     "comments_after_print",           default: "",    null: false
    t.float    "wastepaper_incoming"
    t.float    "wastepaper_remaining"
    t.float    "material_for_store_got"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  add_index "additional_parameters", ["order_id"], name: "index_additional_parameters_on_order_id", using: :btree

  create_table "azote_providers", force: :cascade do |t|
    t.string   "name"
    t.integer  "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "EUR", null: false
    t.integer  "pack_amount"
    t.string   "pack_unit"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "backgrounds", force: :cascade do |t|
    t.string   "name"
    t.float    "raw_weight"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.string   "packing_method"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "outer_diameter"
  end

  create_table "color_presets", force: :cascade do |t|
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "name"
    t.integer  "colors_number"
    t.integer  "offset_plates_count"
  end

  create_table "cutting_indices", force: :cascade do |t|
    t.float    "value",                default: 0.0
    t.integer  "cutting_machine_id"
    t.integer  "product_structure_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "cutting_indices", ["cutting_machine_id"], name: "index_cutting_indices_on_cutting_machine_id", using: :btree
  add_index "cutting_indices", ["product_structure_id"], name: "index_cutting_indices_on_product_structure_id", using: :btree

  create_table "cutting_machines", force: :cascade do |t|
    t.string   "name"
    t.integer  "hour_cost_cents",    default: 0,     null: false
    t.string   "hour_cost_currency", default: "EUR", null: false
    t.integer  "vpzp",               default: 0
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "cylinders", force: :cascade do |t|
    t.string   "name"
    t.integer  "perimeter"
    t.integer  "machine_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "cylinders", ["machine_id"], name: "index_cylinders_on_machine_id", using: :btree

  create_table "exchange_rates", force: :cascade do |t|
    t.string   "from"
    t.string   "to"
    t.decimal  "rate",       precision: 15, scale: 6
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "exchange_rates", ["from", "to"], name: "index_exchange_rates_on_from_and_to", using: :btree
  add_index "exchange_rates", ["from"], name: "index_exchange_rates_on_from", using: :btree
  add_index "exchange_rates", ["to"], name: "index_exchange_rates_on_to", using: :btree

  create_table "lacquering_aniloxes", force: :cascade do |t|
    t.integer  "machine_id"
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "lacquering_aniloxes", ["machine_id"], name: "index_lacquering_aniloxes_on_machine_id", using: :btree

  create_table "lacquering_indices", force: :cascade do |t|
    t.integer  "machine_id"
    t.integer  "lacquering_type_id"
    t.integer  "lacquering_anilox_id"
    t.integer  "lacquering_polymer_id"
    t.float    "coverage_ratio",        default: 0.0
    t.float    "dry_weight",            default: 0.0
    t.float    "wet_weight",            default: 0.0
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "lacquering_indices", ["lacquering_anilox_id"], name: "index_lacquering_indices_on_lacquering_anilox_id", using: :btree
  add_index "lacquering_indices", ["lacquering_polymer_id"], name: "index_lacquering_indices_on_lacquering_polymer_id", using: :btree
  add_index "lacquering_indices", ["lacquering_type_id"], name: "index_lacquering_indices_on_lacquering_type_id", using: :btree
  add_index "lacquering_indices", ["machine_id"], name: "index_lacquering_indices_on_machine_id", using: :btree

  create_table "lacquering_names", force: :cascade do |t|
    t.integer  "machine_id"
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "lacquering_names", ["machine_id"], name: "index_lacquering_names_on_machine_id", using: :btree

  create_table "lacquering_polymers", force: :cascade do |t|
    t.integer  "machine_id"
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "lacquering_polymers", ["machine_id"], name: "index_lacquering_polymers_on_machine_id", using: :btree

  create_table "lacquering_types", force: :cascade do |t|
    t.integer  "machine_id"
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "lacquering_types", ["machine_id"], name: "index_lacquering_types_on_machine_id", using: :btree

  create_table "lamination_cylinders", force: :cascade do |t|
    t.float    "value"
    t.integer  "lamination_machine_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "lamination_cylinders", ["lamination_machine_id"], name: "index_lamination_cylinders_on_lamination_machine_id", using: :btree

  create_table "lamination_glue_systems", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lamination_glue_types", force: :cascade do |t|
    t.string   "name"
    t.integer  "lamination_type_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "lamination_glue_types", ["lamination_type_id"], name: "index_lamination_glue_types_on_lamination_type_id", using: :btree

  create_table "lamination_glues", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lamination_indices", force: :cascade do |t|
    t.integer  "lamination_machine_id"
    t.float    "value"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "lamination_glue_system_1_id"
    t.integer  "lamination_glue_system_2_id"
    t.integer  "vpzp",                        default: 0
  end

  add_index "lamination_indices", ["lamination_glue_system_1_id"], name: "index_lamination_indices_on_lamination_glue_system_1_id", using: :btree
  add_index "lamination_indices", ["lamination_glue_system_2_id"], name: "index_lamination_indices_on_lamination_glue_system_2_id", using: :btree
  add_index "lamination_indices", ["lamination_machine_id"], name: "index_lamination_indices_on_lamination_machine_id", using: :btree

  create_table "lamination_machines", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "use_lamination_roto", default: false
    t.integer  "hour_cost_cents"
    t.string   "hour_cost_currency"
  end

  create_table "lamination_roto_glue_names", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "resin_name"
    t.string   "hardener_name"
  end

  create_table "lamination_roto_glue_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lamination_roto_indices", force: :cascade do |t|
    t.integer  "lamination_roto_linearity_id"
    t.integer  "lamination_roto_glue_type_id"
    t.float    "value_dry"
    t.float    "value_wet"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "lamination_roto_indices", ["lamination_roto_glue_type_id"], name: "index_lamination_roto_indices_on_lamination_roto_glue_type_id", using: :btree
  add_index "lamination_roto_indices", ["lamination_roto_linearity_id"], name: "index_lamination_roto_indices_on_lamination_roto_linearity_id", using: :btree

  create_table "lamination_roto_linearities", force: :cascade do |t|
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lamination_roto_presers", force: :cascade do |t|
    t.integer  "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lamination_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "machine_parameters", force: :cascade, comment: "Machine params for product structures" do |t|
    t.integer  "machine_id"
    t.integer  "product_structure_id"
    t.float    "gross_percent",              default: 0.0
    t.float    "good_percent",               default: 0.0
    t.float    "after_lamination_1_percent", default: 0.0
    t.float    "after_lamination_2_percent", default: 0.0
    t.float    "after_cut_percent",          default: 0.0
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "machine_parameters", ["machine_id"], name: "index_machine_parameters_on_machine_id", using: :btree
  add_index "machine_parameters", ["product_structure_id"], name: "index_machine_parameters_on_product_structure_id", using: :btree

  create_table "machine_template_speeds", force: :cascade do |t|
    t.integer  "vpbp"
    t.integer  "template_id"
    t.integer  "machine_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "machine_template_speeds", ["machine_id"], name: "index_machine_template_speeds_on_machine_id", using: :btree
  add_index "machine_template_speeds", ["template_id"], name: "index_machine_template_speeds_on_template_id", using: :btree

  create_table "machine_template_spendings", force: :cascade do |t|
    t.integer  "thousand_more"
    t.integer  "thousand_less"
    t.integer  "machine_id"
    t.integer  "template_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "machine_template_spendings", ["machine_id"], name: "index_machine_template_spendings_on_machine_id", using: :btree
  add_index "machine_template_spendings", ["template_id"], name: "index_machine_template_spendings_on_template_id", using: :btree

  create_table "machines", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "calculation_coefficient", default: 0
    t.integer  "max_colors_count",        default: 0
    t.boolean  "use_roto",                default: false
    t.boolean  "use_lacquering_polymer",  default: false
    t.boolean  "use_azote",               default: false
    t.integer  "azote_count"
    t.integer  "price_clear_cents",       default: 0,     null: false
    t.string   "price_clear_currency",    default: "EUR", null: false
    t.integer  "gum_cleaning_202",        default: 0
    t.integer  "gum_cleaning_204",        default: 0
    t.integer  "gum_cleaning_101_203",    default: 0
    t.integer  "hour_cost_cents"
    t.string   "hour_cost_currency"
  end

  create_table "offset_plate_prices", force: :cascade do |t|
    t.integer  "price_cents",              default: 0,     null: false
    t.string   "price_currency",           default: "EUR", null: false
    t.integer  "offset_plate_id"
    t.integer  "offset_plate_provider_id"
    t.integer  "pack_amount"
    t.string   "pack_unit"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "offset_plate_prices", ["offset_plate_id"], name: "index_offset_plate_prices_on_offset_plate_id", using: :btree
  add_index "offset_plate_prices", ["offset_plate_provider_id"], name: "index_offset_plate_prices_on_offset_plate_provider_id", using: :btree

  create_table "offset_plate_providers", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "offset_plates", force: :cascade do |t|
    t.string   "name"
    t.integer  "machine_id"
    t.integer  "cylinder_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "offset_plates", ["cylinder_id"], name: "index_offset_plates_on_cylinder_id", using: :btree
  add_index "offset_plates", ["machine_id"], name: "index_offset_plates_on_machine_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "template_id"
    t.string   "workflow_state"
    t.integer  "amount"
    t.float    "width"
    t.float    "height"
    t.integer  "color_preset_id"
    t.integer  "machine_id"
    t.integer  "cylinder_id"
    t.integer  "item_count_on_width"
    t.float    "canvas_width"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "name"
    t.float    "ink_amount"
    t.string   "number"
    t.integer  "client_id"
    t.boolean  "lacquering",                             default: false
    t.integer  "lacquering_anilox_id"
    t.integer  "lacquering_name_id"
    t.integer  "lacquering_polymer_id"
    t.integer  "lacquering_type_id"
    t.integer  "lamination_machine_1_id"
    t.integer  "lamination_cylinder_1_id"
    t.integer  "lamination_type_1_id"
    t.integer  "lamination_glue_1_id"
    t.integer  "lamination_glue_system_1_id"
    t.integer  "lamination_glue_type_1_id"
    t.float    "canvas_width_lamination_1"
    t.float    "canvas_width_lamination_2"
    t.integer  "lamination_glue_system_2_id"
    t.integer  "lamination_type_2_id"
    t.integer  "lamination_cylinder_2_id"
    t.integer  "lamination_glue_2_id"
    t.integer  "lamination_glue_type_2_id"
    t.integer  "lamination_machine_2_id"
    t.integer  "lamination_roto_glue_name_1_id"
    t.integer  "lamination_roto_glue_type_1_id"
    t.integer  "lamination_roto_linearity_1_id"
    t.integer  "lamination_roto_preser_1_id"
    t.integer  "lamination_roto_glue_viscosity_1"
    t.float    "lamination_roto_acetate_1"
    t.float    "lamination_roto_resin_1"
    t.float    "lamination_roto_hardener_1"
    t.integer  "lamination_roto_glue_name_2_id"
    t.integer  "lamination_roto_glue_type_2_id"
    t.integer  "lamination_roto_linearity_2_id"
    t.integer  "lamination_roto_preser_2_id"
    t.integer  "lamination_roto_glue_viscosity_2"
    t.float    "lamination_roto_acetate_2"
    t.float    "lamination_roto_resin_2"
    t.float    "lamination_roto_hardener_2"
    t.integer  "product_structure_id"
    t.boolean  "roto",                                   default: false
    t.integer  "roto_linearity_id"
    t.integer  "roto_preser_id"
    t.integer  "roto_paint_id"
    t.boolean  "need_price_calculation",                 default: false
    t.integer  "yellow_percent"
    t.integer  "magenta_percent"
    t.integer  "cyan_percent"
    t.integer  "black_percent"
    t.integer  "yellow_provider_id"
    t.integer  "magenta_provider_id"
    t.integer  "cyan_provider_id"
    t.integer  "black_provider_id"
    t.integer  "background_for_print_provider_id"
    t.integer  "offset_plate_provider_id"
    t.integer  "azote_provider_id"
    t.integer  "price_additional_cents",                 default: 0,     null: false
    t.string   "price_additional_currency",              default: "EUR", null: false
    t.integer  "price_pack_cents",                       default: 0,     null: false
    t.string   "price_pack_currency",                    default: "EUR", null: false
    t.integer  "pack_provider_id"
    t.string   "pack_name"
    t.integer  "pack_amount"
    t.integer  "panton_1_paint_id"
    t.float    "panton_1_amount",                        default: 0.0
    t.integer  "panton_1_provider_id"
    t.integer  "panton_2_paint_id"
    t.float    "panton_2_amount",                        default: 0.0
    t.integer  "panton_2_provider_id"
    t.integer  "roto_acetate_provider_id"
    t.string   "ror"
    t.integer  "roto_paint_provider_id"
    t.integer  "roto_acetate_name_id"
    t.integer  "roto_paint_percentage"
    t.integer  "roto_acetate_percentage"
    t.integer  "cutting_machine_id"
    t.integer  "lacquering_provider_id"
    t.integer  "background_for_lamination1_provider_id"
    t.integer  "background_for_lamination2_provider_id"
  end

  add_index "orders", ["background_for_lamination1_provider_id"], name: "index_orders_on_background_for_lamination1_provider_id", using: :btree
  add_index "orders", ["background_for_lamination2_provider_id"], name: "index_orders_on_background_for_lamination2_provider_id", using: :btree
  add_index "orders", ["color_preset_id"], name: "index_orders_on_color_preset_id", using: :btree
  add_index "orders", ["cutting_machine_id"], name: "index_orders_on_cutting_machine_id", using: :btree
  add_index "orders", ["cylinder_id"], name: "index_orders_on_cylinder_id", using: :btree
  add_index "orders", ["lacquering_provider_id"], name: "index_orders_on_lacquering_provider_id", using: :btree
  add_index "orders", ["machine_id"], name: "index_orders_on_machine_id", using: :btree
  add_index "orders", ["panton_1_paint_id"], name: "index_orders_on_panton_1_paint_id", using: :btree
  add_index "orders", ["panton_1_provider_id"], name: "index_orders_on_panton_1_provider_id", using: :btree
  add_index "orders", ["panton_2_paint_id"], name: "index_orders_on_panton_2_paint_id", using: :btree
  add_index "orders", ["panton_2_provider_id"], name: "index_orders_on_panton_2_provider_id", using: :btree
  add_index "orders", ["roto_acetate_name_id"], name: "index_orders_on_roto_acetate_name_id", using: :btree
  add_index "orders", ["roto_paint_provider_id"], name: "index_orders_on_roto_paint_provider_id", using: :btree
  add_index "orders", ["template_id"], name: "index_orders_on_template_id", using: :btree

  create_table "pack_providers", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "paint_prices", force: :cascade do |t|
    t.integer  "price_cents",       default: 0,     null: false
    t.string   "price_currency",    default: "EUR", null: false
    t.integer  "pack_amount"
    t.string   "pack_unit"
    t.integer  "paint_provider_id"
    t.integer  "paint_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "paint_prices", ["paint_id"], name: "index_paint_prices_on_paint_id", using: :btree
  add_index "paint_prices", ["paint_provider_id"], name: "index_paint_prices_on_paint_provider_id", using: :btree

  create_table "paint_providers", force: :cascade do |t|
    t.string   "name"
    t.string   "yellow"
    t.string   "magenta"
    t.string   "cyan"
    t.string   "black"
    t.integer  "price_yellow_cents",     default: 0,     null: false
    t.string   "price_yellow_currency",  default: "EUR", null: false
    t.integer  "price_magenta_cents",    default: 0,     null: false
    t.string   "price_magenta_currency", default: "EUR", null: false
    t.integer  "price_cyan_cents",       default: 0,     null: false
    t.string   "price_cyan_currency",    default: "EUR", null: false
    t.integer  "price_black_cents",      default: 0,     null: false
    t.string   "price_black_currency",   default: "EUR", null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "yellow_pack_amount"
    t.string   "yellow_pack_unit"
    t.integer  "magenta_pack_amount"
    t.string   "magenta_pack_unit"
    t.integer  "cyan_pack_amount"
    t.string   "cyan_pack_unit"
    t.integer  "black_pack_amount"
    t.string   "black_pack_unit"
  end

  create_table "paints", force: :cascade do |t|
    t.string   "paint_type"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "machine_id"
  end

  add_index "paints", ["machine_id"], name: "index_paints_on_machine_id", using: :btree

  create_table "product_structures", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "uid"
  end

  add_index "product_structures", ["uid"], name: "index_product_structures_on_uid", using: :btree

  create_table "roto_indices", force: :cascade do |t|
    t.float    "value_dry"
    t.float    "value_wet"
    t.integer  "roto_linearity_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "roto_indices", ["roto_linearity_id"], name: "index_roto_indices_on_roto_linearity_id", using: :btree

  create_table "roto_linearities", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roto_paints", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roto_presers", force: :cascade do |t|
    t.integer  "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "template_background_prices", force: :cascade do |t|
    t.integer  "template_background_provider_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "price_cents"
    t.string   "price_currency"
    t.integer  "pack_amount"
    t.string   "pack_unit"
    t.integer  "background_id"
  end

  add_index "template_background_prices", ["background_id"], name: "index_template_background_prices_on_background_id", using: :btree
  add_index "template_background_prices", ["template_background_provider_id"], name: "back_providers_on_back_prices", using: :btree

  create_table "template_background_providers", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "templates", force: :cascade do |t|
    t.integer  "product_structure_id"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "lamination_scheme"
    t.integer  "thousand_more",                  default: 0
    t.integer  "thousand_less",                  default: 0
    t.integer  "background_id"
    t.integer  "background_for_lamination_1_id"
    t.integer  "background_for_lamination_2_id"
  end

  add_index "templates", ["background_for_lamination_1_id"], name: "index_templates_on_background_for_lamination_1_id", using: :btree
  add_index "templates", ["background_for_lamination_2_id"], name: "index_templates_on_background_for_lamination_2_id", using: :btree
  add_index "templates", ["background_id"], name: "index_templates_on_background_id", using: :btree
  add_index "templates", ["product_structure_id"], name: "index_templates_on_product_structure_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "role"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "first_name"
    t.string   "last_name"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "additional_parameters", "orders"
  add_foreign_key "cutting_indices", "cutting_machines"
  add_foreign_key "cutting_indices", "product_structures"
  add_foreign_key "cylinders", "machines"
  add_foreign_key "lacquering_aniloxes", "machines"
  add_foreign_key "lacquering_indices", "lacquering_aniloxes"
  add_foreign_key "lacquering_indices", "lacquering_polymers"
  add_foreign_key "lacquering_indices", "lacquering_types"
  add_foreign_key "lacquering_indices", "machines"
  add_foreign_key "lacquering_names", "machines"
  add_foreign_key "lacquering_polymers", "machines"
  add_foreign_key "lacquering_types", "machines"
  add_foreign_key "lamination_cylinders", "lamination_machines"
  add_foreign_key "lamination_glue_types", "lamination_types"
  add_foreign_key "lamination_indices", "lamination_machines"
  add_foreign_key "lamination_roto_indices", "lamination_roto_glue_types"
  add_foreign_key "lamination_roto_indices", "lamination_roto_linearities"
  add_foreign_key "machine_parameters", "machines"
  add_foreign_key "machine_parameters", "product_structures"
  add_foreign_key "machine_template_speeds", "machines"
  add_foreign_key "machine_template_speeds", "templates"
  add_foreign_key "machine_template_spendings", "machines"
  add_foreign_key "machine_template_spendings", "templates"
  add_foreign_key "offset_plate_prices", "offset_plate_providers"
  add_foreign_key "offset_plate_prices", "offset_plates"
  add_foreign_key "offset_plates", "cylinders"
  add_foreign_key "offset_plates", "machines"
  add_foreign_key "orders", "color_presets"
  add_foreign_key "orders", "cylinders"
  add_foreign_key "orders", "machines"
  add_foreign_key "orders", "templates"
  add_foreign_key "paint_prices", "paint_providers"
  add_foreign_key "paint_prices", "paints"
  add_foreign_key "paints", "machines"
  add_foreign_key "roto_indices", "roto_linearities"
  add_foreign_key "template_background_prices", "template_background_providers"
  add_foreign_key "templates", "product_structures"
end
