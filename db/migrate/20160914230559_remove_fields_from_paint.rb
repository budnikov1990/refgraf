class RemoveFieldsFromPaint < ActiveRecord::Migration
  def change
    remove_column :paints, :price_cents, :integer
    remove_column :paints, :price_currency, :string
    remove_column :paints, :pack_amount, :integer
    remove_column :paints, :pack_unit, :string
    remove_reference :paints, :paint_provider, index: true, foreign_key: true
  end
end
