class User < ActiveRecord::Base
  ROLES = %w(admin technologist operator storeman manager).freeze

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :omniauthable
  # :registerable, :recoverable
  devise :database_authenticatable, :rememberable, :trackable, :validatable

  ROLES.each do |role|
    define_method "#{role}?" do
      self.role == role
    end
  end

  validates :role, presence: true, inclusion: { in: ROLES }
  validates :first_name, presence: true
  validates :last_name, presence: true
end
