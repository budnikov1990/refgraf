class LacqueringPolymersController < ApplicationController
  before_action :set_and_authorize_lacquering_polymer, only: [:edit, :update, :destroy]

  # GET /lacquering_polymers
  def index
    authorize LacqueringPolymer
    @lacquering_polymers = policy_scope(LacqueringPolymer)
    @lacquering_polymers = @lacquering_polymers.order(id: :asc)
    @lacquering_polymers = @lacquering_polymers.page(params[:page]).per(25)
  end

  # GET /lacquering_polymers/new
  def new
    authorize LacqueringPolymer
    @lacquering_polymer = LacqueringPolymer.new
  end

  # POST /lacquering_polymers
  def create
    authorize LacqueringPolymer
    @lacquering_polymer = LacqueringPolymer.new(lacquering_polymer_params)
    if @lacquering_polymer.save
      redirect_to lacquering_polymers_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lacquering_polymers/:id/edit
  def edit
  end

  # PATCH/PUT /lacquering_polymers/:id
  def update
    if @lacquering_polymer.update(lacquering_polymer_params)
      redirect_to edit_lacquering_polymer_url(@lacquering_polymer),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lacquering_polymers/:id
  def destroy
    if @lacquering_polymer.destroy
      redirect_to lacquering_polymers_url, notice: t('shared.destroyed')
    else
      redirect_to lacquering_polymers_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lacquering_polymer_params
    params.require(:lacquering_polymer).permit(:value, :machine_id)
  end

  def set_and_authorize_lacquering_polymer
    @lacquering_polymer = LacqueringPolymer.find(params[:id])
    authorize @lacquering_polymer
  end
end
