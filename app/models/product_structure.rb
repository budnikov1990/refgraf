class ProductStructure < ActiveRecord::Base
  has_many :templates, dependent: :restrict_with_error
  has_many :machine_parameters, dependent: :restrict_with_error
  has_many :orders, dependent: :restrict_with_error

  validates :name, presence: true
  validates :uid, presence: true, uniqueness: true

  before_validation :update_uid

  def simplex
    uid.eql?('simplex')
  end

  def duplex
    uid.eql?('duplex')
  end

  def triplex
    uid.eql?('triplex')
  end

  private

  def update_uid
    self.uid = uid.to_s.tr(' ', '_').downcase
  end
end
