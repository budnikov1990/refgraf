class ProductStructuresController < ApplicationController
  before_action :set_and_authorize_structure, only: [:edit, :update, :destroy]

  # GET /product_structures
  def index
    authorize ProductStructure
    @structures = policy_scope(ProductStructure)
    @structures = @structures.order(id: :asc)
    @structures = @structures.page(params[:page]).per(25)
  end

  # GET /product_structures/new
  def new
    authorize ProductStructure
    @structure = ProductStructure.new
  end

  # POST /product_structures
  def create
    authorize ProductStructure
    @structure = ProductStructure.new(structure_params)
    if @structure.save
      redirect_to product_structures_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /product_structures/:id/edit
  def edit
  end

  # PATCH/PUT /product_structures/:id
  def update
    if @structure.update(structure_params)
      redirect_to edit_product_structure_url(@structure), notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /product_structures/:id
  def destroy
    if @structure.destroy
      redirect_to product_structures_url, notice: t('shared.destroyed')
    else
      redirect_to product_structures_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def structure_params
    params.require(:product_structure).permit(:name, :uid)
  end

  def set_and_authorize_structure
    @structure = ProductStructure.find(params[:id])
    authorize @structure
  end
end
