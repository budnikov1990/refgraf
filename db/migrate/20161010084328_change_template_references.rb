class ChangeTemplateReferences < ActiveRecord::Migration
  def change
    remove_reference :template_background_prices, :template, index: true

    add_reference :template_background_prices, :background, index: true
  end
end
