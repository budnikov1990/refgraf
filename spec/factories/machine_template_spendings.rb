FactoryGirl.define do
  factory :machine_template_spending do
    thousand_more 1
    thousand_less 1
    machine nil
    template nil
  end
end
