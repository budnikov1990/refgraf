class Background < ActiveRecord::Base
  has_many :templates, dependent: :destroy
  has_many :template_background_prices, dependent: :destroy

  validates :name, presence: true
  validates :raw_weight, numericality: { greater_than_or_equal_to: 0 }
end
