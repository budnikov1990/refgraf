class AddLaminationProvidersToOrder < ActiveRecord::Migration
  def change
    add_reference :orders, :background_for_lamination1_provider, index: true
    add_reference :orders, :background_for_lamination2_provider, index: true
  end
end
