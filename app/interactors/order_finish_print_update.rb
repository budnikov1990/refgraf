class OrderFinishPrintUpdate
  include Interactor
  include TransactionalInteractor

  def call
    # context.order.additional_parameter.update_attributes(context.params)
    context.order.finish_print!
    log
  end

  private

  delegate :params, :user, :order, to: :context

  def log
    Activity.create!(
      key: 'order.finish_print',
      trackable: order,
      owner: user,
      data: log_params
    )
  end

  def log_params
    {
      id: context.order.id,
      title: context.order.name,
      number: context.order.number,
      previous_state: 'printing',
      new_state: 'printed'
    }
  end
end
