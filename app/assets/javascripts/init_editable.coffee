$ ->
  $('.editable').each (i, elem) ->
    $(elem).editable({
      ajaxOptions: {
        type: "PATCH"
      },
      emptytext: 'Puste',
      mode: 'inline'
    });