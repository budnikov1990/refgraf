require 'features_helper'

RSpec.feature 'View list of azote_providers' do
  context 'by authenticated user' do
    given(:admin) { create(:user, role: 'manager') }
    given!(:azote_provider) { create(:azote_provider) }

    background do
      login admin
      visit azote_providers_path
    end

    scenario 'User sees list of azote_providers' do
      within "#azote_provider_#{azote_provider.id}" do
        expect(page).to have_content azote_provider.name
        expect(page).to have_selector '.fa-trash'
      end
    end
  end
end
