class LacqueringAniloxesController < ApplicationController
  before_action :set_and_authorize_lacquering_anilox, only: [:edit, :update, :destroy]

  # GET /lacquering_aniloxes
  def index
    authorize LacqueringAnilox
    @lacquering_aniloxes = policy_scope(LacqueringAnilox)
    @lacquering_aniloxes = @lacquering_aniloxes.order(id: :asc)
    @lacquering_aniloxes = @lacquering_aniloxes.page(params[:page]).per(25)
  end

  # GET /lacquering_aniloxes/new
  def new
    authorize LacqueringAnilox
    @lacquering_anilox = LacqueringAnilox.new
  end

  # POST /lacquering_aniloxes
  def create
    authorize LacqueringAnilox
    @lacquering_anilox = LacqueringAnilox.new(lacquering_anilox_params)
    if @lacquering_anilox.save
      redirect_to lacquering_aniloxes_url, notice: t('shared.created')
    else
      render :new
    end
  end

  # GET /lacquering_aniloxes/:id/edit
  def edit
  end

  # PATCH/PUT /lacquering_aniloxes/:id
  def update
    if @lacquering_anilox.update(lacquering_anilox_params)
      redirect_to edit_lacquering_anilox_url(@lacquering_anilox),
                  notice: t('shared.updated')
    else
      render :edit
    end
  end

  # DELETE /lacquering_aniloxes/:id
  def destroy
    if @lacquering_anilox.destroy
      redirect_to lacquering_aniloxes_url, notice: t('shared.destroyed')
    else
      redirect_to lacquering_aniloxes_url, alert: t('shared.not_destroyed')
    end
  end

  private

  def lacquering_anilox_params
    params.require(:lacquering_anilox).permit(:value, :machine_id)
  end

  def set_and_authorize_lacquering_anilox
    @lacquering_anilox = LacqueringAnilox.find(params[:id])
    authorize @lacquering_anilox
  end
end
