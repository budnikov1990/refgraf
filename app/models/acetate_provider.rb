class AcetateProvider < ActiveRecord::Base
  has_many :acetate_names, dependent: :destroy, inverse_of: :acetate_provider

  accepts_nested_attributes_for :acetate_names, reject_if: :all_blank, allow_destroy: true

  validates :name, presence: true
end
