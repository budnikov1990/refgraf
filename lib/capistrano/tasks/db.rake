namespace :db do
  desc 'Seeds default data to database'
  task :seed do
    on roles(:app), in: :sequence, wait: 5 do
      within release_path do
        execute :bundle, "exec rake db:seed RAILS_ENV=#{fetch(:stage)}"
      end
    end
  end

  desc 'Reset database'
  task :reset do
    on roles(:app), in: :sequence, wait: 5 do
      within release_path do
        execute :bundle, "exec rake db:schema:load RAILS_ENV=#{fetch(:stage)}"
        execute :bundle, "exec rake db:seed RAILS_ENV=#{fetch(:stage)}"
      end
    end
  end
end
